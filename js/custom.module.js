/**
* @ngdoc module
* @name WIKI
*
* @description
* Customizations for Wikidata Hackdays 2019 "Graph based Query Expansion" Frontend
*
*/

let app = angular.module('viewCustom', ['angularLoad']);

app.run(['angularLoad', function( angularLoad ){
    // map for places
    angularLoad.loadScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyAplG9Rfw9EpasYwzjkGVIz82fqFpxSPKA&sensor=false');
    angularLoad.loadCSS('https://unpkg.com/leaflet@1.5.1/dist/leaflet.css',{integrity: 'sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==', crossorigin:''});
    angularLoad.loadScript('https://unpkg.com/leaflet@1.5.1/dist/leaflet.js',{integrity: 'sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==', crossorigin:''});
}]);
// mapbox: pk.eyJ1IjoiYmVybmR1IiwiYSI6ImNrMjBpMHRidjE1ZGkzaHFnNDdlMXV4eWkifQ.RACc6dWL675djVZaMmHBww
// https://account.mapbox.com/access-tokens/
