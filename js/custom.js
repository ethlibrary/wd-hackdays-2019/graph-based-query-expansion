(function(){
"use strict";
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
* @ngdoc module
* @name WIKI
*
* @description
* Customizations for Wikidata Hackdays 2019 "Graph based Query Expansion" Frontend
*
*/

var app = angular.module('viewCustom', ['angularLoad']);

app.run(['angularLoad', function (angularLoad) {
    // map for places
    angularLoad.loadScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyAplG9Rfw9EpasYwzjkGVIz82fqFpxSPKA&sensor=false');
    angularLoad.loadCSS('https://unpkg.com/leaflet@1.5.1/dist/leaflet.css', { integrity: 'sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==', crossorigin: '' });
    angularLoad.loadScript('https://unpkg.com/leaflet@1.5.1/dist/leaflet.js', { integrity: 'sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==', crossorigin: '' });
}]);
// mapbox: pk.eyJ1IjoiYmVybmR1IiwiYSI6ImNrMjBpMHRidjE1ZGkzaHFnNDdlMXV4eWkifQ.RACc6dWL675djVZaMmHBww
// https://account.mapbox.com/access-tokens/

/**
* @memberof WIKI
* @ngdoc directive
* @name prmSearchResultListAfter
*
* @description
* WIKI After Component<br>
*
* Customization for the search result list:<br>
*
* - Query expansion via wikidata
*
*
* <b>Requirements</b><br>
*
* WIKI Component {@link WIKI.ethWdExpansionPersons}<br>
* WIKI Component {@link WIKI.ethWdExpansionPlaces}<br>
* WIKI Component {@link WIKI.ethWdExpansionPerson}<br>
* WIKI Component {@link WIKI.ethWdExpansionPlace}<br>
*
*
* <b>In Package</b><br>
*
* WIKI<br>
 */
app.component('prmSearchResultListAfter', {
    bindings: { parentCtrl: '<' },
    template: '<eth-wd-expansion-persons ng-if="$ctrl.tab===\'default_tab\'"></eth-wd-expansion-persons>\n               <eth-wd-expansion-person ng-if="$ctrl.tab===\'default_tab\'"></eth-wd-expansion-person>\n               <eth-wd-expansion-places ng-if="$ctrl.tab===\'default_tab\'"></eth-wd-expansion-places>\n               <eth-wd-expansion-place ng-if="$ctrl.tab===\'default_tab\'"></eth-wd-expansion-place>\n               ',
    controller: function () {
        function prmSearchResultListAfterController(searchService) {
            _classCallCheck(this, prmSearchResultListAfterController);

            this.searchService = searchService;
            this.tab = '';
        }

        _createClass(prmSearchResultListAfterController, [{
            key: '$onInit',
            value: function $onInit() {
                if (!this.parentCtrl.primolyticsService || !this.parentCtrl.primolyticsService.searchService) {
                    this.tab = "";
                    console.error("***ETH*** prmSearchResultListAfter.$onInit: primolyticsService or searchService not available");
                } else {
                    this.tab = this.searchService.getTab(this.parentCtrl.primolyticsService.searchService);
                }
            }
        }]);

        return prmSearchResultListAfterController;
    }()
});

// ==ClosureCompiler==
// @compilation_level ADVANCED_OPTIMIZATIONS
// @externs_url http://closure-compiler.googlecode.com/svn/trunk/contrib/externs/maps/google_maps_api_v3_3.js
// ==/ClosureCompiler==

/**
 * @name MarkerClusterer for Google Maps v3
 * @version version 1.0.3
 * @author Luke Mahe
 * @fileoverview
 * The library creates and manages per-zoom-level clusters for large amounts of
 * markers.
 */

/**
 * @license
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A Marker Clusterer that clusters markers.
 *
 * @param {google.maps.Map} map The Google map to attach to.
 * @param {Array.<google.maps.Marker>=} opt_markers Optional markers to add to
 *   the cluster.
 * @param {Object=} opt_options support the following options:
 *     'gridSize': (number) The grid size of a cluster in pixels.
 *     'maxZoom': (number) The maximum zoom level that a marker can be part of a
 *                cluster.
 *     'zoomOnClick': (boolean) Whether the default behaviour of clicking on a
 *                    cluster is to zoom into it.
 *     'imagePath': (string) The base URL where the images representing
 *                  clusters will be found. The full URL will be:
 *                  {imagePath}[1-5].{imageExtension}
 *                  Default: '../images/m'.
 *     'imageExtension': (string) The suffix for images URL representing
 *                       clusters will be found. See _imagePath_ for details.
 *                       Default: 'png'.
 *     'averageCenter': (boolean) Whether the center of each cluster should be
 *                      the average of all markers in the cluster.
 *     'minimumClusterSize': (number) The minimum number of markers to be in a
 *                           cluster before the markers are hidden and a count
 *                           is shown.
 *     'styles': (object) An object that has style properties:
 *       'url': (string) The image url.
 *       'height': (number) The image height.
 *       'width': (number) The image width.
 *       'anchor': (Array) The anchor position of the label text.
 *       'textColor': (string) The text color.
 *       'textSize': (number) The text size.
 *       'backgroundPosition': (string) The position of the backgound x, y.
 * @constructor
 * @extends google.maps.OverlayView
 */
function MarkerClusterer(map, opt_markers, opt_options) {
    // MarkerClusterer implements google.maps.OverlayView interface. We use the
    // extend function to extend MarkerClusterer with google.maps.OverlayView
    // because it might not always be available when the code is defined so we
    // look for it at the last possible moment. If it doesn't exist now then
    // there is no point going ahead :)
    this.extend(MarkerClusterer, google.maps.OverlayView);
    this.map_ = map;

    /**
     * @type {Array.<google.maps.Marker>}
     * @private
     */
    this.markers_ = [];

    /**
     *  @type {Array.<Cluster>}
     */
    this.clusters_ = [];

    this.sizes = [53, 56, 66, 78, 90];

    /**
     * @private
     */
    this.styles_ = [];

    /**
     * @type {boolean}
     * @private
     */
    this.ready_ = false;

    var options = opt_options || {};

    /**
     * @type {number}
     * @private
     */
    this.gridSize_ = options['gridSize'] || 60;

    /**
     * @private
     */
    this.minClusterSize_ = options['minimumClusterSize'] || 2;

    /**
     * @type {?number}
     * @private
     */
    this.maxZoom_ = options['maxZoom'] || null;

    this.styles_ = options['styles'] || [];

    /**
     * @type {string}
     * @private
     */
    this.imagePath_ = options['imagePath'] || this.MARKER_CLUSTER_IMAGE_PATH_;

    /**
     * @type {string}
     * @private
     */
    this.imageExtension_ = options['imageExtension'] || this.MARKER_CLUSTER_IMAGE_EXTENSION_;

    /**
     * @type {boolean}
     * @private
     */
    this.zoomOnClick_ = true;

    if (options['zoomOnClick'] != undefined) {
        this.zoomOnClick_ = options['zoomOnClick'];
    }

    /**
     * @type {boolean}
     * @private
     */
    this.averageCenter_ = false;

    if (options['averageCenter'] != undefined) {
        this.averageCenter_ = options['averageCenter'];
    }

    this.setupStyles_();

    this.setMap(map);

    /**
     * @type {number}
     * @private
     */
    this.prevZoom_ = this.map_.getZoom();

    // Add the map event listeners
    var that = this;
    google.maps.event.addListener(this.map_, 'zoom_changed', function () {
        // Determines map type and prevent illegal zoom levels
        var zoom = that.map_.getZoom();
        var minZoom = that.map_.minZoom || 0;
        var maxZoom = Math.min(that.map_.maxZoom || 100, that.map_.mapTypes[that.map_.getMapTypeId()].maxZoom);
        zoom = Math.min(Math.max(zoom, minZoom), maxZoom);

        if (that.prevZoom_ != zoom) {
            that.prevZoom_ = zoom;
            that.resetViewport();
        }
    });

    google.maps.event.addListener(this.map_, 'idle', function () {
        that.redraw();
    });

    // Finally, add the markers
    if (opt_markers && (opt_markers.length || Object.keys(opt_markers).length)) {
        this.addMarkers(opt_markers, false);
    }
}

/**
 * The marker cluster image path.
 *
 * @type {string}
 * @private
 */
MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_PATH_ = '../images/m';

/**
 * The marker cluster image path.
 *
 * @type {string}
 * @private
 */
MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_EXTENSION_ = 'png';

/**
 * Extends a objects prototype by anothers.
 *
 * @param {Object} obj1 The object to be extended.
 * @param {Object} obj2 The object to extend with.
 * @return {Object} The new extended object.
 * @ignore
 */
MarkerClusterer.prototype.extend = function (obj1, obj2) {
    return function (object) {
        for (var property in object.prototype) {
            this.prototype[property] = object.prototype[property];
        }
        return this;
    }.apply(obj1, [obj2]);
};

/**
 * Implementaion of the interface method.
 * @ignore
 */
MarkerClusterer.prototype.onAdd = function () {
    this.setReady_(true);
};

/**
 * Implementaion of the interface method.
 * @ignore
 */
MarkerClusterer.prototype.draw = function () {};

/**
 * Sets up the styles object.
 *
 * @private
 */
MarkerClusterer.prototype.setupStyles_ = function () {
    if (this.styles_.length) {
        return;
    }

    for (var i = 0, size; size = this.sizes[i]; i++) {
        this.styles_.push({
            url: this.imagePath_ + (i + 1) + '.' + this.imageExtension_,
            height: size,
            width: size
        });
    }
};

/**
 *  Fit the map to the bounds of the markers in the clusterer.
 */
MarkerClusterer.prototype.fitMapToMarkers = function () {
    var markers = this.getMarkers();
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, marker; marker = markers[i]; i++) {
        bounds.extend(marker.getPosition());
    }

    this.map_.fitBounds(bounds);
};

/**
 *  Sets the styles.
 *
 *  @param {Object} styles The style to set.
 */
MarkerClusterer.prototype.setStyles = function (styles) {
    this.styles_ = styles;
};

/**
 *  Gets the styles.
 *
 *  @return {Object} The styles object.
 */
MarkerClusterer.prototype.getStyles = function () {
    return this.styles_;
};

/**
 * Whether zoom on click is set.
 *
 * @return {boolean} True if zoomOnClick_ is set.
 */
MarkerClusterer.prototype.isZoomOnClick = function () {
    return this.zoomOnClick_;
};

/**
 * Whether average center is set.
 *
 * @return {boolean} True if averageCenter_ is set.
 */
MarkerClusterer.prototype.isAverageCenter = function () {
    return this.averageCenter_;
};

/**
 *  Returns the array of markers in the clusterer.
 *
 *  @return {Array.<google.maps.Marker>} The markers.
 */
MarkerClusterer.prototype.getMarkers = function () {
    return this.markers_;
};

/**
 *  Returns the number of markers in the clusterer
 *
 *  @return {Number} The number of markers.
 */
MarkerClusterer.prototype.getTotalMarkers = function () {
    return this.markers_.length;
};

/**
 *  Sets the max zoom for the clusterer.
 *
 *  @param {number} maxZoom The max zoom level.
 */
MarkerClusterer.prototype.setMaxZoom = function (maxZoom) {
    this.maxZoom_ = maxZoom;
};

/**
 *  Gets the max zoom for the clusterer.
 *
 *  @return {number} The max zoom level.
 */
MarkerClusterer.prototype.getMaxZoom = function () {
    return this.maxZoom_;
};

/**
 *  The function for calculating the cluster icon image.
 *
 *  @param {Array.<google.maps.Marker>} markers The markers in the clusterer.
 *  @param {number} numStyles The number of styles available.
 *  @return {Object} A object properties: 'text' (string) and 'index' (number).
 *  @private
 */
MarkerClusterer.prototype.calculator_ = function (markers, numStyles) {
    var index = 0;
    var count = markers.length;
    var dv = count;
    while (dv !== 0) {
        dv = parseInt(dv / 10, 10);
        index++;
    }

    index = Math.min(index, numStyles);
    return {
        text: count,
        index: index
    };
};

/**
 * Set the calculator function.
 *
 * @param {function(Array, number)} calculator The function to set as the
 *     calculator. The function should return a object properties:
 *     'text' (string) and 'index' (number).
 *
 */
MarkerClusterer.prototype.setCalculator = function (calculator) {
    this.calculator_ = calculator;
};

/**
 * Get the calculator function.
 *
 * @return {function(Array, number)} the calculator function.
 */
MarkerClusterer.prototype.getCalculator = function () {
    return this.calculator_;
};

/**
 * Add an array of markers to the clusterer.
 *
 * @param {Array.<google.maps.Marker>} markers The markers to add.
 * @param {boolean=} opt_nodraw Whether to redraw the clusters.
 */
MarkerClusterer.prototype.addMarkers = function (markers, opt_nodraw) {
    if (markers.length) {
        for (var i = 0, marker; marker = markers[i]; i++) {
            this.pushMarkerTo_(marker);
        }
    } else if (Object.keys(markers).length) {
        for (var marker in markers) {
            this.pushMarkerTo_(markers[marker]);
        }
    }
    if (!opt_nodraw) {
        this.redraw();
    }
};

/**
 * Pushes a marker to the clusterer.
 *
 * @param {google.maps.Marker} marker The marker to add.
 * @private
 */
MarkerClusterer.prototype.pushMarkerTo_ = function (marker) {
    marker.isAdded = false;
    if (marker['draggable']) {
        // If the marker is draggable add a listener so we update the clusters on
        // the drag end.
        var that = this;
        google.maps.event.addListener(marker, 'dragend', function () {
            marker.isAdded = false;
            that.repaint();
        });
    }
    this.markers_.push(marker);
};

/**
 * Adds a marker to the clusterer and redraws if needed.
 *
 * @param {google.maps.Marker} marker The marker to add.
 * @param {boolean=} opt_nodraw Whether to redraw the clusters.
 */
MarkerClusterer.prototype.addMarker = function (marker, opt_nodraw) {
    this.pushMarkerTo_(marker);
    if (!opt_nodraw) {
        this.redraw();
    }
};

/**
 * Removes a marker and returns true if removed, false if not
 *
 * @param {google.maps.Marker} marker The marker to remove
 * @return {boolean} Whether the marker was removed or not
 * @private
 */
MarkerClusterer.prototype.removeMarker_ = function (marker) {
    var index = -1;
    if (this.markers_.indexOf) {
        index = this.markers_.indexOf(marker);
    } else {
        for (var i = 0, m; m = this.markers_[i]; i++) {
            if (m == marker) {
                index = i;
                break;
            }
        }
    }

    if (index == -1) {
        // Marker is not in our list of markers.
        return false;
    }

    marker.setMap(null);

    this.markers_.splice(index, 1);

    return true;
};

/**
 * Remove a marker from the cluster.
 *
 * @param {google.maps.Marker} marker The marker to remove.
 * @param {boolean=} opt_nodraw Optional boolean to force no redraw.
 * @return {boolean} True if the marker was removed.
 */
MarkerClusterer.prototype.removeMarker = function (marker, opt_nodraw) {
    var removed = this.removeMarker_(marker);

    if (!opt_nodraw && removed) {
        this.resetViewport();
        this.redraw();
        return true;
    } else {
        return false;
    }
};

/**
 * Removes an array of markers from the cluster.
 *
 * @param {Array.<google.maps.Marker>} markers The markers to remove.
 * @param {boolean=} opt_nodraw Optional boolean to force no redraw.
 */
MarkerClusterer.prototype.removeMarkers = function (markers, opt_nodraw) {
    // create a local copy of markers if required
    // (removeMarker_ modifies the getMarkers() array in place)
    var markersCopy = markers === this.getMarkers() ? markers.slice() : markers;
    var removed = false;

    for (var i = 0, marker; marker = markersCopy[i]; i++) {
        var r = this.removeMarker_(marker);
        removed = removed || r;
    }

    if (!opt_nodraw && removed) {
        this.resetViewport();
        this.redraw();
        return true;
    }
};

/**
 * Sets the clusterer's ready state.
 *
 * @param {boolean} ready The state.
 * @private
 */
MarkerClusterer.prototype.setReady_ = function (ready) {
    if (!this.ready_) {
        this.ready_ = ready;
        this.createClusters_();
    }
};

/**
 * Returns the number of clusters in the clusterer.
 *
 * @return {number} The number of clusters.
 */
MarkerClusterer.prototype.getTotalClusters = function () {
    return this.clusters_.length;
};

/**
 * Returns the google map that the clusterer is associated with.
 *
 * @return {google.maps.Map} The map.
 */
MarkerClusterer.prototype.getMap = function () {
    return this.map_;
};

/**
 * Sets the google map that the clusterer is associated with.
 *
 * @param {google.maps.Map} map The map.
 */
MarkerClusterer.prototype.setMap = function (map) {
    this.map_ = map;
};

/**
 * Returns the size of the grid.
 *
 * @return {number} The grid size.
 */
MarkerClusterer.prototype.getGridSize = function () {
    return this.gridSize_;
};

/**
 * Sets the size of the grid.
 *
 * @param {number} size The grid size.
 */
MarkerClusterer.prototype.setGridSize = function (size) {
    this.gridSize_ = size;
};

/**
 * Returns the min cluster size.
 *
 * @return {number} The grid size.
 */
MarkerClusterer.prototype.getMinClusterSize = function () {
    return this.minClusterSize_;
};

/**
 * Sets the min cluster size.
 *
 * @param {number} size The grid size.
 */
MarkerClusterer.prototype.setMinClusterSize = function (size) {
    this.minClusterSize_ = size;
};

/**
 * Extends a bounds object by the grid size.
 *
 * @param {google.maps.LatLngBounds} bounds The bounds to extend.
 * @return {google.maps.LatLngBounds} The extended bounds.
 */
MarkerClusterer.prototype.getExtendedBounds = function (bounds) {
    var projection = this.getProjection();

    // Turn the bounds into latlng.
    var tr = new google.maps.LatLng(bounds.getNorthEast().lat(), bounds.getNorthEast().lng());
    var bl = new google.maps.LatLng(bounds.getSouthWest().lat(), bounds.getSouthWest().lng());

    // Convert the points to pixels and the extend out by the grid size.
    var trPix = projection.fromLatLngToDivPixel(tr);
    trPix.x += this.gridSize_;
    trPix.y -= this.gridSize_;

    var blPix = projection.fromLatLngToDivPixel(bl);
    blPix.x -= this.gridSize_;
    blPix.y += this.gridSize_;

    // Convert the pixel points back to LatLng
    var ne = projection.fromDivPixelToLatLng(trPix);
    var sw = projection.fromDivPixelToLatLng(blPix);

    // Extend the bounds to contain the new bounds.
    bounds.extend(ne);
    bounds.extend(sw);

    return bounds;
};

/**
 * Determins if a marker is contained in a bounds.
 *
 * @param {google.maps.Marker} marker The marker to check.
 * @param {google.maps.LatLngBounds} bounds The bounds to check against.
 * @return {boolean} True if the marker is in the bounds.
 * @private
 */
MarkerClusterer.prototype.isMarkerInBounds_ = function (marker, bounds) {
    return bounds.contains(marker.getPosition());
};

/**
 * Clears all clusters and markers from the clusterer.
 */
MarkerClusterer.prototype.clearMarkers = function () {
    this.resetViewport(true);

    // Set the markers a empty array.
    this.markers_ = [];
};

/**
 * Clears all existing clusters and recreates them.
 * @param {boolean} opt_hide To also hide the marker.
 */
MarkerClusterer.prototype.resetViewport = function (opt_hide) {
    // Remove all the clusters
    for (var i = 0, cluster; cluster = this.clusters_[i]; i++) {
        cluster.remove();
    }

    // Reset the markers to not be added and to be invisible.
    for (var i = 0, marker; marker = this.markers_[i]; i++) {
        marker.isAdded = false;
        if (opt_hide) {
            marker.setMap(null);
        }
    }

    this.clusters_ = [];
};

/**
 *
 */
MarkerClusterer.prototype.repaint = function () {
    var oldClusters = this.clusters_.slice();
    this.clusters_.length = 0;
    this.resetViewport();
    this.redraw();

    // Remove the old clusters.
    // Do it in a timeout so the other clusters have been drawn first.
    window.setTimeout(function () {
        for (var i = 0, cluster; cluster = oldClusters[i]; i++) {
            cluster.remove();
        }
    }, 0);
};

/**
 * Redraws the clusters.
 */
MarkerClusterer.prototype.redraw = function () {
    this.createClusters_();
};

/**
 * Calculates the distance between two latlng locations in km.
 * @see http://www.movable-type.co.uk/scripts/latlong.html
 *
 * @param {google.maps.LatLng} p1 The first lat lng point.
 * @param {google.maps.LatLng} p2 The second lat lng point.
 * @return {number} The distance between the two points in km.
 * @private
*/
MarkerClusterer.prototype.distanceBetweenPoints_ = function (p1, p2) {
    if (!p1 || !p2) {
        return 0;
    }

    var R = 6371; // Radius of the Earth in km
    var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
    var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
};

/**
 * Add a marker to a cluster, or creates a new cluster.
 *
 * @param {google.maps.Marker} marker The marker to add.
 * @private
 */
MarkerClusterer.prototype.addToClosestCluster_ = function (marker) {
    var distance = 40000; // Some large number
    var clusterToAddTo = null;
    var pos = marker.getPosition();
    for (var i = 0, cluster; cluster = this.clusters_[i]; i++) {
        var center = cluster.getCenter();
        if (center) {
            var d = this.distanceBetweenPoints_(center, marker.getPosition());
            if (d < distance) {
                distance = d;
                clusterToAddTo = cluster;
            }
        }
    }

    if (clusterToAddTo && clusterToAddTo.isMarkerInClusterBounds(marker)) {
        clusterToAddTo.addMarker(marker);
    } else {
        var cluster = new Cluster(this);
        cluster.addMarker(marker);
        this.clusters_.push(cluster);
    }
};

/**
 * Creates the clusters.
 *
 * @private
 */
MarkerClusterer.prototype.createClusters_ = function () {
    if (!this.ready_) {
        return;
    }

    // Get our current map view bounds.
    // Create a new bounds object so we don't affect the map.
    var mapBounds = new google.maps.LatLngBounds(this.map_.getBounds().getSouthWest(), this.map_.getBounds().getNorthEast());
    var bounds = this.getExtendedBounds(mapBounds);

    for (var i = 0, marker; marker = this.markers_[i]; i++) {
        if (!marker.isAdded && this.isMarkerInBounds_(marker, bounds)) {
            this.addToClosestCluster_(marker);
        }
    }
};

/**
 * A cluster that contains markers.
 *
 * @param {MarkerClusterer} markerClusterer The markerclusterer that this
 *     cluster is associated with.
 * @constructor
 * @ignore
 */
function Cluster(markerClusterer) {
    this.markerClusterer_ = markerClusterer;
    this.map_ = markerClusterer.getMap();
    this.gridSize_ = markerClusterer.getGridSize();
    this.minClusterSize_ = markerClusterer.getMinClusterSize();
    this.averageCenter_ = markerClusterer.isAverageCenter();
    this.center_ = null;
    this.markers_ = [];
    this.bounds_ = null;
    this.clusterIcon_ = new ClusterIcon(this, markerClusterer.getStyles(), markerClusterer.getGridSize());
}

/**
 * Determins if a marker is already added to the cluster.
 *
 * @param {google.maps.Marker} marker The marker to check.
 * @return {boolean} True if the marker is already added.
 */
Cluster.prototype.isMarkerAlreadyAdded = function (marker) {
    if (this.markers_.indexOf) {
        return this.markers_.indexOf(marker) != -1;
    } else {
        for (var i = 0, m; m = this.markers_[i]; i++) {
            if (m == marker) {
                return true;
            }
        }
    }
    return false;
};

/**
 * Add a marker the cluster.
 *
 * @param {google.maps.Marker} marker The marker to add.
 * @return {boolean} True if the marker was added.
 */
Cluster.prototype.addMarker = function (marker) {
    if (this.isMarkerAlreadyAdded(marker)) {
        return false;
    }

    if (!this.center_) {
        this.center_ = marker.getPosition();
        this.calculateBounds_();
    } else {
        if (this.averageCenter_) {
            var l = this.markers_.length + 1;
            var lat = (this.center_.lat() * (l - 1) + marker.getPosition().lat()) / l;
            var lng = (this.center_.lng() * (l - 1) + marker.getPosition().lng()) / l;
            this.center_ = new google.maps.LatLng(lat, lng);
            this.calculateBounds_();
        }
    }

    marker.isAdded = true;
    this.markers_.push(marker);

    var len = this.markers_.length;
    if (len < this.minClusterSize_ && marker.getMap() != this.map_) {
        // Min cluster size not reached so show the marker.
        marker.setMap(this.map_);
    }

    if (len == this.minClusterSize_) {
        // Hide the markers that were showing.
        for (var i = 0; i < len; i++) {
            this.markers_[i].setMap(null);
        }
    }

    if (len >= this.minClusterSize_) {
        marker.setMap(null);
    }

    this.updateIcon();
    return true;
};

/**
 * Returns the marker clusterer that the cluster is associated with.
 *
 * @return {MarkerClusterer} The associated marker clusterer.
 */
Cluster.prototype.getMarkerClusterer = function () {
    return this.markerClusterer_;
};

/**
 * Returns the bounds of the cluster.
 *
 * @return {google.maps.LatLngBounds} the cluster bounds.
 */
Cluster.prototype.getBounds = function () {
    var bounds = new google.maps.LatLngBounds(this.center_, this.center_);
    var markers = this.getMarkers();
    for (var i = 0, marker; marker = markers[i]; i++) {
        bounds.extend(marker.getPosition());
    }
    return bounds;
};

/**
 * Removes the cluster
 */
Cluster.prototype.remove = function () {
    this.clusterIcon_.remove();
    this.markers_.length = 0;
    delete this.markers_;
};

/**
 * Returns the number of markers in the cluster.
 *
 * @return {number} The number of markers in the cluster.
 */
Cluster.prototype.getSize = function () {
    return this.markers_.length;
};

/**
 * Returns a list of the markers in the cluster.
 *
 * @return {Array.<google.maps.Marker>} The markers in the cluster.
 */
Cluster.prototype.getMarkers = function () {
    return this.markers_;
};

/**
 * Returns the center of the cluster.
 *
 * @return {google.maps.LatLng} The cluster center.
 */
Cluster.prototype.getCenter = function () {
    return this.center_;
};

/**
 * Calculated the extended bounds of the cluster with the grid.
 *
 * @private
 */
Cluster.prototype.calculateBounds_ = function () {
    var bounds = new google.maps.LatLngBounds(this.center_, this.center_);
    this.bounds_ = this.markerClusterer_.getExtendedBounds(bounds);
};

/**
 * Determines if a marker lies in the clusters bounds.
 *
 * @param {google.maps.Marker} marker The marker to check.
 * @return {boolean} True if the marker lies in the bounds.
 */
Cluster.prototype.isMarkerInClusterBounds = function (marker) {
    return this.bounds_.contains(marker.getPosition());
};

/**
 * Returns the map that the cluster is associated with.
 *
 * @return {google.maps.Map} The map.
 */
Cluster.prototype.getMap = function () {
    return this.map_;
};

/**
 * Updates the cluster icon
 */
Cluster.prototype.updateIcon = function () {
    var zoom = this.map_.getZoom();
    var mz = this.markerClusterer_.getMaxZoom();

    if (mz && zoom > mz) {
        // The zoom is greater than our max zoom so show all the markers in cluster.
        for (var i = 0, marker; marker = this.markers_[i]; i++) {
            marker.setMap(this.map_);
        }
        return;
    }

    if (this.markers_.length < this.minClusterSize_) {
        // Min cluster size not yet reached.
        this.clusterIcon_.hide();
        return;
    }

    var numStyles = this.markerClusterer_.getStyles().length;
    var sums = this.markerClusterer_.getCalculator()(this.markers_, numStyles);
    this.clusterIcon_.setCenter(this.center_);
    this.clusterIcon_.setSums(sums);
    this.clusterIcon_.show();
};

/**
 * A cluster icon
 *
 * @param {Cluster} cluster The cluster to be associated with.
 * @param {Object} styles An object that has style properties:
 *     'url': (string) The image url.
 *     'height': (number) The image height.
 *     'width': (number) The image width.
 *     'anchor': (Array) The anchor position of the label text.
 *     'textColor': (string) The text color.
 *     'textSize': (number) The text size.
 *     'backgroundPosition: (string) The background postition x, y.
 * @param {number=} opt_padding Optional padding to apply to the cluster icon.
 * @constructor
 * @extends google.maps.OverlayView
 * @ignore
 */
function ClusterIcon(cluster, styles, opt_padding) {
    cluster.getMarkerClusterer().extend(ClusterIcon, google.maps.OverlayView);

    this.styles_ = styles;
    this.padding_ = opt_padding || 0;
    this.cluster_ = cluster;
    this.center_ = null;
    this.map_ = cluster.getMap();
    this.div_ = null;
    this.sums_ = null;
    this.visible_ = false;

    this.setMap(this.map_);
}

/**
 * Triggers the clusterclick event and zoom's if the option is set.
 */
ClusterIcon.prototype.triggerClusterClick = function () {
    var clusterBounds = this.cluster_.getBounds();
    var markerClusterer = this.cluster_.getMarkerClusterer();

    // Trigger the clusterclick event.
    google.maps.event.trigger(markerClusterer.map_, 'clusterclick', this.cluster_);

    if (markerClusterer.isZoomOnClick()) {
        // Zoom into the cluster.
        this.map_.fitBounds(clusterBounds);
        this.map_.setCenter(clusterBounds.getCenter());
    }
};

/**
 * Adding the cluster icon to the dom.
 * @ignore
 */
ClusterIcon.prototype.onAdd = function () {
    this.div_ = document.createElement('DIV');
    if (this.visible_) {
        var pos = this.getPosFromLatLng_(this.center_);
        this.div_.style.cssText = this.createCss(pos);
        this.div_.innerHTML = this.sums_.text;
    }

    var panes = this.getPanes();
    panes.overlayMouseTarget.appendChild(this.div_);

    var that = this;
    google.maps.event.addDomListener(this.div_, 'click', function () {
        that.triggerClusterClick();
    });
};

/**
 * Returns the position to place the div dending on the latlng.
 *
 * @param {google.maps.LatLng} latlng The position in latlng.
 * @return {google.maps.Point} The position in pixels.
 * @private
 */
ClusterIcon.prototype.getPosFromLatLng_ = function (latlng) {
    var pos = this.getProjection().fromLatLngToDivPixel(latlng);
    pos.x -= parseInt(this.width_ / 2, 10);
    pos.y -= parseInt(this.height_ / 2, 10);
    return pos;
};

/**
 * Draw the icon.
 * @ignore
 */
ClusterIcon.prototype.draw = function () {
    if (this.visible_) {
        var pos = this.getPosFromLatLng_(this.center_);
        this.div_.style.top = pos.y + 'px';
        this.div_.style.left = pos.x + 'px';
        this.div_.style.zIndex = google.maps.Marker.MAX_ZINDEX + 1;
    }
};

/**
 * Hide the icon.
 */
ClusterIcon.prototype.hide = function () {
    if (this.div_) {
        this.div_.style.display = 'none';
    }
    this.visible_ = false;
};

/**
 * Position and show the icon.
 */
ClusterIcon.prototype.show = function () {
    if (this.div_) {
        var pos = this.getPosFromLatLng_(this.center_);
        this.div_.style.cssText = this.createCss(pos);
        this.div_.style.display = '';
    }
    this.visible_ = true;
};

/**
 * Remove the icon from the map
 */
ClusterIcon.prototype.remove = function () {
    this.setMap(null);
};

/**
 * Implementation of the onRemove interface.
 * @ignore
 */
ClusterIcon.prototype.onRemove = function () {
    if (this.div_ && this.div_.parentNode) {
        this.hide();
        this.div_.parentNode.removeChild(this.div_);
        this.div_ = null;
    }
};

/**
 * Set the sums of the icon.
 *
 * @param {Object} sums The sums containing:
 *   'text': (string) The text to display in the icon.
 *   'index': (number) The style index of the icon.
 */
ClusterIcon.prototype.setSums = function (sums) {
    this.sums_ = sums;
    this.text_ = sums.text;
    this.index_ = sums.index;
    if (this.div_) {
        this.div_.innerHTML = sums.text;
    }

    this.useStyle();
};

/**
 * Sets the icon to the the styles.
 */
ClusterIcon.prototype.useStyle = function () {
    var index = Math.max(0, this.sums_.index - 1);
    index = Math.min(this.styles_.length - 1, index);
    var style = this.styles_[index];
    this.url_ = style['url'];
    this.height_ = style['height'];
    this.width_ = style['width'];
    this.textColor_ = style['textColor'];
    this.anchor_ = style['anchor'];
    this.textSize_ = style['textSize'];
    this.backgroundPosition_ = style['backgroundPosition'];
};

/**
 * Sets the center of the icon.
 *
 * @param {google.maps.LatLng} center The latlng to set as the center.
 */
ClusterIcon.prototype.setCenter = function (center) {
    this.center_ = center;
};

/**
 * Create the css text based on the position of the icon.
 *
 * @param {google.maps.Point} pos The position.
 * @return {string} The css style text.
 */
ClusterIcon.prototype.createCss = function (pos) {
    var style = [];
    style.push('background-image:url(' + this.url_ + ');');
    var backgroundPosition = this.backgroundPosition_ ? this.backgroundPosition_ : '0 0';
    style.push('background-position:' + backgroundPosition + ';');

    if (_typeof(this.anchor_) === 'object') {
        if (typeof this.anchor_[0] === 'number' && this.anchor_[0] > 0 && this.anchor_[0] < this.height_) {
            style.push('height:' + (this.height_ - this.anchor_[0]) + 'px; padding-top:' + this.anchor_[0] + 'px;');
        } else {
            style.push('height:' + this.height_ + 'px; line-height:' + this.height_ + 'px;');
        }
        if (typeof this.anchor_[1] === 'number' && this.anchor_[1] > 0 && this.anchor_[1] < this.width_) {
            style.push('width:' + (this.width_ - this.anchor_[1]) + 'px; padding-left:' + this.anchor_[1] + 'px;');
        } else {
            style.push('width:' + this.width_ + 'px; text-align:center;');
        }
    } else {
        style.push('height:' + this.height_ + 'px; line-height:' + this.height_ + 'px; width:' + this.width_ + 'px; text-align:center;');
    }

    var txtColor = this.textColor_ ? this.textColor_ : 'black';
    var txtSize = this.textSize_ ? this.textSize_ : 11;

    style.push('cursor:pointer; top:' + pos.y + 'px; left:' + pos.x + 'px; color:' + txtColor + '; position:absolute; font-size:' + txtSize + 'px; font-family:Arial,sans-serif; font-weight:bold');
    return style.join('');
};

// Export Symbols for Closure
// If you are not going to compile with closure then you can remove the
// code below.
var window = window || {};
window['MarkerClusterer'] = MarkerClusterer;
MarkerClusterer.prototype['addMarker'] = MarkerClusterer.prototype.addMarker;
MarkerClusterer.prototype['addMarkers'] = MarkerClusterer.prototype.addMarkers;
MarkerClusterer.prototype['clearMarkers'] = MarkerClusterer.prototype.clearMarkers;
MarkerClusterer.prototype['fitMapToMarkers'] = MarkerClusterer.prototype.fitMapToMarkers;
MarkerClusterer.prototype['getCalculator'] = MarkerClusterer.prototype.getCalculator;
MarkerClusterer.prototype['getGridSize'] = MarkerClusterer.prototype.getGridSize;
MarkerClusterer.prototype['getExtendedBounds'] = MarkerClusterer.prototype.getExtendedBounds;
MarkerClusterer.prototype['getMap'] = MarkerClusterer.prototype.getMap;
MarkerClusterer.prototype['getMarkers'] = MarkerClusterer.prototype.getMarkers;
MarkerClusterer.prototype['getMaxZoom'] = MarkerClusterer.prototype.getMaxZoom;
MarkerClusterer.prototype['getStyles'] = MarkerClusterer.prototype.getStyles;
MarkerClusterer.prototype['getTotalClusters'] = MarkerClusterer.prototype.getTotalClusters;
MarkerClusterer.prototype['getTotalMarkers'] = MarkerClusterer.prototype.getTotalMarkers;
MarkerClusterer.prototype['redraw'] = MarkerClusterer.prototype.redraw;
MarkerClusterer.prototype['removeMarker'] = MarkerClusterer.prototype.removeMarker;
MarkerClusterer.prototype['removeMarkers'] = MarkerClusterer.prototype.removeMarkers;
MarkerClusterer.prototype['resetViewport'] = MarkerClusterer.prototype.resetViewport;
MarkerClusterer.prototype['repaint'] = MarkerClusterer.prototype.repaint;
MarkerClusterer.prototype['setCalculator'] = MarkerClusterer.prototype.setCalculator;
MarkerClusterer.prototype['setGridSize'] = MarkerClusterer.prototype.setGridSize;
MarkerClusterer.prototype['setMaxZoom'] = MarkerClusterer.prototype.setMaxZoom;
MarkerClusterer.prototype['onAdd'] = MarkerClusterer.prototype.onAdd;
MarkerClusterer.prototype['draw'] = MarkerClusterer.prototype.draw;

Cluster.prototype['getCenter'] = Cluster.prototype.getCenter;
Cluster.prototype['getSize'] = Cluster.prototype.getSize;
Cluster.prototype['getMarkers'] = Cluster.prototype.getMarkers;

ClusterIcon.prototype['onAdd'] = ClusterIcon.prototype.onAdd;
ClusterIcon.prototype['draw'] = ClusterIcon.prototype.draw;
ClusterIcon.prototype['onRemove'] = ClusterIcon.prototype.onRemove;

Object.keys = Object.keys || function (o) {
    var result = [];
    for (var name in o) {
        if (o.hasOwnProperty(name)) result.push(name);
    }
    return result;
};

if ((typeof module === 'undefined' ? 'undefined' : _typeof(module)) == 'object') {
    module.exports = MarkerClusterer;
}

/**
* @memberof WIKI
* @ngdoc component
* @name wdExpansionPerson
*
* @description
* WIKI Component:<br>
*
* - person page
* - switch between search variants (precision/recall)
* - informations and links for the person
*
*
* <b>Requirements</b><br>
*
* Service {@link WIKI.wikidataService}<br>
* Service {@link WIKI.daasService}<br>
*
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.prmSearchResultListAfter}<br>
*
*
* <b>In Package</b><br>
*
* WIKI <br>
*
*/
app.component('ethWdExpansionPerson', {
    require: {
        parent: '^prmSearchResultListAfter'
    },
    template: '\n    <div class="wikidata-person" ng-if="$ctrl.person && $ctrl.person.item">\n        <a name="wikidata-person-searchprecision-anchor"></a>\n        <div class="wikidata-person-searchprecision">\n            <h2>{{$ctrl.person.label}}: {{$ctrl.label.precision[$ctrl.lang]}}?</h2>\n            <p class="wikidata-person-searchprecision__intro">\n                {{$ctrl.label.precisionIntro1[$ctrl.lang]}}\n                <span class="wikidata-person-searchprecision__gnd1"></span>\n                {{$ctrl.label.precisionIntro2[$ctrl.lang]}}\n                <b>{{$ctrl.label.precisionIntro3[$ctrl.lang]}}</b><br>\n                {{$ctrl.label.precisionIntro4[$ctrl.lang]}}\n            </p>\n            <div class="wikidata-person-searchprecision__options">\n                <div ng-if="$ctrl.person.gnd" class="wikidata-person-searchprecision__option-gnd">\n                    <md-button active="true" class="md-primary wikidata-person-searchprecision wikidata-person-searchprecision-gnd" ng-click="$ctrl.searchPersonByGndId($ctrl.person.gndid,$ctrl.person.qid)"><span class="wikidata-person-searchprecision__gnd2"></span> {{$ctrl.label.precisionGndTitle[$ctrl.lang]}}</md-button>\n                    <p>\n                        {{$ctrl.label.precisionGnd1[$ctrl.lang]}}\n                        <b>{{$ctrl.label.precisionGnd2[$ctrl.lang]}}</b>?\n                        {{$ctrl.label.precisionGnd3[$ctrl.lang]}}\n                    </p>\n                </div>\n                <div>\n                    <md-button class="md-primary wikidata-person-searchprecision wikidata-person-searchprecision-birthyear" ng-click="$ctrl.searchBestPossibleForPerson($ctrl.person);"><span class="wikidata-person-searchprecision__best"></span> {{$ctrl.label.precisionBestTitle[$ctrl.lang]}}</md-button>\n                    <p>\n                        {{$ctrl.label.precisionBest1[$ctrl.lang]}}\n                        <b>{{$ctrl.label.precisionBest2[$ctrl.lang]}}</b>\n                        {{$ctrl.label.precisionBest3[$ctrl.lang]}}\n                    </p>\n                </div>\n                <div>\n                    <md-button class="md-primary wikidata-person-searchprecision wikidata-person-searchprecision-label" ng-click="$ctrl.searchPersonByLabel($ctrl.person);"><span class="wikidata-person-searchprecision__name"></span> {{$ctrl.label.precisionNameTitle[$ctrl.lang]}}</md-button>\n                    <p>\n                        {{$ctrl.label.precisionName1[$ctrl.lang]}}\n                        <b>{{$ctrl.label.precisionName2[$ctrl.lang]}}</b>\n                        {{$ctrl.label.precisionName3[$ctrl.lang]}}\n                    </p>\n                </div>\n                <div>\n                    <md-button class="md-primary wikidata-person-searchprecision wikidata-person-searchprecision-variants" ng-click="$ctrl.searchPersonByVariantQuery($ctrl.person);"><span class="wikidata-person-searchprecision__variants"></span> {{$ctrl.label.precisionNameVariantsTitle[$ctrl.lang]}}</md-button>\n                    <p>\n                        {{$ctrl.label.precisionNameVariants1[$ctrl.lang]}}\n                        <b>{{$ctrl.label.precisionNameVariants2[$ctrl.lang]}}</b>\n                        {{$ctrl.label.precisionNameVariants3[$ctrl.lang]}}\n                    </p>\n                </div>\n            </div>\n        </div>\n        <a name="wikidata-person-anchor"></a>\n        <md-card md-theme="default" md-theme-watch>\n            <md-card-title>\n                <md-card-title-text>\n                    <!-- span class="wikidata-source-hint">Die folgenden Informationen sind aus Wikidata, Metagrid und der GND</span-->\n                    <h2 class="md-headline">{{$ctrl.person.itemLabel.value}}</h2>\n                </md-card-title-text>\n            </md-card-title>\n            <md-card-content>\n                <div class="wikidata-person__section wikidata-person__section-first">\n                    <div>\n                        <h3 class="md-subhead">{{$ctrl.label.infoWD[$ctrl.lang]}}</h3>\n                        <span>{{$ctrl.person.itemDescription.value}}</span>\n                        <span>{{$ctrl.person.professions.value}}</span>\n                        <span ng-if="$ctrl.person.gnd">GND ID: {{$ctrl.person.gnd.value}}</span>\n                        <span ng-if="$ctrl.person.birth">{{$ctrl.label.born[$ctrl.lang]}} {{$ctrl.person.birth.value.substring(0,$ctrl.person.birth.value.indexOf(\'T\'))}} in {{$ctrl.person.birthplaceLabel.value}}</span>\n                        <span ng-if="$ctrl.person.death">{{$ctrl.label.died[$ctrl.lang]}} {{$ctrl.person.death.value.substring(0,$ctrl.person.death.value.indexOf(\'T\'))}} in {{$ctrl.person.deathplaceLabel.value}}</span>\n                    </div>\n                    <img ng-if="$ctrl.person.image.value" class="md-card-image" ng-src="{{$ctrl.person.image.value}}?width=200px" />\n                </div>\n                <div class="wikidata-person__section" ng-if="$ctrl.person.entityfacts.biographicalOrHistoricalInformation">\n                    <h3 class="md-subhead">{{$ctrl.label.infoGND[$ctrl.lang]}}</h3>\n                    <span ng-if="$ctrl.person.entityfacts.biographicalOrHistoricalInformation">{{$ctrl.person.entityfacts.biographicalOrHistoricalInformation}}</span>\n                    <span ng-if="$ctrl.person.entityfacts.professionOrOccupation">{{$ctrl.person.entityfacts.professionOrOccupation}}</span>\n                    <span ng-if="$ctrl.person.entityfacts.dateOfBirth">{{$ctrl.label.born[$ctrl.lang]}} {{$ctrl.person.entityfacts.dateOfBirth}} in {{$ctrl.person.entityfacts.placeOfBirth}}</span>\n                    <span ng-if="$ctrl.person.entityfacts.dateOfDeath">{{$ctrl.label.died[$ctrl.lang]}} {{$ctrl.person.entityfacts.dateOfDeath}} in {{$ctrl.person.entityfacts.placeOfDeath}}</span>\n                </div>\n                <div ng-if="$ctrl.person.archived  && $ctrl.person.archived.length>0" class="wikidata-person__section">\n                    <h3 class="md-subhead">{{$ctrl.label.archives[$ctrl.lang]}}</h3>\n                    <p ng-repeat="object in $ctrl.person.archived">\n                        <span ng-if="!object.ref.value"">\n                            <span>{{object.archivedLabel.value}}</span>\n                            <span ng-if="object.inventoryno"> (Inventarnummer: {{object.inventoryno.value}})</span>\n                        </span>\n                        <a target="_blank" ng-if="object.ref.value" ng-href="{{object.ref.value}}">\n                            <span>{{object.archivedLabel.value}}</span>\n                            <span ng-if="object.inventoryno"> (Inventarnummer: {{object.inventoryno.value}})</span>\n                        </a>\n                    </p>\n                </div>\n                <div class="wikidata-person__section">\n                    <div ng-if="$ctrl.person.researcherProfile  && $ctrl.person.researcherProfile.length>0" class="wikidata-person__section">\n                        <h3 class="md-subhead">{{$ctrl.label.researcherProfile[$ctrl.lang]}}</h3>\n                        <a ng-repeat="object in $ctrl.person.researcherProfile" target="_blank" ng-href="{{object.url}}">\n                            {{object.label}}\n                        </a>\n                    </div>\n                    <h3 class="md-subhead">{{$ctrl.label.linksWD[$ctrl.lang]}}</h3>\n                        <a target="_blank" href="{{$ctrl.person.item.value}}">Wikidata</a>\n                        <a ng-if="$ctrl.person.wc" target="_blank" href="https://commons.wikimedia.org/wiki/Category:{{$ctrl.person.wc.value}}">Wikimedia Commons</a>\n                        <a ng-if="$ctrl.person.hls" target="_blank" href="http://www.hls-dhs-dss.ch/textes/d/D{{$ctrl.person.hls.value}}.php">HLS (Historisches Lexikon der Schweiz)</a>\n                        <a ng-if="$ctrl.person.viaf" target="_blank" href="https://viaf.org/viaf/{{$ctrl.person.viaf.value}}/">VIAF (Virtual International Authority File)</a>\n                        <a ng-if="$ctrl.person.gnd" target="_blank" href="https://d-nb.info/gnd/{{$ctrl.person.gnd.value}}">GND der DNB (Deutsche Nationalbibliothek)</a>\n                        <a ng-if="$ctrl.person.sfa" target="_blank" href="https://www.swiss-archives.ch/archivplansuche.aspx?ID={{$ctrl.person.sfa.value}}">SBA (Schweizerisches Bundesarchiv)</a>\n                        <a ng-if="$ctrl.person.dodis" target="_blank" href="https://dodis.ch/{{$ctrl.person.dodis.value}}">DODIS (Diplomatische Dokumente der Schweiz)</a>\n                        <a ng-if="$ctrl.person.bdel" target="_blank" href="https://www2.unil.ch/elitessuisses/index.php?page=detailPerso&idIdentite={{$ctrl.person.bdel.value}}">BDEL (Schweizerische Eliten im 20. Jahrhundert)</a>\n                        <a ng-if="$ctrl.person.loc" target="_blank" href="http://id.loc.gov/authorities/names/{{$ctrl.person.loc.value}}.html">LOC (Library of Congress)</a>\n                        <a ng-if="$ctrl.person.cerl" target="_blank" href="https://data.cerl.org/thesaurus/{{$ctrl.person.cerl.value}}">CERL Thesaurus</a>\n                    <div ng-if="$ctrl.person.metagrid && $ctrl.person.metagrid.length>0">\n                        <h3 class="md-subhead">{{$ctrl.label.linksMetagrid[$ctrl.lang]}}</h3>\n                        <p class="wikidata-person__links-hint">Links powered by <a target="_blank" href="https://www.metagrid.ch/" class="wikidata-person__links-source md-default-theme">Metagrid</a></p>\n                        <a target="_blank" ng-repeat="object in $ctrl.person.metagrid" ng-href="{{object.url}}">{{object.label}}</a>\n                    </div>\n                    <div ng-if="$ctrl.person.findbuchLinks && $ctrl.person.findbuchLinks.length>0">\n                        <h3 class="md-subhead">{{$ctrl.label.linksFindbuch[$ctrl.lang]}}</h3>\n                        <p class="wikidata-person__links-hint">\n                            {{$ctrl.label.linksFindbuchHint1[$ctrl.lang]}}\n                            <a target="_blank" href="http://beacon.findbuch.de/seealso/pnd-aks" class="wikidata-person__links-source md-default-theme">pnd-aks</a>,\n                            {{$ctrl.label.linksFindbuchHint2[$ctrl.lang]}}\n                            <a target="_blank" href="https://de.wikipedia.org/wiki/Wikipedia:BEACON" class="wikidata-person__links-source md-default-theme">BEACON</a>\n                            {{$ctrl.label.linksFindbuchHint3[$ctrl.lang]}}\n                        </p>\n                        <a target="_blank" ng-repeat="object in $ctrl.person.findbuchLinks" ng-href="{{object.url}}">{{object.label}}</a>\n                    </div>\n                </div>\n\n                <div ng-if="$ctrl.person.students  && $ctrl.person.students.length>0" class="wikidata-person__section">\n                    <h3 class="md-subhead">{{$ctrl.label.students[$ctrl.lang]}}</h3>\n                    <p class="wikidata-person-small" ng-repeat="object in $ctrl.person.students">\n                        <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf(\'/\')+1))">\n                            <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>\n                            <span class="wikidata-person-small__text">{{object.itemLabel.value}} ({{object.itemDescription.value}}, {{$ctrl.label.birthday[$ctrl.lang]}}: {{object.births.value.substring(0,object.births.value.indexOf(\'T\'))}})</span>\n                        </a>\n                    </p>\n                </div>\n                <div ng-if="$ctrl.person.teachers  && $ctrl.person.teachers.length > 0" class="wikidata-person__section">\n                    <h3 class="md-subhead">{{$ctrl.label.teachers[$ctrl.lang]}}</h3>\n                    <p class="wikidata-person-small" ng-repeat="object in $ctrl.person.teachers">\n                        <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf(\'/\')+1))">\n                            <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>\n                            <span class="wikidata-person-small__text">{{object.itemLabel.value}} ({{object.itemDescription.value}}, {{$ctrl.label.birthday[$ctrl.lang]}}: {{object.births.value.substring(0,object.births.value.indexOf(\'T\'))}})</span>\n                        </a>\n                    </p>\n                </div>\n            </md-card-content>\n        </md-card>\n    </div>\n    ',

    controller: function () {
        function WdExpansionPersonController(wikidataService, daasService, $location, $element, $scope, $compile, $anchorScroll, $timeout, $rootScope) {
            _classCallCheck(this, WdExpansionPersonController);

            this.wikidataService = wikidataService;
            this.daasService = daasService;
            this.$location = $location;
            this.$element = $element;
            this.$scope = $scope;
            this.$compile = $compile;
            this.$anchorScroll = $anchorScroll;
            this.$timeout = $timeout;
            this.$rootScope = $rootScope;
            this.label = {
                results: {
                    de_DE: 'Ergebnisse',
                    en_US: 'results'
                },
                precision: {
                    de_DE: 'Genauere oder mehr Ergebnisse',
                    en_US: 'More precise or more results'
                },
                moreInformations: {
                    de_DE: 'Mehr Informationen zur Person',
                    en_US: 'More information about the person'
                },
                infoWD: {
                    de_DE: 'Informationen aus Wikidata',
                    en_US: 'Informations from Wikidata'
                },
                infoGND: {
                    de_DE: 'Informationen aus der GND (Deutsche Nationalbibliothek)',
                    en_US: 'Informations from the GND (Deutsche Nationalbibliothek)'
                },
                archives: {
                    de_DE: 'Archive',
                    en_US: 'Archives'
                },
                researcherProfile: {
                    de_DE: 'Forscherprofile',
                    en_US: 'Researcher profiles'
                },
                linksWD: {
                    de_DE: 'Externe Links aus Wikidata',
                    en_US: 'External links from Wikidata'
                },
                linksMetagrid: {
                    de_DE: 'Externe Links von Metagrid',
                    en_US: 'External links from Metagrid'
                },
                linksFindbuch: {
                    de_DE: 'Externe Links von beacon.findbuch',
                    en_US: 'External links from beacon.findbuch'
                },
                linksFindbuchHint1: {
                    de_DE: 'Links von dem SeeAlso-Service',
                    en_US: 'Links from the SeeAlso-Service'
                },
                linksFindbuchHint2: {
                    de_DE: 'der auf',
                    en_US: 'which is based on'
                },
                linksFindbuchHint3: {
                    de_DE: 'basiert',
                    en_US: ''
                },
                students: {
                    de_DE: 'Bedeutende Schüler oder Doktoranden (aus Wikidata)',
                    en_US: 'Important students or doctoral students (from Wikidata)'
                },
                teachers: {
                    de_DE: 'Lehrer (aus Wikidata)',
                    en_US: 'Teachers (from Wikidata)'
                },
                born: {
                    de_DE: 'Geboren am',
                    en_US: 'Born on'
                },
                died: {
                    de_DE: 'Gestorben am',
                    en_US: 'Died on'
                },
                precisionIntro1: {
                    de_DE: 'Sie sehen',
                    en_US: 'You see'
                },
                precisionIntro2: {
                    de_DE: 'Ergebnisse der',
                    en_US: 'results of the'
                },
                precisionIntro3: {
                    de_DE: 'bei Suche nach einer eindeutigen Identifikationsnummer der Person (GND).',
                    en_US: 'in search for a unique identification number of the person (GND).'
                },
                precisionIntro4: {
                    de_DE: 'Sie sehen möglicherweise nicht alle Ergebnisse, da die Identifikationsnummer der Person nicht bei allen Ressourcen zugeordnet ist.',
                    en_US: 'You may not see all results because the identification number of the person is not assigned to all resources.However, you can expand the search to see more results.'
                },
                precisionGndTitle: {
                    de_DE: 'Nur nach eindeutiger Identifikationsnummer (GND) suchen',
                    en_US: 'Search only for unique identification number (GND)'
                },
                precisionGnd1: {
                    de_DE: 'Wollen Sie nach der',
                    en_US: 'Do you want to search for'
                },
                precisionGnd2: {
                    de_DE: 'eindeutigen Identifikationsnummer der Person (GND) suchen',
                    en_US: 'the unique identification number of the person (GND)'
                },
                precisionGnd3: {
                    de_DE: 'Sie sehen möglicherweise nicht alle Ergebnisse, da die Identifikationsnummer nicht bei allen Ressourcen zugeordnet ist.',
                    en_US: 'You may not see all results because the identification number is not assigned to all resources.'
                },
                precisionBestTitle: {
                    de_DE: 'bei Suche nach Name und Geburtsjahr oder eindeutiger Identifikationsnummer (GND)',
                    en_US: 'in search by name and year of birth or unique identification number (GND)'
                },
                precisionBest1: {
                    de_DE: 'Wollen Sie nach dem',
                    en_US: 'Do you want to search for the'
                },
                precisionBest2: {
                    de_DE: 'Namen suchen, eingeschränkt durch Identifikationsnummer der Person (GND) oder das Geburtsjahr',
                    en_US: 'name, restricted by the identification number of the person (GND) or the year of birth'
                },
                precisionBest3: {
                    de_DE: 'und damit mehr Ergebnisse sehen? Sie sehen möglicherweise nicht alle Ergebnisse, da Geburtsjahr oder Identifikationsnummer nicht allen Ressourcen zugeordnet sind.',
                    en_US: 'and see more results? You may not see all results because the birth year or identification number is not assigned to all resources.'
                },
                precisionNameTitle: {
                    de_DE: 'bei Suche nach Name',
                    en_US: 'in search by name'
                },
                precisionName1: {
                    de_DE: 'Wollen Sie',
                    en_US: 'Do you want to search'
                },
                precisionName2: {
                    de_DE: 'nur nach dem Namen',
                    en_US: 'by name only'
                },
                precisionName3: {
                    de_DE: 'suchen? Sie sehen möglicherweise auch falsche Ergebnisse, wenn mehrere Personen denselben Namen tragen.',
                    en_US: 'You may also see wrong results if several people have the same name.'
                },
                precisionNameVariantsTitle: {
                    de_DE: 'bei Suche nach Namensvarianten',
                    en_US: 'in search by name variants'
                },
                precisionNameVariants1: {
                    de_DE: 'Wollen Sie nach',
                    en_US: 'Do you want to search for'
                },
                precisionNameVariants2: {
                    de_DE: 'dem Namen und den Namensvarianten (aus Wikidata)',
                    en_US: 'the name and the name variants (from Wikidata)'
                },
                precisionNameVariants3: {
                    de_DE: 'suchen? Sie sehen möglicherweise auch falsche Ergebnisse, wenn mehrere Personen einen der Namen tragen.',
                    en_US: '? You may also see wrong results if several people have one of the names.'
                },
                birthday: {
                    de_DE: 'Geburtstag',
                    en_US: 'Birthday'
                }
            };
        }

        _createClass(WdExpansionPersonController, [{
            key: '$onInit',
            value: function $onInit() {
                this.parentCtrl = this.parent.parentCtrl;
                // this.search: input in searchField
                this.search = '';
                this.person = {};
                this.lang = this.getLanguage(this.$rootScope);
                var query = this.parentCtrl.searchService.searchUtil.$stateParams.query;
                if (query.indexOf('lsr04') > -1) {
                    this.search = query.replace('lsr04,contains,', '');
                } else {
                    this.search = query.replace('any,contains,', '');
                }

                // check querystring for searchfor
                var searchFor = this.$location.search().searchfor;

                // call by searchpage
                if (searchFor && searchFor.indexOf('person/') > -1) {
                    this.getPerson(searchFor.substring(searchFor.indexOf('/') + 1));
                }
                // permalink
                else if (this.search.indexOf('[wd/person]') > -1) {
                        this.getPerson(this.search.substring(this.search.indexOf(']') + 1));
                    }
            }
        }, {
            key: 'getPerson',
            value: function getPerson(qid) {
                var _this = this;

                this.wikidataService.getPersonGndByQID(qid, this.lang)
                // do a quick search for item and gnd
                .then(function (data) {
                    if (!data.results || !data.results.bindings || data.results.bindings.length === 0) return null;
                    if (_this.search.indexOf('[wd/person]') > -1) {
                        if (data.results.bindings[0].gnd) {
                            var gndid = data.results.bindings[0].gnd.value;
                            _this.parentCtrl.searchService.searchFieldsService.mainSearch = gndid;
                            _this.$timeout(function () {
                                _this.parentCtrl.searchService.search({ query: 'lsr04,contains,' + gndid }, true);
                            }, 300);
                        } else {
                            var label = data.results.bindings[0].itemLabel.value;
                            _this.parentCtrl.searchService.searchFieldsService.mainSearch = label;
                            _this.$timeout(function () {
                                _this.parentCtrl.searchService.search({ query: 'any,contains,' + label }, true);
                            }, 300);
                        }
                    }
                    return _this.wikidataService.getPersonByQID(qid, _this.lang);
                })
                // get additional informations
                .then(function (data) {
                    if (!data || !data.results || !data.results.bindings || data.results.bindings.length === 0) return null;
                    // set properties of person object
                    _this.person = data.results.bindings[0];

                    // name variants / aliases
                    var aVariants = data.results.bindings[0].aliasList.value.split('|');
                    if (aVariants.indexOf(_this.person.itemLabel.value) === -1) {
                        aVariants.unshift(_this.person.itemLabel.value);
                    }
                    _this.person.variantQuery = _this.buildPrimoQueryValue(aVariants);
                    if (_this.person.gnd) {
                        _this.person.gndid = _this.person.gnd.value;
                    }
                    _this.person.label = _this.person.itemLabel.value;
                    _this.person.qid = qid;
                    _this.person.yearOfBirth = _this.person.birth ? _this.person.birth.value.substring(0, _this.person.birth.value.indexOf('-')) : null;

                    // render precision and recall navigation
                    try {
                        _this.enrichPRNavigation();
                    } catch (e) {
                        throw e;
                        console.error("***ETH*** an error occured: wdExpansionPerson.enrichPRNavigation");
                        console.error(e.message);
                    }
                    return _this.wikidataService.getResearcherProfile(qid, _this.lang);
                }).then(function (data) {
                    if (!data || !data.results || !data.results.bindings || data.results.bindings.length === 0) return null;
                    _this.person.researcherProfile = [];
                    if (data.results.bindings[0].orcid && data.results.bindings[0].orcid.value) {
                        _this.person.researcherProfile.push({ 'orcid': data.results.bindings[0].orcid.value, 'label': 'ORCID ID', 'url': 'https://orcid.org/' + data.results.bindings[0].orcid.value });
                    }
                    if (data.results.bindings[0].publonid && data.results.bindings[0].publonid.value) {
                        _this.person.researcherProfile.push({ 'publonid': data.results.bindings[0].publonid.value, 'label': 'Publons author ID', 'url': 'https://publons.com/researcher/' + data.results.bindings[0].publonid.value });
                    }
                    if (data.results.bindings[0].researcherid && data.results.bindings[0].researcherid.value) {
                        _this.person.researcherProfile.push({ 'researcherid': data.results.bindings[0].researcherid.value, 'label': 'ResearcherID', 'url': 'https://www.researcherid.com/rid/' + data.results.bindings[0].researcherid.value });
                    }
                    if (data.results.bindings[0].scholar && data.results.bindings[0].scholar.value) {
                        _this.person.researcherProfile.push({ 'scholar': data.results.bindings[0].scholar.value, 'label': 'Google Scholar author ID', 'url': 'https://scholar.google.com/citations?user=' + data.results.bindings[0].scholar.value });
                    }
                    if (data.results.bindings[0].scopus && data.results.bindings[0].scopus.value) {
                        _this.person.researcherProfile.push({ 'scopus': data.results.bindings[0].scopus.value, 'label': 'Scopus author ID', 'url': 'https://www.scopus.com/authid/detail.uri?authorId=' + data.results.bindings[0].scopus.value });
                    }
                    return _this.person.qid;
                }).then(function (qid) {
                    if (!qid) return null;

                    if (_this.person.gnd) {
                        _this.daasService.searchEntityfacts(_this.person.gnd.value).then(function (data) {
                            _this.person.entityfacts = data;
                        });
                    }
                    if (_this.person.archived && _this.person.archived.value && _this.person.archived.value != '') {
                        _this.wikidataService.getArchivedOfPerson(qid, _this.lang).then(function (data) {
                            if (data.results && data.results.bindings) {
                                _this.person.archived = data.results.bindings;
                            } else {
                                _this.person.archived = null;
                            }
                        });
                    }
                    _this.wikidataService.getStudentsOfPerson(qid, _this.lang).then(function (data) {
                        if (data.results && data.results.bindings) {
                            _this.person.students = data.results.bindings;
                        } else {
                            _this.person.students = null;
                        }
                    });

                    _this.wikidataService.getTeachersOfPerson(qid, _this.lang).then(function (data) {
                        if (data.results && data.results.bindings) {
                            _this.person.teachers = data.results.bindings;
                        } else {
                            _this.person.teachers = null;
                        }
                    });

                    if (_this.person.gnd) {
                        _this.daasService.searchMetagrid(_this.person.gnd.value).then(function (data) {
                            if (!data || !data.resources) return;
                            var sourcesWhitelist = ["sikart", "elites-suisses-au-xxe-siecle", "bsg", "dodis", "helveticarchives", "helveticat", "hls-dhs-dss", "ethz", "histoirerurale", "lonsea", "ssrq", "alfred-escher", "geschichtedersozialensicherheit", "history-state", "rag"];
                            var whitelistedMetagridLinks = [];
                            for (var j = 0; j < data.resources.length; j++) {
                                var resource = data.resources[j];
                                // https://api.metagrid.ch/providers.json
                                var slug = resource.slug;
                                var url = resource.uri;
                                if (sourcesWhitelist.indexOf(slug) === -1) {
                                    continue;
                                }
                                var label = slug;
                                if (slug === 'elites-suisses-au-xxe-siecle') {
                                    label = "Elites suisses au XXe siècle";
                                } else if (slug === 'sikart') {
                                    label = "SIKART Lexikon zur Kunst in der Schweiz";
                                } else if (slug === 'history-state') {
                                    label = "FRUS (Office of the Historian)";
                                } else if (slug === 'rag') {
                                    label = "Repertorium Academicum Germanicum";
                                } else if (slug === 'bsg') {
                                    label = "Bibliographie der Schweizergeschichte";
                                } else if (slug === 'dodis') {
                                    label = "Diplomatische Dokumente der Schweiz";
                                } else if (slug === 'helveticarchives') {
                                    label = "HelveticArchives";
                                } else if (slug === 'helveticat') {
                                    label = "Helveticat";
                                } else if (slug === 'hls-dhs-dss') {
                                    label = "Historisches Lexikon der Schweiz";
                                } else if (slug === 'ethz') {
                                    label = "ETH Zürich Hochschularchiv";
                                } else if (slug === 'histoirerurale') {
                                    label = "Archiv für Agrargeschichte";
                                } else if (slug === 'lonsea') {
                                    label = "Lonsea";
                                } else if (slug === 'ssrq') {
                                    label = "Sammlung Schweizerischer Rechtsquellen";
                                } else if (slug === 'alfred-escher') {
                                    label = "Alfred Escher-Briefedition";
                                } else if (slug === 'geschichtedersozialensicherheit') {
                                    label = "Geschichte der sozialen Sicherheit";
                                }
                                whitelistedMetagridLinks.push({ 'slug': slug, 'url': url, 'label': label });
                            }
                            _this.person.metagrid = whitelistedMetagridLinks;
                        });
                        _this.daasService.searchFindbuch(_this.person.gnd.value).then(function (data) {
                            if (!data.uris) return;
                            var sourcesWhitelist = ["ba.e-pics.ethz.ch", "tools.wmflabs.org/reasonator", "e-codices.unifr.ch", "deutsche-biographie.de", "deutsche-digitale-bibliothek.de", "kalliope.staatsbibliothek-berlin.de", "tools.wmflabs.org/persondata/redirect/gnd/de/", "tools.wmflabs.org/persondata/redirect/gnd/en/", "tools.wmflabs.org/persondata/redirect/gnd/commons/", "www.hdg.de/lemo/html/biografien/", "beacon.findbuch.de/gnd-resolver/pw_tls/", "www.perlentaucher.de/autor/", "archinform.net/gnd/", "www.gutenberg.org/ebooks/author/", "beacon.findbuch.de/gnd-resolver/pw_mactut/", "beacon.findbuch.de/gnd-resolver/pw_imdb/"];
                            var whitelistedFindbuchLinks = [];
                            var alreadyFoundUrls = [];
                            _this.person.findbuchLinks = [];
                            for (var j = 0; j < data.uris.length; j++) {
                                var isWhitelisted = false;
                                var url = data.uris[j];
                                var path = url;

                                if (url.indexOf("?") > -1) path = url.substring(0, url.indexOf("?"));
                                // Für NDB und ADB nur einen Link
                                if (url.indexOf("#ndbcontent") > -1) path = url.substring(0, url.indexOf("#ndbcontent"));else if (url.indexOf("#adbcontent") > -1) path = url.substring(0, url.indexOf("#adbcontent"));else if (url.indexOf("#allcontent") > -1) path = url.substring(0, url.indexOf("#allcontent"));

                                if (url.indexOf("gnd-resolver/pw_tls/") > -1) {
                                    path = url.substring(0, url.indexOf("pw_tls/") + 7);
                                }
                                for (var k = 0; k < sourcesWhitelist.length; k++) {
                                    if (url.indexOf(sourcesWhitelist[k]) > -1) isWhitelisted = true;
                                }
                                if (!isWhitelisted || alreadyFoundUrls.indexOf(path) > -1) continue;
                                var label = data.labels[j];
                                _this.person.findbuchLinks.push({ 'url': url, 'label': label });
                                alreadyFoundUrls.push(path);
                            }
                        });
                    }
                }).catch(function (e) {
                    throw e;
                    console.error("***ETH*** an error occured: wdExpansionPerson.getPerson");
                    console.error(e.message);
                });
            }
        }, {
            key: 'buildPrimoQueryValue',
            value: function buildPrimoQueryValue(items) {
                // in primo 7 rows and max 30 boolean operators
                /*
                The system will display a message and provide suggestions when the following limits are exceeded:
                The query contains more than 30 boolean operators.
                The query contains more than 8 question marks.
                The query contains more than 8 asterisks and the word length is greater than 2 (such as abb* or ab*c).
                The query contains more than 4 asterisks and the word length is less than 3 (such as ab*).
                The entire query consists of a single letter and an asterisk (such as a*).
                */
                var query = '';
                var maxItems = 20;
                var maxLength = 1000;
                items.forEach(function (e, i) {
                    if (i < maxItems) {
                        if (query.length + e.length < maxLength) {
                            query += e;
                            if (i < items.length - 1 && i < maxItems - 1) {
                                query += ' OR ';
                            }
                        }
                    }
                });
                return query;
            }

            // enrich precision and recall navigation

        }, {
            key: 'enrichPRNavigation',
            value: function enrichPRNavigation() {
                var _this2 = this;

                var resultList = angular.element(this.$element[0].parentElement.parentElement);
                if (resultList) {
                    var html = '\n                    <a name="wikidata-top-anchor"></a>\n                    <div class="wikidata-person-top">\n                        <a class="wikidata-anchor-link" ng-click="$ctrl.scrollTo(\'wikidata-person-searchprecision-anchor\')">\n                            {{$ctrl.label.precision[$ctrl.lang]}}?\n                        </a>\n                        <a class="wikidata-anchor-link" ng-click="$ctrl.scrollTo(\'wikidata-person-anchor\')">{{$ctrl.label.moreInformations[$ctrl.lang]}} "' + this.person.label + '"</a>\n                    </div>\n                ';
                    resultList.prepend(this.$compile(html)(this.$scope));

                    // determine number of results of search variants
                    // search for label
                    this.daasService.searchPrimo('any,contains,' + this.person.label).then(function (data) {
                        if (document.getElementsByClassName('wikidata-person-searchprecision__name').length > 0) {
                            document.getElementsByClassName('wikidata-person-searchprecision__name')[0].innerHTML = data.info.total + ' ' + _this2.label.results[_this2.lang];
                        }
                    });
                    // search for gnd
                    if (this.person.gndid) {
                        this.daasService.searchPrimo('lsr04,contains,' + this.person.gndid).then(function (data) {
                            if (document.getElementsByClassName('wikidata-person-searchprecision__gnd1').length > 0) {
                                document.getElementsByClassName('wikidata-person-searchprecision__gnd1')[0].innerHTML = data.info.total;
                            }
                            if (document.getElementsByClassName('wikidata-person-searchprecision__gnd2').length > 0) {
                                document.getElementsByClassName('wikidata-person-searchprecision__gnd2')[0].innerHTML = data.info.total + ' ' + _this2.label.results[_this2.lang];
                            }
                        });
                    } else {
                        if (document.getElementsByClassName('wikidata-person-searchprecision__gnd1').length > 0) {
                            document.getElementsByClassName('wikidata-person-searchprecision__gnd1')[0].innerHTML = '0';
                        }
                        if (document.getElementsByClassName('wikidata-person-searchprecision__gnd2').length > 0) {
                            document.getElementsByClassName('wikidata-person-searchprecision__gnd2')[0].innerHTML = '0 ' + this.label.results[this.lang];
                        }
                    }
                    // search for name variants / aliases
                    this.daasService.searchPrimo('any,contains,' + this.person.variantQuery).then(function (data) {
                        if (document.getElementsByClassName('wikidata-person-searchprecision__variants').length > 0) {
                            document.getElementsByClassName('wikidata-person-searchprecision__variants')[0].innerHTML = data.info.total + ' ' + _this2.label.results[_this2.lang];
                        }
                    });
                    // search for name AND (GND OR BIRTHYEAR)
                    var primoServiceQuery = this.person.label;;
                    if (this.person.yearOfBirth && this.person.gndid) {
                        primoServiceQuery = primoServiceQuery + ' AND (' + this.person.yearOfBirth + ' OR ' + this.person.gndid + ')';
                    } else if (this.person.yearOfBirth) {
                        primoServiceQuery = primoServiceQuery + ' ' + this.person.yearOfBirth;
                    } else if (this.person.gndid) {
                        primoServiceQuery = primoServiceQuery + ' ' + this.person.gndid;
                    }
                    this.daasService.searchPrimo('any,contains,' + primoServiceQuery).then(function (data) {
                        if (document.getElementsByClassName('wikidata-person-searchprecision__best').length > 0) {
                            document.getElementsByClassName('wikidata-person-searchprecision__best')[0].innerHTML = data.info.total + ' ' + _this2.label.results[_this2.lang];
                        }
                    });
                }
            }
        }, {
            key: 'scrollTo',
            value: function scrollTo(anchorName) {
                this.$location.hash(anchorName);
                this.$anchorScroll();
            }
        }, {
            key: 'searchPersonByLabel',
            value: function searchPersonByLabel(person) {
                var query = person.label;
                this.parentCtrl.searchService.search({ query: 'any,contains,' + query }, true);
                this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
                var buttons = document.getElementsByClassName('wikidata-person-searchprecision');
                for (var i = 0; i < buttons.length; i++) {
                    buttons[i].removeAttribute('active');
                }
                document.getElementsByClassName('wikidata-person-searchprecision__intro')[0].style.display = 'none';
                document.getElementsByClassName('wikidata-person-searchprecision__option-gnd')[0].style.display = 'block';
                document.getElementsByClassName('wikidata-person-searchprecision-label')[0].setAttribute('active', 'true');
                this.scrollTo('wikidata-top-anchor');
            }
        }, {
            key: 'searchPersonByGndId',
            value: function searchPersonByGndId(gndid, qid) {
                var query = gndid;
                this.parentCtrl.searchService.search({ query: 'lsr04,contains,' + query }, true);
                this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
                var buttons = document.getElementsByClassName('wikidata-person-searchprecision');
                for (var i = 0; i < buttons.length; i++) {
                    buttons[i].removeAttribute('active');
                }
                document.getElementsByClassName('wikidata-person-searchprecision__intro')[0].style.display = 'none';
                document.getElementsByClassName('wikidata-person-searchprecision__option-gnd')[0].style.display = 'block';
                document.getElementsByClassName('wikidata-person-searchprecision-gnd')[0].setAttribute('active', 'true');
                this.scrollTo('wikidata-top-anchor');
            }
        }, {
            key: 'loadPersonPageByGndId',
            value: function loadPersonPageByGndId(gndid, qid) {
                var query = '[wd/person]' + qid;
                var url = '/primo-explore/search?query=any,contains,' + query + '&tab=default_tab&vid=WIKI&lang=' + this.lang;
                location.href = url;
            }
        }, {
            key: 'searchPersonByVariantQuery',
            value: function searchPersonByVariantQuery(person) {
                var query = person.variantQuery;
                if (!person.variantQuery) {
                    query = person.label;
                }
                this.parentCtrl.searchService.search({ query: 'any,contains,' + query }, true);
                this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
                var buttons = document.getElementsByClassName('wikidata-person-searchprecision');
                for (var i = 0; i < buttons.length; i++) {
                    buttons[i].removeAttribute('active');
                }
                document.getElementsByClassName('wikidata-person-searchprecision__intro')[0].style.display = 'none';
                document.getElementsByClassName('wikidata-person-searchprecision__option-gnd')[0].style.display = 'block';
                document.getElementsByClassName('wikidata-person-searchprecision-variants')[0].setAttribute('active', 'true');
                this.scrollTo('wikidata-top-anchor');
            }
        }, {
            key: 'searchBestPossibleForPerson',
            value: function searchBestPossibleForPerson(person) {
                var query = person.label;
                if (person.yearOfBirth && person.gndid) {
                    query = query + ' AND (' + person.yearOfBirth + ' OR ' + person.gndid + ')';
                } else if (person.yearOfBirth) {
                    query = query + ' ' + person.yearOfBirth;
                } else if (person.gndid) {
                    query = query + ' ' + person.gndid;
                }
                this.parentCtrl.searchService.search({ query: 'any,contains,' + query }, true);
                this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
                var buttons = document.getElementsByClassName('wikidata-person-searchprecision');
                for (var i = 0; i < buttons.length; i++) {
                    buttons[i].removeAttribute('active');
                }
                document.getElementsByClassName('wikidata-person-searchprecision__intro')[0].style.display = 'none';
                document.getElementsByClassName('wikidata-person-searchprecision__option-gnd')[0].style.display = 'block';
                document.getElementsByClassName('wikidata-person-searchprecision-birthyear')[0].setAttribute('active', 'true');
                this.scrollTo('wikidata-top-anchor');
            }
        }, {
            key: 'loadPlacePage',
            value: function loadPlacePage(qid, label) {
                var query = '[wd/place]' + qid;
                var url = '/primo-explore/search?query=any,contains,' + query + '&tab=default_tab&vid=WIKI&lang=' + this.lang;
                location.href = url;
            }
        }, {
            key: 'getLanguage',
            value: function getLanguage() {
                var sms = this.$rootScope.$$childHead.$ctrl.userSessionManagerService;
                if (!sms) {
                    console.error("***ETH*** WdExpansionPersonController: userSessionManagerService not available");
                    return 'de';
                } else {
                    return sms.getUserLanguage() || $window.appConfig['primo-view']['attributes-map'].interfaceLanguage;
                }
            }
        }]);

        return WdExpansionPersonController;
    }()
});
/*
<div ng-if="$ctrl.person.places && $ctrl.person.places.length>0" class="wikidata-person__section">
    <h3 class="md-subhead">Orte (aus Wikidata)</h3>
    <p class="wikidata-place-small" ng-repeat="object in $ctrl.person.places">
        <a target="_blank" ng-click="$ctrl.loadPlacePage(object.place.value.substring(object.place.value.lastIndexOf('/')+1),object.placeLabel.value);">
            <span class="wikidata-place-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
            <span class="wikidata-place-small__text">{{object.placeLabel.value}} ({{object.translatedPred}})</span>
        </a>
    </p>
</div>
*/
/*
this.wikidataService.getPlacesOfPerson(qid)
    .then((data) => {
        this.person.places = data.results.bindings;
        this.person.places.forEach( (e,i) => {
            e.translatedPred = '';
            if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P19'){
                e.translatedPred = 'Geburtsort'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P20'){
                e.translatedPred = 'Sterbeort'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P69'){
                e.translatedPred = 'besuchte Bildungseinrichtung'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P108'){
                e.translatedPred = 'Arbeitgeber'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P119'){
                e.translatedPred = 'Begräbnisort'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P551'){
                e.translatedPred = 'Wohnsitz'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P937'){
                e.translatedPred = 'Wirkungsort'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P1321'){
                e.translatedPred = 'Heimatort'
            }
        })
    })
*/

/**
* @memberof WIKI
* @ngdoc component
* @name wdExpansionPersons
*
* @description
* WIKI Component:<br>
*
* - Query expansion via wikidata: persons for searchterm
*
* <b>Requirements</b><br>
*
* Service {@link WIKI.wikidataService}<br>
*
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.prmSearchResultListAfter}<br>
*
*
* <b>In Package</b><br>
*
* WIKI <br>
*
*/
app.component('ethWdExpansionPersons', {
    require: {
        parent: '^prmSearchResultListAfter'
    },
    template: '\n    <div class="wikidata-persons" ng-if="$ctrl.persons.length > 0">\n        <a name="wikidata-persons-anchor"></a>\n        <md-card ng-repeat="person in $ctrl.persons" md-theme="default" md-theme-watch>\n            <md-card-title>\n                <md-card-title-text>\n                    <span class="md-headline">{{person.label}}</span>\n                    <span class="md-subhead">{{person.description}}</span>\n                    <span class="md-subhead">{{person.professions}}</span>\n                    <span class="md-subhead" ng-if="person.gnd">GND ID: {{person.gndid}}</span>\n                    <span class="md-subhead">{{$ctrl.label.birthday[$ctrl.lang]}}: {{person.birth}}</span>\n                </md-card-title-text>\n                <md-card-title-media ng-if="person.image">\n                    <img class="md-card-image" ng-src="{{person.image}}?width=200px" />\n                </md-card-title-media>\n            </md-card-title>\n            <md-card-actions layout-xs="column" layout="row" layout-align="start center" layout-align-xs="start start">\n                <md-button class="md-primary" ng-if="person.gndid" ng-click="$ctrl.loadPersonPageByGndId(person.gndid,person.qid)">{{$ctrl.label.searchThis[$ctrl.lang]}}</md-button>\n            </md-card-actions>\n        </md-card>\n    </div>\n    ',
    controller: function () {
        function WdExpansionPersonsController(wikidataService, $location, $element, $scope, $compile, $anchorScroll, $rootScope) {
            _classCallCheck(this, WdExpansionPersonsController);

            this.wikidataService = wikidataService;
            this.$location = $location;
            this.$element = $element;
            this.$scope = $scope;
            this.$compile = $compile;
            this.$anchorScroll = $anchorScroll;
            this.$rootScope = $rootScope;
            this.label = {
                persons: {
                    de_DE: 'Personen zu meinem Suchbegriff',
                    en_US: 'Persons matching my search term'
                },
                searchThis: {
                    de_DE: 'Diese Person suchen',
                    en_US: 'Search for this person'
                },
                birthday: {
                    de_DE: 'Geburtstag',
                    en_US: 'Birthday'
                }
            };
        }

        _createClass(WdExpansionPersonsController, [{
            key: '$onInit',
            value: function $onInit() {
                this.parentCtrl = this.parent.parentCtrl;
                // this.search: input in searchField
                this.search = '';
                // this.persons: object array of persons
                this.persons = [];
                this.lang = this.getLanguage(this.$rootScope);

                var query = this.parentCtrl.searchService.searchUtil.$stateParams.query;
                if (query.indexOf('lsr04') > -1) {
                    this.search = query.replace('lsr04,contains,', '');
                } else {
                    this.search = query.replace('any,contains,', '');
                }
                // check querystring for searchfor
                var searchFor = this.$location.search().searchfor;
                if (!searchFor && this.search.indexOf('[wd/') === -1) {
                    this.getPersons(this.search);
                }
            }
        }, {
            key: 'getPersons',
            value: function getPersons(search) {
                var _this3 = this;

                this.wikidataService.getPersons(search, this.lang).then(function (data) {
                    if (!data.results || !data.results.bindings || data.results.bindings.length === 0) return null;
                    var lastItemValue = '';
                    data.results.bindings.forEach(function (e) {
                        if (e.item.value != lastItemValue && e.item.value.indexOf(e.itemLabel.value) === -1) {
                            var aVariants = e.aliasList.value.split('|');
                            if (aVariants.indexOf(e.itemLabel.value) === -1) {
                                aVariants.unshift(e.itemLabel.value);
                            }
                            var variantQuery = _this3.buildPrimoQueryValue(aVariants);
                            // set properties of person object array
                            _this3.persons.push({
                                'qid': e.item.value.substring(e.item.value.lastIndexOf('/') + 1),
                                'label': e.itemLabel.value,
                                'description': e.itemDescription ? e.itemDescription.value : null,
                                'alias': e.aliasList ? e.aliasList.value : null,
                                'image': e.image ? e.image.value : null,
                                'birth': e.birth ? e.birth.value.substring(0, e.birth.value.indexOf('T')) : null,
                                'professions': e.professions ? e.professions.value : null,
                                'gndid': e.gnd ? e.gnd.value : null,
                                'viafid': e.viaf ? e.viaf.value : null,
                                'variantQuery': variantQuery,
                                'yearOfBirth': e.birth ? e.birth.value.substring(0, e.birth.value.indexOf('-')) : null
                            });
                            lastItemValue = e.item.value;
                        }
                    });
                    // render link at top of page
                    var resultList = angular.element(_this3.$element[0].parentElement.parentElement);
                    if (resultList) {
                        var html = '\n                            <div class="wikidata-search-suggestion">\n                                <a class="wikidata-anchor-link" ng-click="$ctrl.$location.hash(\'wikidata-persons-anchor\');$ctrl.$anchorScroll();">\n                                    {{$ctrl.label.persons[$ctrl.lang]}}\n                                </a>\n                            </div>\n                        ';
                        resultList.prepend(_this3.$compile(html)(_this3.$scope));
                    }
                }).catch(function (e) {
                    console.error("***ETH*** an error occured: getPersons");
                    console.error(e.message);
                });
            }
        }, {
            key: 'buildPrimoQueryValue',
            value: function buildPrimoQueryValue(items) {
                // in primo 7 rows and max 30 boolean operators
                /*
                The system will display a message and provide suggestions when the following limits are exceeded:
                The query contains more than 30 boolean operators.
                The query contains more than 8 question marks.
                The query contains more than 8 asterisks and the word length is greater than 2 (such as abb* or ab*c).
                The query contains more than 4 asterisks and the word length is less than 3 (such as ab*).
                The entire query consists of a single letter and an asterisk (such as a*).
                */
                var query = '';
                var maxItems = 20;
                var maxLength = 1000;
                items.forEach(function (e, i) {
                    if (i < maxItems) {
                        if (query.length + e.length < maxLength) {
                            query += e;
                            if (i < items.length - 1 && i < maxItems - 1) {
                                query += ' OR ';
                            }
                        }
                    }
                });
                return query;
            }
        }, {
            key: 'loadPersonPageByGndId',
            value: function loadPersonPageByGndId(gndid, qid) {
                var query = gndid;
                if (!gndid) {
                    query = qid;
                }
                var url = '/primo-explore/search?query=lsr04,contains,' + query + '&tab=default_tab&vid=WIKI&lang=' + this.lang + '&searchfor=person/' + qid;
                location.href = url;
            }
        }, {
            key: 'getLanguage',
            value: function getLanguage() {
                var sms = this.$rootScope.$$childHead.$ctrl.userSessionManagerService;
                if (!sms) {
                    console.error("***ETH*** WdExpansionPersonController: userSessionManagerService not available");
                    return 'de';
                } else {
                    return sms.getUserLanguage() || $window.appConfig['primo-view']['attributes-map'].interfaceLanguage;
                }
            }
        }]);

        return WdExpansionPersonsController;
    }()
});

/*
<md-button class="md-primary" ng-click="$ctrl.loadBestPossiblePersonPage(person);">Suche nach Name und Geburtsjahr oder eindeutiger Identifikationnummer (GND)</md-button>
<md-button class="md-primary" ng-click="$ctrl.loadPersonPageByLabel(person);">Suche nach Name</md-button>
<md-button class="md-primary" ng-click="$ctrl.loadPersonPageByVariantQuery(person);">Suche nach Namensvarianten</md-button>

loadPersonPageByLabel(person){
    let query = person.label;
    let url = `/primo-explore/search?query=any,contains,${query}&tab=default_tab&vid=WIKI&lang=de_DE&searchfor=person/${person.qid}`;
    location.href = url;
}

loadPersonPageByVariantQuery(person){
    let url = `/primo-explore/search?tab=default_tab&search_scope=default_scope&vid=WIKI&lang=de_DE&offset=0&searchfor=person/${person.qid}&query=any,contains,${person.variantQuery}`;
    location.href = url;
}

loadBestPossiblePersonPage(person){
    let query = person.label;
    if(person.yearOfBirth &&  person.gndid){
        query = query + ' AND (' + person.yearOfBirth + ' OR ' + person.gndid + ')';
    }
    else if (person.yearOfBirth) {
        query = query + ' ' + person.yearOfBirth;
    }
    else if (person.gndid) {
        query = query + ' ' + person.gndid;
    }
    let url = `/primo-explore/search?tab=default_tab&search_scope=default_scope&vid=WIKI&lang=de_DE&offset=0&searchfor=person/${person.qid}&query=any,contains,${query}`;
    location.href = url;
}
*/

/**
* @memberof WIKI
* @ngdoc component
* @name wdExpansionPlace
*
* @description
* WIKI Component:<br>
*
* - place page
* - switch between all results and selected results from ETHorama
* - map and links
*
* <b>Requirements</b><br>
*
* Service {@link WIKI.wikidataService}<br>
* Service {@link WIKI.poiService}<br>
* Service {@link WIKI.daasService}<br>
*
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.prmSearchResultListAfter}<br>
*
*
* <b>In Package</b><br>
*
* WIKI <br>
 */
/*
<div class="wikidata-map-container">
    <div id="card"></div>
</div>
*/
app.component('ethWdExpansionPlace', {
    require: {
        parent: '^prmSearchResultListAfter'
    },
    template: '\n    <div class="wikidata-place" ng-if="$ctrl.hasResults">\n        <a name="wikidata-place-anchor"></a>\n        <div class="wikidata-map-container">\n            <div id="mapid"></div>\n        </div>\n        <md-card md-theme="default" md-theme-watch>\n            <md-card-title>\n                <md-card-title-text>\n                    <h2 class="md-headline">{{$ctrl.itemLabel}}</h2>\n                </md-card-title-text>\n                <md-card-title-media ng-if="$ctrl.pois[0].image">\n                    <img class="md-card-image" ng-src="{{$ctrl.pois[0].image.value}}?width=200px" />\n                </md-card-title-media>\n            </md-card-title>\n            <md-card-content>\n                <h3 class="md-subhead">{{$ctrl.label.wdLinks[$ctrl.lang]}}</h3>\n                <p ng-if="$ctrl.wikidataURL"><a href="{{$ctrl.wikidataURL}}" target="_blank">Wikidata</a></p>\n                <p ng-if="$ctrl.wikipediaURL"><a href="{{$ctrl.wikipediaURL}}" target="_blank">Wikipedia</a></p>\n                <p ng-if="$ctrl.geonamesURL"><a href="{{$ctrl.geonamesURL}}" target="_blank">Geonames</a></p>\n                <p ng-if="$ctrl.gndURL"><a href="{{$ctrl.gndURL}}" target="_blank">GND (DNB)</a></p>\n                <p ng-if="$ctrl.hlsURL"><a href="{{$ctrl.hlsURL}}" target="_blank">Historisches Lexikon der Schweiz</a></p>\n                <p ng-if="$ctrl.archinformURL"><a href="{{$ctrl.archinformURL}}" target="_blank">archINFORM</a></p>\n\n                <h3 class="md-subhead" ng-if="$ctrl.geolinks.length > 0">{{$ctrl.label.geolinkerLinks[$ctrl.lang]}}</h3>\n                <p ng-repeat="object in $ctrl.geolinks">\n                    <a href="{{object}}" target="_blank">{{object}}</a>\n                </p>\n            <md-card-content>\n        </md-card>\n    </div>\n    ',

    controller: function () {
        function WdExpansionPlaceController(wikidataService, poiService, daasService, $timeout, $location, $element, $scope, $compile, $rootScope) {
            _classCallCheck(this, WdExpansionPlaceController);

            this.wikidataService = wikidataService;
            this.poiService = poiService;
            this.daasService = daasService;
            this.$timeout = $timeout;
            this.$location = $location;
            this.$element = $element;
            this.$scope = $scope;
            this.$compile = $compile;
            this.$rootScope = $rootScope;
            this.label = {
                allResults: {
                    de_DE: 'Alle Suchergebnisse zu',
                    en_US: 'All search results for'
                },
                insteadAll1: {
                    de_DE: 'Stattdessen alle Suchergebnisse zu',
                    en_US: 'See all search results for'
                },
                insteadAll2: {
                    de_DE: 'sehen',
                    en_US: 'instead'
                },
                dossier: {
                    de_DE: 'Unser Dossier zu',
                    en_US: 'Our dossier on'
                },
                insteadDossier1: {
                    de_DE: 'Stattdessen unser Dossier zum Ort',
                    en_US: 'See our dossier on place'
                },
                insteadDossier2: {
                    de_DE: 'sehen',
                    en_US: 'instead'
                },
                wdLinks: {
                    de_DE: 'Externe Links aus Wikidata',
                    en_US: 'External links from Wikidata'
                },
                geolinkerLinks: {
                    de_DE: 'Externe Links der Geolinker API',
                    en_US: 'External links from Geolinker API'
                }
            };
        }

        _createClass(WdExpansionPlaceController, [{
            key: '$onInit',
            value: function $onInit() {
                this.parentCtrl = this.parent.parentCtrl;
                this.search = '';
                this.pois = [];
                this.hasResults = false;
                this.lang = this.getLanguage(this.$rootScope);

                // this.search: input in Primo searchField
                var query = this.parentCtrl.searchService.searchUtil.$stateParams.query;
                if (query.indexOf('lsr04') > -1) {
                    this.search = query.replace('lsr04,contains,', '');
                } else {
                    this.search = query.replace('any,contains,', '');
                }
                // check querystring for searchfor
                var searchFor = this.$location.search().searchfor;

                // call by searchpage
                if (searchFor && searchFor.indexOf('place/') > -1) {
                    this.getPlace(searchFor.substring(searchFor.indexOf('/') + 1));
                }
                // permalink
                else if (this.search.indexOf('[wd/place]') > -1) {
                        this.getPlace(this.search.substring(this.search.indexOf(']') + 1));
                    }
            }
        }, {
            key: 'getPlace',
            value: function getPlace(qid) {
                var _this4 = this;

                this.poiService.getPlaceByQNumber(qid).then(function (data) {
                    if (!data) return qid;
                    var firstKey = Object.keys(data)[0];
                    _this4.pois = [data[firstKey]];
                    if (_this4.pois.length === 0) return qid;
                    _this4.pois[0].qid = qid;
                    return qid;
                }).then(function (qid) {
                    return _this4.wikidataService.getPlace(qid, _this4.lang);
                }).then(function (data) {
                    if (data.results.bindings && data.results.bindings.length > 0) {
                        _this4.hasResults = true;
                        _this4.itemLabel = data.results.bindings[0].itemLabel.value;
                        _this4.parentCtrl.searchService.searchFieldsService.mainSearch = _this4.itemLabel;
                        _this4.$timeout(function () {
                            _this4.searchAllPlaceRessources(_this4.itemLabel);
                        }, 200);

                        _this4.$timeout(function () {
                            _this4.renderLeafletMap(data.results.bindings[0]);
                        }, 500);

                        // render header for result list
                        var resultList = angular.element(_this4.$element[0].parentElement.parentElement);
                        if (resultList) {
                            var html = '\n                            <div class="wikidata-place-top" ng-if="$ctrl.pois.length == 0">\n                                <h1>Ergebnisse zu "' + _this4.itemLabel + '"</h1>\n                            </div>\n                            <div class="wikidata-place-top" ng-if="$ctrl.pois.length > 0">\n                                <h1><span class="wikidata-place-top__text">' + _this4.label.allResults[_this4.lang] + '</span> "' + _this4.itemLabel + '" </h1>\n                                <md-button style="display:none;" class="md-primary wikidata-place-top__button-all" ng-click="$ctrl.searchAllPlaceRessources(\'' + _this4.itemLabel + '\');">' + _this4.label.insteadAll1[_this4.lang] + ' "' + _this4.itemLabel + '" ' + _this4.label.insteadAll2[_this4.lang] + '</md-button>\n                                <md-button class="md-primary wikidata-place-top__button-dossier" ng-click="$ctrl.searchSelectedPlaceRessources($ctrl.pois[0]);">' + _this4.label.insteadDossier1[_this4.lang] + ' "' + _this4.itemLabel + '" ' + _this4.label.insteadDossier2[_this4.lang] + '</md-button>\n                            </div>';
                            resultList.prepend(_this4.$compile(html)(_this4.$scope));
                        }

                        var place = data.results.bindings[0];
                        if (place.item && place.item.value) {
                            _this4.wikidataURL = place.item.value;
                        }
                        if (place.geonames) {
                            _this4.geonamesURL = 'https://www.geonames.org/' + place.geonames.value;
                        }
                        if (place.wikipedia) {
                            _this4.wikipediaURL = place.wikipedia.value;
                        }
                        if (place.gnd) {
                            _this4.gndURL = 'http://d-nb.info/gnd/' + place.gnd.value;
                        }
                        if (place.hls) {
                            _this4.hlsURL = 'http://www.hls-dhs-dss.ch/textes/d/D' + place.hls.value + '.php';
                        }
                        if (place.archinform) {
                            _this4.archinformURL = 'https://www.archinform.net/ort/' + place.archinform.value + '.htm';
                        }
                        _this4.daasService.searchGeolinkerByQID(qid).then(function (data) {
                            try {
                                _this4.geolinks = data.data.resolverneo4j.links;
                            } catch (e) {
                                _this4.geolinks = null;
                            }
                        });
                    }
                }).catch(function (e) {
                    console.error("***ETH*** an error occured: getPlace wikidataService ");
                    console.error(e.message);
                    throw e;
                });
            }
        }, {
            key: 'renderPlaceMap',
            value: function renderPlaceMap(wikiResult) {
                var point = wikiResult.coordinate_location.value;
                var lng = point.substring(6, point.indexOf(' '));
                var lat = point.substring(point.indexOf(' ') + 1, point.length - 1);
                var position = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));

                var map = new google.maps.Map(document.getElementById("card"), {
                    zoom: 13,
                    center: position
                });
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
            }
        }, {
            key: 'renderLeafletMap',
            value: function renderLeafletMap(wikiResult) {
                var point = wikiResult.coordinate_location.value;
                var lng = point.substring(6, point.indexOf(' '));
                var lat = point.substring(point.indexOf(' ') + 1, point.length - 1);
                if (!document.getElementById('mapid')) return;
                var map = L.map('mapid').setView([lat, lng], 13);
                L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                    maxZoom: 18,
                    id: 'mapbox.streets',
                    accessToken: 'pk.eyJ1IjoiYmVybmR1IiwiYSI6ImNrMjBpMHRidjE1ZGkzaHFnNDdlMXV4eWkifQ.RACc6dWL675djVZaMmHBww'
                }).addTo(map);
                var marker = L.marker([lat, lng]).addTo(map);
            }
        }, {
            key: 'searchSelectedPlaceRessources',
            value: function searchSelectedPlaceRessources(poi) {
                var items = [];
                poi.contentitems.forEach(function (e) {
                    items.push(e.sourceid);
                });
                // in primo max 30 boolsche operatoren
                var query = 'any,contains,';
                items.forEach(function (e, i) {
                    if (i < 31) {
                        query += e;
                        if (i < items.length - 1 && i < 30) {
                            query += ' OR ';
                        }
                    }
                });
                this.parentCtrl.searchService.search({ query: query }, true);
                if (document.getElementsByClassName('wikidata-place-top__text')[0]) {
                    document.getElementsByClassName('wikidata-place-top__text')[0].innerHTML = this.label.dossier[this.lang];
                    document.getElementsByClassName('wikidata-place-top__button-all')[0].style.display = 'block';
                    document.getElementsByClassName('wikidata-place-top__button-dossier')[0].style.display = 'none';
                }
            }
        }, {
            key: 'searchAllPlaceRessources',
            value: function searchAllPlaceRessources(search) {
                // input field
                this.parentCtrl.searchService.searchFieldsService.mainSearch = search;
                // do search
                this.parentCtrl.searchService.search({ query: 'any,contains,' + search }, true);
                if (document.getElementsByClassName('wikidata-place-top__text')[0]) {
                    document.getElementsByClassName('wikidata-place-top__text')[0].innerHTML = this.label.allResults[this.lang];
                    document.getElementsByClassName('wikidata-place-top__button-all')[0].style.display = 'none';
                    document.getElementsByClassName('wikidata-place-top__button-dossier')[0].style.display = 'block';
                }
            }
        }, {
            key: 'getLanguage',
            value: function getLanguage() {
                var sms = this.$rootScope.$$childHead.$ctrl.userSessionManagerService;
                if (!sms) {
                    console.error("***ETH*** WdExpansionPersonController: userSessionManagerService not available");
                    return 'de';
                } else {
                    return sms.getUserLanguage() || $window.appConfig['primo-view']['attributes-map'].interfaceLanguage;
                }
            }
        }]);

        return WdExpansionPlaceController;
    }()
});

/*
Persons:
<h3 class="md-subhead" ng-if="$ctrl.birthplaceAtPlace.length > 0">Hier geborene historische Personen (vor 1940 geboren, aus Wikidata)</h3>
<p class="wikidata-person-small" ng-repeat="object in $ctrl.birthplaceAtPlace">
    <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
        <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
        <span class="wikidata-person-small__text">{{object.itemLabel.value}} ({{object.itemDescription.value}}, Geburtstag:{{object.births.value.substring(0,object.births.value.indexOf('T'))}})</span>
    </a>
</p>

<h3 class="md-subhead" ng-if="$ctrl.educatedAtPlace.length > 0">Hier ausgebildete historische Personen (vor 1940 geboren, aus Wikidata)</h3>
<p class="wikidata-person-small" target="_blank" ng-repeat="object in $ctrl.educatedAtPlace">
    <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
        <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
        <span class="wikidata-person-small__text">{{object.itemLabel.value}} ({{object.itemDescription.value}}, Geburtstag:{{object.births.value.substring(0,object.births.value.indexOf('T'))}})</span>
    </a>
</p>

<h3 class="md-subhead" ng-if="$ctrl.personsAtPlace.length > 0">Hier lebten, arbeiteten oder starben historische Personen(vor 1940 geboren, aus Wikidata)</h3>
<p class="wikidata-person-small" ng-repeat="object in $ctrl.personsAtPlace">
    <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
        <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
        <span class="wikidata-person-small__text">{{object.itemLabel.value}} (<span ng-if="object.itemDescription">{{object.itemDescription.value}},</span> Geburtstag:{{object.births.value.substring(0,object.births.value.indexOf('T'))}})</span>
    </a>
</p>

<h3 class="md-subhead" ng-if="$ctrl.employeesAtPlace.length > 0">Arbeitsstätte von historischen Personen (vor 1940 geboren, aus Wikidata)</h3>
<p class="wikidata-person-small" ng-repeat="object in $ctrl.employeesAtPlace">
    <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
        <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
        <span class="wikidata-person-small__text">{{object.itemLabel.value}} ({{object.itemDescription.value}}, Geburtstag:{{object.births.value.substring(0,object.births.value.indexOf('T'))}})</span>
    </a>
</p>

<h3 class="md-subhead" ng-if="$ctrl.burriedInPlace.length > 0">Hier begrabene historische Personen (vor 1940 geboren, aus Wikidata)</h3>
<p class="wikidata-person-small" ng-repeat="object in $ctrl.burriedInPlace">
    <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
        <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
        <span class="wikidata-person-small__text">{{object.itemLabel.value}} ({{object.itemDescription.value}}, Geburtstag:{{object.births.value.substring(0,object.births.value.indexOf('T'))}})</span>
    </a>
</p>
*/
/*
this.wikidataService.getPersonsBornInPlace(qid)
    .then((data) => {
        try{
            this.birthplaceAtPlace = data.results.bindings;
        }
        catch(e){
            this.birthplaceAtPlace = null;
        }
    })
this.wikidataService.getPersonsByPlace(qid)
    .then((data) => {
        try{
            this.personsAtPlace = data.results.bindings;
        }
        catch(e){
            this.personsAtPlace = null;
        }
    })
this.wikidataService.getPersonsEmployedByPlace(qid)
    .then((data) => {
        try{
            this.employeesAtPlace = data.results.bindings;
        }
        catch(e){
            this.employeesAtPlace = null;
        }

    })
this.wikidataService.getPersonsEducatedAtPlace(qid)
    .then((data) => {
        try{
            this.educatedAtPlace = data.results.bindings;
        }
        catch(e){
            this.educatedAtPlace = null;
        }
    })
this.wikidataService.getPersonsBurriedInPlace(qid)
    .then((data) => {
        try{
            this.burriedInPlace = data.results.bindings;
        }
        catch(e){
            this.burriedInPlace = null;
        }
    })

*/
/*
loadPersonPageByGndId(gndid, qid){
    let query = gndid;
    if(!gndid){
        query = qid;
    }
    let url = `/primo-explore/search?query=lsr04,contains,${query}&tab=default_tab&vid=WIKI&lang=de_DE&searchfor=person/${qid}`;
    location.href = url;
}
*/

/**
* @memberof WIKI
* @ngdoc component
* @name wdExpansionPlaces
*
* @description
* WIKI Component:<br>
*
* - Query expansion via wikidata: places for searchterm
*
* <b>Requirements</b><br>
*
* Service {@link WIKI.wikidataService}<br>
*
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.prmSearchResultListAfter}<br>
*
*
* <b>In Package</b><br>
*
* WIKI <br>
 */
app.component('ethWdExpansionPlaces', {
    require: {
        parent: '^prmSearchResultListAfter'
    },
    template: '\n    <div class="wikidata-places" ng-if="$ctrl.hasResults">\n        <a name="wikidata-places-anchor"></a>\n        <div class="wikidata-map-container">\n            <div id="card"></div>\n        </div>\n    </div>\n    ',

    controller: function () {
        function WdExpansionPlacesController(wikidataService, poiService, $timeout, $location, $element, $scope, $compile, $anchorScroll, $rootScope) {
            _classCallCheck(this, WdExpansionPlacesController);

            this.wikidataService = wikidataService;
            this.poiService = poiService;
            this.$timeout = $timeout;
            this.$location = $location;
            this.$element = $element;
            this.$scope = $scope;
            this.$compile = $compile;
            this.$anchorScroll = $anchorScroll;
            this.$rootScope = $rootScope;
            this.label = {
                places: {
                    de_DE: 'Orte zu meinem Suchbegriff',
                    en_US: 'Places matching my search term'
                },
                searchThis1: {
                    de_DE: 'Klicken Sie bitte auf den Marker, um nach"',
                    en_US: 'Please click on the marker to search for "'
                },
                searchThis2: {
                    de_DE: '" zu suchen!',
                    en_US: '"!'
                }
            };
        }

        _createClass(WdExpansionPlacesController, [{
            key: '$onInit',
            value: function $onInit() {
                this.parentCtrl = this.parent.parentCtrl;
                this.search = '';
                this.pois = [];
                this.hasResults = false;
                this.lang = this.getLanguage(this.$rootScope);

                // this.search: input in Primo searchField
                var query = this.parentCtrl.searchService.searchUtil.$stateParams.query;
                if (query.indexOf('lsr04') > -1) {
                    this.search = query.replace('lsr04,contains,', '');
                } else {
                    this.search = query.replace('any,contains,', '');
                }
                // check querystring for searchfor
                var searchFor = this.$location.search().searchfor;
                if (!searchFor && this.search.indexOf('[wd/') === -1) {
                    this.getPlaces(this.search);
                }
                if (this.search === '[current]') {
                    if ("geolocation" in navigator) {
                        var that = this;
                        navigator.geolocation.getCurrentPosition(function (position) {
                            that.getCurrentPoi(position.coords.latitude, position.coords.longitude);
                        });
                    }
                }
            }
        }, {
            key: 'getPlaces',
            value: function getPlaces(search) {
                var _this5 = this;

                this.wikidataService.getPlaces(search, this.lang).then(function (data) {
                    if (!data.results || !data.results.bindings || data.results.bindings.length === 0) return null;
                    // filter for instanceOf to reduce number of places
                    var wikiResults = data.results.bindings.filter(function (e) {
                        if (e.instanceOfLabels.value.indexOf('canton of') > -1) {
                            return false;
                        } else if (e.instanceOfLabels.value.indexOf('greater region') > -1) {
                            return false;
                        } else if (e.instanceOfLabels.value == 'Kanton') {
                            return false;
                        } else if (e.instanceOfLabels.value == 'Wikimedia-Dublette') {
                            return false;
                        } else if (e.instanceOfLabels.value.indexOf('Grossregionen') > -1) {
                            return false;
                        } else if (e.instanceOfLabels.value.indexOf('Bezirk des Kantons') > -1) {
                            return false;
                        }
                        return true;
                    });
                    if (wikiResults.length === 0) {
                        return;
                    }
                    _this5.hasResults = true;
                    _this5.$timeout(function () {
                        _this5.renderPlacesMap(wikiResults);
                    }, 1000);
                });
            }
        }, {
            key: 'renderPlacesMap',
            value: function renderPlacesMap(wikiResults) {
                var _this6 = this;

                var map = new google.maps.Map(document.getElementById("card"), {
                    zoom: 5,
                    center: { lat: 47, lng: 8 }
                });
                var latlngList = [];
                var controller = this;
                var lastLng = 0;
                var lastLat = 0;
                // if point is located at the moon: <http://www.wikidata.org/entity/Q405> Point(-176.2 49.6)
                wikiResults = wikiResults.filter(function (wikiResult, i) {
                    if (!wikiResult.coordinate_location) return false;
                    if (wikiResult.coordinate_location.value.indexOf('www.wikidata.org') > 0) {
                        return false;
                    }
                    return true;
                });
                var markers = wikiResults.map(function (wikiResult, i) {
                    var point = wikiResult.coordinate_location.value;
                    var lat = parseFloat(point.substring(point.indexOf(' ') + 1, point.length - 1));
                    var lng = parseFloat(point.substring(6, point.indexOf(' ')));
                    if (lng == lastLng && lat == lastLat) {
                        lat = lat + 0.00001;
                    }
                    var position = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                    latlngList.push(position);
                    lastLng = lng;
                    lastLat = lat;
                    var marker = new google.maps.Marker({
                        position: position
                    });
                    google.maps.event.addListener(marker, 'click', function () {
                        controller.loadPlacePage(wikiResult.item.value.substring(wikiResult.item.value.lastIndexOf('/') + 1), wikiResult.itemLabel.value);
                    });
                    var content = '<div class="marker-info">';
                    if (wikiResult.image) {
                        content += '<img src="' + wikiResult.image.value + '">';
                    }
                    content += '<div class="marker-info-text"><p class="marker-info-title">' + wikiResult.itemLabel.value + '</p>';
                    if (wikiResult.instanceOfLabels) {
                        content += '(' + wikiResult.instanceOfLabels.value + ')';
                    }
                    content += '<p class="marker-info-hint">' + _this6.label.searchThis1[_this6.lang] + wikiResult.itemLabel.value + _this6.label.searchThis2[_this6.lang] + '</p></div></div>';
                    var infoWindow = new google.maps.InfoWindow({
                        content: content
                    });
                    google.maps.event.addListener(marker, 'mouseover', function () {
                        infoWindow.open(map, this);
                    });
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infoWindow.close(map, this);
                    });
                    return marker;
                });
                // Add a marker clusterer to manage the markers.
                var markerCluster = new MarkerClusterer(map, markers, {
                    imagePath: 'custom/WIKI/img/m',
                    maxZoom: 15
                });

                // Add bounds -> zoom appropriate
                var bounds = new google.maps.LatLngBounds();
                latlngList.forEach(function (n) {
                    bounds.extend(n);
                });
                map.setCenter(bounds.getCenter());
                map.fitBounds(bounds);

                // top link to map container
                var resultList = angular.element(this.$element[0].parentElement.parentElement);
                if (resultList) {
                    var html = '\n                    <div class="wikidata-search-suggestion">\n                        <a class="wikidata-anchor-link" ng-click="$ctrl.$location.hash(\'wikidata-places-anchor\');$ctrl.$anchorScroll();">\n                            {{$ctrl.label.places[$ctrl.lang]}}\n                        </a>\n                    </div>\n                ';
                    resultList.prepend(this.$compile(html)(this.$scope));
                }
            }
        }, {
            key: 'loadPlacePage',
            value: function loadPlacePage(qid, label) {
                var url = '/primo-explore/search?query=any,contains,' + label + '&tab=default_tab&vid=WIKI&lang=' + this.lang + '&searchfor=place/' + qid;
                location.href = url;
            }
        }, {
            key: 'getCurrentPoi',
            value: function getCurrentPoi(lat, lng) {
                var _this7 = this;

                this.poiService.nearTo(lat, lng, 50).then(function (pois) {
                    if (!pois || pois.length === 0) return null;
                    _this7.wikidataService.enrichPOIsWithQID(pois).then(function (pois) {
                        if (!pois[0].qid) return null;
                        console.error(pois);
                        var url = '/primo-explore/search?query=any,contains,' + pois[0].title + '&tab=default_tab&vid=WIKI&lang=' + _this7.lang + '&searchfor=place/' + pois[0].qid;
                        location.href = url;
                    });
                }).catch(function (e) {
                    console.error("***ETH*** an error occured: MainAfterController getPoi ");
                    console.error(e.message);
                    throw e;
                });
            }
        }, {
            key: 'getLanguage',
            value: function getLanguage() {
                var sms = this.$rootScope.$$childHead.$ctrl.userSessionManagerService;
                if (!sms) {
                    console.error("***ETH*** WdExpansionPersonController: userSessionManagerService not available");
                    return 'de';
                } else {
                    return sms.getUserLanguage() || $window.appConfig['primo-view']['attributes-map'].interfaceLanguage;
                }
            }
        }]);

        return WdExpansionPlacesController;
    }()
});

/**
* @memberof WIKI
* @ngdoc service
* @name daasService
*
* @description
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.wdExpansionPerson}<br>
* WIKI Component {@link WIKI.wdExpansionPlace}<br>
*
*
* <b>In Package</b><br>
*
* WIKI<br>
 */
app.factory('daasService', ['$http', '$sce', function ($http, $sce) {

    function searchPrimo(q) {
        var baseurl = 'https://daas.library.ethz.ch/rib/v2/search';
        var url = baseurl + '?lang=de_DE&limit=1&q=' + encodeURIComponent(q);
        $sce.trustAsResourceUrl(url);
        return $http.get(url).then(function (response) {
            return response.data;
        }, function (httpError) {
            console.error(httpError);
            if (httpError.status === 404) return null;
            var error = "***ETH*** an error occured: daasService.searchPrimo error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            return null;
        });
    }

    function searchMetagrid(gnd) {
        var baseurl = 'https://daas.library.ethz.ch/rib/v2/enrichments/metagrid/';
        var url = baseurl + gnd;
        $sce.trustAsResourceUrl(url);
        return $http.get(url).then(function (response) {
            return response.data;
        }, function (httpError) {
            console.error(httpError);
            if (httpError.status === 404) return null;
            var error = "***ETH*** an error occured: daasService.searchMetagrid error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            return null;
        });
    }

    function searchEntityfacts(gnd) {
        var baseurl = 'https://daas.library.ethz.ch/rib/v2/enrichments/entityfacts/';
        var url = baseurl + gnd;
        $sce.trustAsResourceUrl(url);
        return $http.get(url).then(function (response) {
            return response.data;
        }, function (httpError) {
            console.error(httpError);
            if (httpError.status === 404) return null;
            var error = "***ETH*** an error occured: daasService.searchEntityfacts error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            return null;
        });
    }

    function searchFindbuch(gnd) {
        var baseurl = 'https://daas.library.ethz.ch/rib/v2/enrichments/findbuch/gnd-person/';
        var url = baseurl + gnd;
        $sce.trustAsResourceUrl(url);
        return $http.get(url).then(function (response) {
            return response.data;
        }, function (httpError) {
            console.error(httpError);
            if (httpError.status === 404) return null;
            var error = "***ETH*** an error occured: daasService.searchFindbuch error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            return null;
        });
    }

    function searchGeolinkerByQID(qid) {
        var baseurl = 'https://daas.library.ethz.ch/rib/v2/enrichments/geolinker/qid/';
        var url = baseurl + qid;
        $sce.trustAsResourceUrl(url);
        return $http.get(url).then(function (response) {
            return response.data;
        }, function (httpError) {
            if (httpError.status === 404) return null;
            var error = "***ETH*** an error occured: daasService.searchGeolinkerByQID error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            return null;
        });
    }

    return {
        searchPrimo: searchPrimo,
        searchMetagrid: searchMetagrid,
        searchEntityfacts: searchEntityfacts,
        searchFindbuch: searchFindbuch,
        searchGeolinkerByQID: searchGeolinkerByQID
    };
}]);

/**
* @memberof eth
* @ngdoc service
* @name lobidService
*
* @description
* not used
*
* <b>Used by</b><br>
*
*
* <b>In Package</b><br>
*
* WIKI<br>
*
 */
// http://blog.lobid.org/2018/07/06/lobid-gnd-queries.html
app.factory('lobidService', ['$http', '$sce', function ($http, $sce) {

    function getPersonsByID(id) {
        var endpointUrl = 'https://lobid.org/gnd/';

        var fullUrl = endpointUrl + id + '.json';
        $sce.trustAsResourceUrl(fullUrl);

        return $http.get(fullUrl, { headers: { 'Accept': 'application/sparql-results+json' } }).then(function (response) {
            return response.data;
        }, function (httpError) {
            if (httpError.status === 404) {
                return null;
            } else {
                var error = "***ETH*** an error occured: lobidService getPersonsByPreferredName error callback: " + httpError.status;
                if (httpError.data && httpError.data.errorMessage) {
                    error += ' - ' + httpError.data.errorMessage;
                }
                console.error(error);
                return null;
            }
        });
    }

    function getPersonsByPreferredName(name) {
        var endpointUrl = 'https://lobid.org/gnd/search';

        var fullUrl = endpointUrl + '?size=15&filter=type:Person&format=json:suggest&q=preferredName:' + name;
        //let fullUrl = endpointUrl + '?size=100&filter=type:Person&format=json:suggest&q=sameAs.collection.id:"http://www.wikidata.org/entity/Q2013" AND preferredName:' + name;
        $sce.trustAsResourceUrl(fullUrl);

        return $http.get(fullUrl, { headers: { 'Accept': 'application/sparql-results+json' } }).then(function (response) {
            return response.data;
        }, function (httpError) {
            var error = "***ETH*** an error occured: lobidService getPersonsByPreferredName error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    function getPersonsByVariantName(name) {
        var endpointUrl = 'https://lobid.org/gnd/search';

        var fullUrl = endpointUrl + '?size=15&filter=type:Person&format=json:suggest&q=variantName:' + name;
        //let fullUrl = endpointUrl + '?size=100&filter=type:Person&format=json:suggest&q=sameAs.collection.id:"http://www.wikidata.org/entity/Q2013" AND preferredName:' + name;
        $sce.trustAsResourceUrl(fullUrl);

        return $http.get(fullUrl, { headers: { 'Accept': 'application/sparql-results+json' } }).then(function (response) {
            return response.data;
        }, function (httpError) {
            var error = "***ETH*** an error occured: lobidService getPersonsByPreferredName error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    function getCorporateBodies(name) {
        var endpointUrl = 'https://lobid.org/gnd/search';
        var fullUrl = endpointUrl + '?size=10&filter=type:CorporateBody&format=json:preferredName&q="' + name + '"';
        $sce.trustAsResourceUrl(fullUrl);

        return $http.get(fullUrl, { headers: { 'Accept': 'application/sparql-results+json' } }).then(function (response) {
            return response.data;
        }, function (httpError) {
            var error = "***ETH*** an error occured: lobidService getCorporateBodies error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    return {
        getPersonsByID: getPersonsByID,
        getPersonsByVariantName: getPersonsByVariantName,
        getPersonsByPreferredName: getPersonsByPreferredName,
        getCorporateBodies: getCorporateBodies
    };
}]);

/**
* @memberof WIKI
* @ngdoc service
* @name poiService
*
* @description
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.wdExpansionPlace}<br>
*
*
* <b>In Package</b><br>
*
* WIKI<br>
 */
app.factory('poiService', ['$http', '$sce', function ($http, $sce) {
    var baseurl = 'https://daas.library.ethz.ch/rib/v2/pois';

    function nearTo(lat, lng, distance) {
        var url = baseurl + "?nearto=" + lat + "," + lng + "," + distance;
        $sce.trustAsResourceUrl(url);
        return $http.get(url).then(function (response) {
            return response.data;
        }, function (httpError) {
            if (httpError.status === 404) return null;
            var error = "***ETH*** an error occured: poiService.nearTo error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    function getPoi(poi) {
        var url = baseurl + "/" + poi;
        $sce.trustAsResourceUrl(url);

        return $http.get(url).then(function (response) {
            return response.data;
        }, function (httpError) {
            console.error(httpError);
            if (httpError.status === 404) return null;
            var error = "***ETH*** an error occured: poiService.getPoi error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            return null;
        });
    }

    function getPlaceByQNumber(qid) {
        var url = baseurl + '?qid=' + qid;
        $sce.trustAsResourceUrl(url);

        return $http.get(url).then(function (response) {
            return response.data;
        }, function (httpError) {
            if (httpError.status === 404 || httpError.statusText === 'Bad Request') return null;
            var error = "***ETH*** an error occured: poiService getPlaceByQNumber error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    function search(q) {
        var url = baseurl + '?q=' + q;
        $sce.trustAsResourceUrl(url);

        return $http.get(url).then(function (response) {
            return response.data;
        }, function (httpError) {
            console.error(httpError);
            if (httpError.status === 404) return null;
            var error = "***ETH*** an error occured: poiService.search error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            return null;
        });
    }

    return {
        nearTo: nearTo,
        getPoi: getPoi,
        getPlaceByQNumber: getPlaceByQNumber,
        search: search
    };
}]);

/**
* @memberof eth
* @ngdoc service
* @name searchService
*
* @description
* Service to get context informations of the current search (tab, scope etc).
*
* <b>Used by</b><br>
*
* Eth After Component {@link eth.prmBriefResultAfter} (DADS)<br>
* Eth After Component {@link eth.prmSearchResultAvailabilityLineAfter}<br>
* Eth After Component {@link eth.prmSearchResultListAfter} (DADS)<br>
*
* Eth Component {@link eth.ethAltmetric}<br>
* Eth Component {@link eth.ethOadoi}<br>
*
*
* <b>In Package</b><br>
*
* WIKI<br>
 */
app.factory('searchService', function () {
    return {
        getTab: function getTab(searchService) {
            return searchService.searchFieldsService._tab;
        },
        getScope: function getScope(searchService) {
            return searchService.searchFieldsService._scope;
        },
        getMainSearch: function getMainSearch(searchService) {
            return searchService.searchFieldsService._mainSearch;
        }
    };
});

/**
* @memberof WIKI
* @ngdoc service
* @name wikidataService
*
* @description
* Services for getting informations about persons and places from wikidata
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.wdExpansionPersons}<br>
* WIKI Component {@link WIKI.wdExpansionPerson}<br>
* WIKI Component {@link WIKI.wdExpansionPlaces}<br>
* WIKI Component {@link WIKI.wdExpansionPlace}<br>
*
*
* <b>In Package</b><br>
*
* WIKI<br>
*
 */
app.factory('wikidataService', ['$http', '$sce', function ($http, $sce) {

    // only CH: ?item wdt:P17 wd:Q39.
    function getPlaces(search, lang) {
        var paramLang = getParamLang(lang);
        var endpointUrl = 'https://query.wikidata.org/sparql';
        var sparqlQuery = '\n        SELECT DISTINCT ?item ?itemLabel ?coordinate_location ?image\n        (group_concat(DISTINCT(?instanceOfLabel) ;separator = ", ") as ?instanceOfLabels )\n        WHERE {\n            SERVICE wikibase:mwapi {\n                bd:serviceParam wikibase:api "EntitySearch" .\n                bd:serviceParam wikibase:endpoint "www.wikidata.org" .\n                bd:serviceParam mwapi:search "' + search + '" .\n                bd:serviceParam mwapi:language "de" .\n                ?item wikibase:apiOutputItem mwapi:item .\n                ?num wikibase:apiOrdinal true .\n            }\n\n            ?item wdt:P31 ?instanceOf.\n            ?item wdt:P625 ?coordinate_location.\n\n            OPTIONAL {?item wdt:P18 ?image.}\n            SERVICE wikibase:label { bd:serviceParam wikibase:language "' + paramLang + '".\n                                    ?instanceOf rdfs:label ?instanceOfLabel .\n                                     ?item rdfs:label ?itemLabel .}\n        }\n        GROUP BY ?item ?itemLabel ?coordinate_location ?image\n        ORDER BY ?coordinate_location\n        LIMIT 100\n        ';
        var fullUrl = endpointUrl + '?query=' + sparqlQuery;
        $sce.trustAsResourceUrl(fullUrl);

        return $http.get(fullUrl, { headers: { 'Accept': 'application/sparql-results+json' } }).then(function (response) {
            return response.data;
        }, function (httpError) {
            var error = "***ETH*** an error occured: wikidataService getPlaces error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    function getPlace(qid, lang) {
        var paramLang = getParamLang(lang);
        var endpointUrl = 'https://query.wikidata.org/sparql';
        var sparqlQuery = '\n        SELECT DISTINCT  ?item ?itemLabel ?wikipedia ?gnd ?geonames ?archinform ?hls ?coordinate_location ?image\n        WHERE {\n          BIND(wd:' + qid + ' AS ?item).\n          ?item wdt:P625 ?coordinate_location.\n          OPTIONAL {?item wdt:P18 ?image.}\n          OPTIONAL {?item wdt:P227 ?gnd}\n          OPTIONAL {?item wdt:P1566 ?geonames}\n          OPTIONAL {?item wdt:P5573 ?archinform}\n          OPTIONAL {?item wdt:P902 ?hls}\n          OPTIONAL {\n            ?wikipedia schema:about ?item .\n            ?wikipedia schema:inLanguage "de" .\n            FILTER (SUBSTR(str(?wikipedia), 1, 25) = "https://de.wikipedia.org/")\n          }\n          SERVICE wikibase:label { bd:serviceParam wikibase:language "' + paramLang + '".\n                                   ?item rdfs:label ?itemLabel .\n                                  }\n        }\n        ';
        var fullUrl = endpointUrl + '?query=' + encodeURIComponent(sparqlQuery);
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, { headers: { 'Accept': 'application/sparql-results+json' } }).then(function (response) {
            return response.data;
        }, function (httpError) {
            var error = "***ETH*** an error occured: wikidataService getPlace error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    // default order of mwapi "Search" is relevance (srsort)
    // https://www.mediawiki.org/wiki/API:Search
    function getPersons(search, lang) {
        var paramLang = getParamLang(lang);
        var endpointUrl = 'https://query.wikidata.org/sparql';
        var sparqlQuery = '\n        SELECT DISTINCT ?item ?itemLabel ?itemDescription ?birth ?death ?image ?gnd ?viaf\n            (group_concat(DISTINCT(?professionLabel) ;separator = "| ") as ?professions )\n            (group_concat(DISTINCT(?aliasLabel) ;separator = "|") as ?aliasList )\n        WHERE {\n          SERVICE wikibase:mwapi {\n              bd:serviceParam wikibase:api "Search" .\n              bd:serviceParam wikibase:endpoint "www.wikidata.org" .\n              bd:serviceParam mwapi:srsearch "' + search + '" .\n              bd:serviceParam wikibase:limit 100 .\n              ?item wikibase:apiOutputItem mwapi:title .\n              ?ordinal wikibase:apiOrdinal true .\n         }\n           ?item wdt:P227 ?gnd.\n           ?item wdt:P214 ?viaf.\n           ?item wdt:P31 wd:Q5.\n           OPTIONAL {?item wdt:P569 ?birth.}\n           OPTIONAL {?item wdt:P570 ?death.}\n           OPTIONAL {?item wdt:P106 ?profession.}\n           OPTIONAL {?item wdt:P18 ?image.}\n           OPTIONAL {?item (rdfs:label|skos:altLabel|wdt:P1449) ?alias.}\n            SERVICE wikibase:label { bd:serviceParam wikibase:language "' + paramLang + '".\n                                  ?profession rdfs:label ?professionLabel .\n                                   ?alias rdfs:label ?aliasLabel .\n                                   ?item schema:description ?itemDescription .\n                                   ?item rdfs:label ?itemLabel .}\n        }\n        GROUP BY ?item ?itemLabel ?itemDescription ?birth ?death ?image ?gnd  ?viaf\n        ORDER BY ASC(?ordinal)\n        LIMIT 100\n        ';
        var fullUrl = endpointUrl + '?query=' + sparqlQuery;
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, { headers: { 'Accept': 'application/sparql-results+json' } }).then(function (response) {
            return response.data;
        }, function (httpError) {
            var error = "***ETH*** an error occured: wikidataService getPersons error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    function getResearcherProfile(qid, lang) {
        var paramLang = getParamLang(lang);
        var endpointUrl = 'https://query.wikidata.org/sparql';
        var sparqlQuery = '\n        SELECT ?item ?itemLabel ?orcid ?researcherid ?publonid ?scholar ?scopus\n        WHERE {\n          BIND(wd:' + qid + ' AS ?item).\n          ?item wdt:P31 wd:Q5.\n          OPTIONAL {?item wdt:P496 ?orcid.}\n          OPTIONAL {?item wdt:P1053 ?researcherid.}\n          OPTIONAL {?item wdt:P3829 ?publonid.}\n          OPTIONAL {?item wdt:P1960 ?scholar.}\n          OPTIONAL {?item wdt:P1153 ?scopus.}\n          SERVICE wikibase:label { bd:serviceParam wikibase:language "' + paramLang + '". }\n        }\n        ';
        var fullUrl = endpointUrl + '?query=' + encodeURIComponent(sparqlQuery);
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, { headers: { 'Accept': 'application/sparql-results+json' } }).then(function (response) {
            return response.data;
        }, function (httpError) {
            var error = "***ETH*** an error occured: wikidataService getResearcherProfile error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    function _getPerson(statement, lang) {
        var paramLang = getParamLang(lang);
        var endpointUrl = 'https://query.wikidata.org/sparql';
        var sparqlQuery = '\n        SELECT ?item ?itemLabel ?itemDescription ?birth ?death ?birthplaceLabel ?deathplaceLabel ?image ?gnd ?dodis ?sfa ?bdel ?hls ?loc ?viaf ?archived ?wc ?cerl\n            (group_concat(DISTINCT(?aliasLabel) ;separator = "|") as ?aliasList )\n            (group_concat(DISTINCT(?professionLabel) ;separator = ", ") as ?professions )\n        WHERE {\n          ' + statement + '.\n          ?item wdt:P31 wd:Q5.\n          OPTIONAL {?item wdt:P569 ?birth.}\n          OPTIONAL {?item wdt:P19 ?birthplace.}\n          OPTIONAL {?item wdt:P570 ?death.}\n          OPTIONAL {?item wdt:P20 ?deathplace.}\n          OPTIONAL {?item wdt:P106 ?profession.}\n          OPTIONAL {?item wdt:P18 ?image.}\n          OPTIONAL {?item wdt:P227 ?gnd.}\n          OPTIONAL {?item wdt:P701 ?dodis.}\n          OPTIONAL {?item wdt:P3889 ?sfa.}\n          OPTIONAL {?item wdt:P6231 ?bdel.}\n          OPTIONAL {?item wdt:P902 ?hls.}\n          OPTIONAL {?item wdt:P244 ?loc.}\n          OPTIONAL {?item wdt:P214 ?viaf.}\n          OPTIONAL {?item wdt:P485 ?archived.}\n          OPTIONAL {?item wdt:P373 ?wc.}\n          OPTIONAL {?item wdt:P1871 ?cerl.}\n          OPTIONAL {?item (rdfs:label|skos:altLabel|wdt:P1449) ?alias.}\n          SERVICE wikibase:label { bd:serviceParam wikibase:language "' + paramLang + '".\n                                ?profession rdfs:label ?professionLabel .\n                                 ?alias rdfs:label ?aliasLabel .\n                                 ?birthplace rdfs:label ?birthplaceLabel .\n                                 ?deathplace rdfs:label ?deathplaceLabel .\n                                 ?item schema:description ?itemDescription .\n                                 ?item rdfs:label ?itemLabel .}\n        }GROUP BY ?item ?itemLabel ?itemDescription ?birth ?death ?birthplaceLabel ?deathplaceLabel ?image ?gnd ?dodis ?sfa ?bdel ?hls ?loc ?viaf ?archived ?wc ?cerl\n        ';
        var fullUrl = endpointUrl + '?query=' + encodeURIComponent(sparqlQuery);
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, { headers: { 'Accept': 'application/sparql-results+json' } }).then(function (response) {
            return response.data;
        }, function (httpError) {
            var error = "***ETH*** an error occured: wikidataService _getPerson error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    function getPersonByGND(id, lang) {
        var statement = '?item wdt:P227 "' + id + '"';
        return _getPerson(statement, lang);
    }

    function getPersonByQID(qid, lang) {
        var statement = 'FILTER ( ?item = <http://www.wikidata.org/entity/' + qid + '> )';
        return _getPerson(statement, lang);
    }

    function getPersonGndByQID(qid, lang) {
        var paramLang = getParamLang(lang);
        var endpointUrl = 'https://query.wikidata.org/sparql';
        var sparqlQuery = '\n        SELECT ?item ?itemLabel ?gnd\n        WHERE {\n          BIND(wd:' + qid + ' AS ?item).\n          OPTIONAL {?item wdt:P227 ?gnd}.\n          SERVICE wikibase:label { bd:serviceParam wikibase:language "' + paramLang + '". }\n        }\n        ';
        var fullUrl = endpointUrl + '?query=' + encodeURIComponent(sparqlQuery);
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, { headers: { 'Accept': 'application/sparql-results+json' } }).then(function (response) {
            return response.data;
        }, function (httpError) {
            var error = "***ETH*** an error occured: wikidataService getPersonGndByQID error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    // archived at: reference
    // 116042621 Peter Debye
    function getArchivedOfPerson(qid, lang) {
        var paramLang = getParamLang(lang);
        var endpointUrl = 'https://query.wikidata.org/sparql';
        var sparqlQuery = '\n        SELECT ?statement ?archived ?archivedLabel ?refnode  ?ref ?inventoryno\n        where{\n            wd:' + qid + ' p:P485 ?statement.\n            ?statement ps:P485 ?archived.\n            OPTIONAL{?statement pq:P217 ?inventoryno.}\n            OPTIONAL{ ?statement prov:wasDerivedFrom ?refnode.\n              ?refnode pr:P854 ?ref.\n            }\n            SERVICE wikibase:label { bd:serviceParam wikibase:language "' + paramLang + '". }\n        }        ';
        var fullUrl = endpointUrl + '?query=' + sparqlQuery;
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, { headers: { 'Accept': 'application/sparql-results+json' } }).then(function (response) {
            return response.data;
        }, function (httpError) {
            var error = "***ETH*** an error occured: wikidataService getArchivedOfPerson error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    // bedeutende Schüler(P802) oder Doktorand(P185)
    function getStudentsOfPerson(qid, lang) {
        var paramLang = getParamLang(lang);
        var endpointUrl = 'https://query.wikidata.org/sparql';
        var sparqlQuery = '\n        SELECT DISTINCT ?item ?itemLabel ?itemDescription ?gnd ?image\n        (group_concat(DISTINCT(?birth) ;separator = " or ") as ?births )\n        (group_concat(DISTINCT(?death) ;separator = " or ") as ?deaths )\n        WHERE {\n          wd:' + qid + ' (wdt:P802|wdt:P185) ?item.\n          OPTIONAL {?item wdt:P18 ?image.}\n          OPTIONAL {?item wdt:P227 ?gnd.}\n          OPTIONAL {?item wdt:P569 ?birth.}\n          OPTIONAL {?item wdt:P570 ?death.}\n          OPTIONAL {?item wdt:P106 ?profession.}\n          SERVICE wikibase:label { bd:serviceParam wikibase:language "' + paramLang + '". }\n        }\n        GROUP BY ?item ?itemLabel ?itemDescription ?gnd ?image\n        ORDER BY ASC(?births)\n        ';
        var fullUrl = endpointUrl + '?query=' + sparqlQuery;
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, { headers: { 'Accept': 'application/sparql-results+json' } }).then(function (response) {
            return response.data;
        }, function (httpError) {
            var error = "***ETH*** an error occured: wikidataService getStudentsOfPerson error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    // Lehrer(P1066) oder Promotionsbetreuer(P184)
    function getTeachersOfPerson(qid, lang) {
        var paramLang = getParamLang(lang);
        var endpointUrl = 'https://query.wikidata.org/sparql';
        var sparqlQuery = '\n        SELECT DISTINCT ?item ?itemLabel ?itemDescription ?gnd ?image\n        (group_concat(DISTINCT(?birth) ;separator = " or ") as ?births )\n        (group_concat(DISTINCT(?death) ;separator = " or ") as ?deaths )\n        WHERE {\n          wd:' + qid + ' (wdt:P1066|wdt:P184) ?item.\n          OPTIONAL {?item wdt:P18 ?image.}\n          OPTIONAL {?item wdt:P227 ?gnd.}\n          OPTIONAL {?item wdt:P569 ?birth.}\n          OPTIONAL {?item wdt:P570 ?death.}\n          OPTIONAL {?item wdt:P106 ?profession.}\n          SERVICE wikibase:label { bd:serviceParam wikibase:language "' + paramLang + '". }\n        }\n        GROUP BY ?item ?itemLabel ?itemDescription ?gnd ?image\n        ORDER BY ASC(?births)\n        ';
        var fullUrl = endpointUrl + '?query=' + sparqlQuery;
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, { headers: { 'Accept': 'application/sparql-results+json' } }).then(function (response) {
            return response.data;
        }, function (httpError) {
            var error = "***ETH*** an error occured: wikidataService getTeachersOfPerson error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }

    // http://fortinet.also.ch/wiki/api.php?action=help&modules=query&submodules=1
    // https://en.wikipedia.org/w/api.php?action=query&prop=pageprops&titles=Kunsthaus_Z%C3%BCrich&format=json

    function enrichPOIsWithQID(pois) {
        var endpointUrl = 'https://de.wikipedia.org/w/api.php?action=query&prop=pageprops&format=json&origin=*';
        var titles = '';
        var counter = 1;
        pois.forEach(function (poi) {
            // todo max 50 titles
            if (counter <= 50) {
                var link = '';
                if (poi.link.indexOf('wikipedia') > -1) {
                    link = decodeURIComponent(poi.link);
                } else if (poi.link2.indexOf('wikipedia') > -1) {
                    link = decodeURIComponent(poi.link2);
                }
                if (link !== '') {
                    poi.wikiTitle = link.substring(link.lastIndexOf('/') + 1);
                    if (counter > 1) {
                        titles += '|';
                    }
                    titles += link.substring(link.lastIndexOf('/') + 1);
                    counter += 1;
                }
            }
        });
        if (titles === '') return null;
        var fullUrl = endpointUrl + '&titles=' + titles;
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl).then(function (response) {
            var mapTitleQid = [];
            for (var key in response.data.query.pages) {
                var page = response.data.query.pages[key];
                if (page.pageprops) {
                    (function () {
                        var title = page.title;
                        var qid = page.pageprops['wikibase_item'];
                        // todo optimize
                        if (response.data.query.normalized) {
                            var normalized = response.data.query.normalized.filter(function (e) {
                                return e.to === title;
                            });
                            if (normalized.length > 0) {
                                title = normalized[0].from;
                            }
                        }
                        mapTitleQid[title] = qid;
                    })();
                }
            }
            pois.forEach(function (poi) {
                if (poi.wikiTitle && mapTitleQid[poi.wikiTitle]) {
                    poi.qid = mapTitleQid[poi.wikiTitle];
                }
            });
            return pois;
        }, function (httpError) {
            var error = "***ETH*** an error occured: wikidataService enrichPOIsWithQID error callback: " + httpError.status;
            if (httpError.data && httpError.data.errorMessage) {
                error += ' - ' + httpError.data.errorMessage;
            }
            console.error(error);
            return null;
        });
    }
    function getParamLang(lang) {
        var paramLang = "de, en, fr";
        if (lang && lang === 'en_US') {
            paramLang = "en, de";
        } else if (lang && lang === 'fr_FR') {
            paramLang = "fr, en, de";
        }
        return paramLang;
    }

    return {
        getPlaces: getPlaces,
        getPlace: getPlace,
        getPersons: getPersons,
        getPersonByGND: getPersonByGND,
        getPersonByQID: getPersonByQID,
        getPersonGndByQID: getPersonGndByQID,
        getResearcherProfile: getResearcherProfile,
        getStudentsOfPerson: getStudentsOfPerson,
        getTeachersOfPerson: getTeachersOfPerson,
        getArchivedOfPerson: getArchivedOfPerson,
        enrichPOIsWithQID: enrichPOIsWithQID
        //getPlacesOfPerson: getPlacesOfPerson,
        //getPersonsByPlace: getPersonsByPlace,
        //getPersonsEmployedByPlace: getPersonsEmployedByPlace,
        //getPersonsEducatedAtPlace:getPersonsEducatedAtPlace,
        //getPersonsBornInPlace:getPersonsBornInPlace,
        //getPersonsBurriedInPlace: getPersonsBurriedInPlace,
    };
}]);
/* Alles was im HSA archiviert ist
SELECT ?item   ?itemLabel
where{
?item wdt:P485 wd:Q39934978.
SERVICE wikibase:label { bd:serviceParam wikibase:language "de,en". }
}
*/

/*
function getPlacesOfPerson(qid){
    const endpointUrl = 'https://query.wikidata.org/sparql';
    let sparqlQuery = `
    SELECT DISTINCT  ?place ?placeLabel ?image ?pred ?predLabel
    WHERE {
      wd:${qid} (wdt:P551|wdt:P20|wdt:P937|wdt:P1321|wdt:P19|wdt:P119|wdt:P69|wdt:P108) ?place.
      wd:${qid} ?pred ?place.
      ?place wdt:P17 wd:Q39.
      OPTIONAL {?place wdt:P18 ?image.}
      SERVICE wikibase:label { bd:serviceParam wikibase:language "de,en". }
    }
    LIMIT 100
    `;
    let fullUrl = endpointUrl + '?query=' + sparqlQuery;
    $sce.trustAsResourceUrl(fullUrl);

    return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
        .then(
            function(response){
                return response.data;
            },
            function(httpError){
                let error = "***ETH*** an error occured: wikidataService getPlacesOfPerson error callback: " + httpError.status;
                if (httpError.data && httpError.data.errorMessage) {
                    error += ' - ' + httpError.data.errorMessage;
                }
                console.error(error);
                return null;
            }
        )
}
*/
/*
function getPersonsByPlace(qid){
    let props = '(wdt:P551|wdt:P20|wdt:P937|wdt:P1321)';
    return _getPersonsByPlace(qid, props);
}

function getPersonsBornInPlace(qid){
    let props = 'wdt:P19';
    return _getPersonsByPlace(qid, props);
}

function getPersonsBurriedInPlace(qid){
    let props = 'wdt:P119';
    return _getPersonsByPlace(qid, props);
}

function getPersonsEducatedAtPlace(qid){
    let props = 'wdt:P69';
    return _getPersonsByPlace(qid, props);
}

function getPersonsEmployedByPlace(qid){
    let props = 'wdt:P108';
    return _getPersonsByPlace(qid, props);
}
*/
/*
// ?item wdt:P106 wd:Q1622272. Hochschullehrer
function _getPersonsByPlace(qid,props,lang){
    let paramLang = getParamLang(lang);
    const endpointUrl = 'https://query.wikidata.org/sparql';
    let sparqlQuery = `
    SELECT DISTINCT ?item ?itemLabel ?itemDescription ?gnd ?image
    (group_concat(DISTINCT(?birth) ;separator = " or ") as ?births )
    (group_concat(DISTINCT(?death) ;separator = " or ") as ?deaths )
    WHERE {
      ?item ${props} wd:${qid}.
      ?item wdt:P18 ?image.
      OPTIONAL {?item wdt:P227 ?gnd.}
      OPTIONAL {?item wdt:P569 ?birth.}
      OPTIONAL {?item wdt:P570 ?death.}
      OPTIONAL {?item wdt:P106 ?profession.}
      FILTER (!bound(?birth) || !(YEAR(?birth) > 1940))
      SERVICE wikibase:label { bd:serviceParam wikibase:language "${paramLang}". }
    }
    GROUP BY ?item ?itemLabel ?itemDescription ?gnd ?image
    ORDER BY ASC(?births)
    LIMIT 100
    `;
    let fullUrl = endpointUrl + '?query=' + sparqlQuery;
    $sce.trustAsResourceUrl(fullUrl);
    return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
        .then(
            function(response){
                return response.data;
            },
            function(httpError){
                let error = "***ETH*** an error occured: wikidataService _getPersonsByPlace error callback: " + httpError.status;
                if (httpError.data && httpError.data.errorMessage) {
                    error += ' - ' + httpError.data.errorMessage;
                }
                console.error(error);
                return null;
            }
        )
}

*/
})();