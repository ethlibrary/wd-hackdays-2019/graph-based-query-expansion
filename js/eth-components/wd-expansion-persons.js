/**
* @memberof WIKI
* @ngdoc component
* @name wdExpansionPersons
*
* @description
* WIKI Component:<br>
*
* - Query expansion via wikidata: persons for searchterm
*
* <b>Requirements</b><br>
*
* Service {@link WIKI.wikidataService}<br>
*
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.prmSearchResultListAfter}<br>
*
*
* <b>In Package</b><br>
*
* WIKI <br>
*
*/
app.component('ethWdExpansionPersons', {
    require: {
        parent: '^prmSearchResultListAfter'
    },
    template: `
    <div class="wikidata-persons" ng-if="$ctrl.persons.length > 0">
        <a name="wikidata-persons-anchor"></a>
        <md-card ng-repeat="person in $ctrl.persons" md-theme="default" md-theme-watch>
            <md-card-title>
                <md-card-title-text>
                    <span class="md-headline">{{person.label}}</span>
                    <span class="md-subhead">{{person.description}}</span>
                    <span class="md-subhead">{{person.professions}}</span>
                    <span class="md-subhead" ng-if="person.gnd">GND ID: {{person.gndid}}</span>
                    <span class="md-subhead">{{$ctrl.label.birthday[$ctrl.lang]}}: {{person.birth}}</span>
                </md-card-title-text>
                <md-card-title-media ng-if="person.image">
                    <img class="md-card-image" ng-src="{{person.image}}?width=200px" />
                </md-card-title-media>
            </md-card-title>
            <md-card-actions layout-xs="column" layout="row" layout-align="start center" layout-align-xs="start start">
                <md-button class="md-primary" ng-if="person.gndid" ng-click="$ctrl.loadPersonPageByGndId(person.gndid,person.qid)">{{$ctrl.label.searchThis[$ctrl.lang]}}</md-button>
            </md-card-actions>
        </md-card>
    </div>
    `,
    controller: class WdExpansionPersonsController {
        constructor( wikidataService, $location, $element, $scope, $compile, $anchorScroll, $rootScope ) {
            this.wikidataService = wikidataService;
            this.$location = $location;
            this.$element = $element;
            this.$scope = $scope;
            this.$compile = $compile;
            this.$anchorScroll = $anchorScroll;
            this.$rootScope = $rootScope;
            this.label = {
                persons: {
                    de_DE: 'Personen zu meinem Suchbegriff',
                    en_US: 'Persons matching my search term'
                },
                searchThis: {
                    de_DE: 'Diese Person suchen',
                    en_US: 'Search for this person'
                },
                birthday: {
                    de_DE: 'Geburtstag',
                    en_US: 'Birthday'
                }
            }
        }

        $onInit() {
            this.parentCtrl = this.parent.parentCtrl;
            // this.search: input in searchField
            this.search = '';
            // this.persons: object array of persons
            this.persons = [];
            this.lang = this.getLanguage(this.$rootScope);

            let query = this.parentCtrl.searchService.searchUtil.$stateParams.query;
            if(query.indexOf('lsr04') > -1){
                this.search = query.replace('lsr04,contains,','');
            }
            else{
                this.search = query.replace('any,contains,','');
            }
            // check querystring for searchfor
            let searchFor = this.$location.search().searchfor;
            if(!searchFor && this.search.indexOf('[wd/') === -1){
                this.getPersons(this.search);
            }
        };

        getPersons(search){
            this.wikidataService.getPersons(search, this.lang)
                .then((data) => {
                    if(!data.results || !data.results.bindings || data.results.bindings.length === 0)
                            return null;
                    let lastItemValue = '';
                    data.results.bindings.forEach(e => {
                        if(e.item.value != lastItemValue && e.item.value.indexOf(e.itemLabel.value) === -1){
                            let aVariants = e.aliasList.value.split('|');
                            if(aVariants.indexOf(e.itemLabel.value) === -1){
                                aVariants.unshift(e.itemLabel.value);
                            }
                            let variantQuery = this.buildPrimoQueryValue(aVariants);
                            // set properties of person object array
                            this.persons.push({
                                'qid':e.item.value.substring(e.item.value.lastIndexOf('/')+1),
                                'label':e.itemLabel.value,
                                'description':e.itemDescription ? e.itemDescription.value : null,
                                'alias':e.aliasList ? e.aliasList.value : null,
                                'image':e.image ? e.image.value : null,
                                'birth':e.birth ? e.birth.value.substring(0,e.birth.value.indexOf('T')) : null,
                                'professions':e.professions ? e.professions.value:null,
                                'gndid':e.gnd ? e.gnd.value : null,
                                'viafid':e.viaf ? e.viaf.value : null,
                                'variantQuery': variantQuery,
                                'yearOfBirth':e.birth? e.birth.value.substring(0,e.birth.value.indexOf('-')): null
                            });
                            lastItemValue = e.item.value;
                        }
                    });
                    // render link at top of page
                    let resultList = angular.element(this.$element[0].parentElement.parentElement);
                    if(resultList){
                        let html = `
                            <div class="wikidata-search-suggestion">
                                <a class="wikidata-anchor-link" ng-click="$ctrl.$location.hash('wikidata-persons-anchor');$ctrl.$anchorScroll();">
                                    {{$ctrl.label.persons[$ctrl.lang]}}
                                </a>
                            </div>
                        `;
                        resultList.prepend(this.$compile(html)(this.$scope));
                    }
                })
                .catch( (e) => {
                    console.error("***ETH*** an error occured: getPersons");
                    console.error(e.message);
                })
        }

        buildPrimoQueryValue(items){
            // in primo 7 rows and max 30 boolean operators
            /*
            The system will display a message and provide suggestions when the following limits are exceeded:
            The query contains more than 30 boolean operators.
            The query contains more than 8 question marks.
            The query contains more than 8 asterisks and the word length is greater than 2 (such as abb* or ab*c).
            The query contains more than 4 asterisks and the word length is less than 3 (such as ab*).
            The entire query consists of a single letter and an asterisk (such as a*).
            */
            let query = '';
            let maxItems = 20;
            let maxLength = 1000;
            items.forEach( (e, i) => {
                if (i < maxItems) {
                    if((query.length + e.length) < maxLength)
                    {
                        query += e;
                        if(i < items.length-1 && i < (maxItems-1)){
                            query += ' OR ';
                        }
                    }
                }
            });
            return query;
        }

        loadPersonPageByGndId(gndid, qid){
            let query = gndid;
            if(!gndid){
                query = qid;
            }
            let url = `/primo-explore/search?query=lsr04,contains,${query}&tab=default_tab&vid=WIKI&lang=${this.lang}&searchfor=person/${qid}`;
            location.href = url;
        }

        getLanguage(){
            let sms = this.$rootScope.$$childHead.$ctrl.userSessionManagerService;
            if (!sms) {
                console.error("***ETH*** WdExpansionPersonController: userSessionManagerService not available");
                return 'de';
            }
            else{
                return sms.getUserLanguage() || $window.appConfig['primo-view']['attributes-map'].interfaceLanguage;
            }
        }
    }
});

/*
<md-button class="md-primary" ng-click="$ctrl.loadBestPossiblePersonPage(person);">Suche nach Name und Geburtsjahr oder eindeutiger Identifikationnummer (GND)</md-button>
<md-button class="md-primary" ng-click="$ctrl.loadPersonPageByLabel(person);">Suche nach Name</md-button>
<md-button class="md-primary" ng-click="$ctrl.loadPersonPageByVariantQuery(person);">Suche nach Namensvarianten</md-button>

loadPersonPageByLabel(person){
    let query = person.label;
    let url = `/primo-explore/search?query=any,contains,${query}&tab=default_tab&vid=WIKI&lang=de_DE&searchfor=person/${person.qid}`;
    location.href = url;
}

loadPersonPageByVariantQuery(person){
    let url = `/primo-explore/search?tab=default_tab&search_scope=default_scope&vid=WIKI&lang=de_DE&offset=0&searchfor=person/${person.qid}&query=any,contains,${person.variantQuery}`;
    location.href = url;
}

loadBestPossiblePersonPage(person){
    let query = person.label;
    if(person.yearOfBirth &&  person.gndid){
        query = query + ' AND (' + person.yearOfBirth + ' OR ' + person.gndid + ')';
    }
    else if (person.yearOfBirth) {
        query = query + ' ' + person.yearOfBirth;
    }
    else if (person.gndid) {
        query = query + ' ' + person.gndid;
    }
    let url = `/primo-explore/search?tab=default_tab&search_scope=default_scope&vid=WIKI&lang=de_DE&offset=0&searchfor=person/${person.qid}&query=any,contains,${query}`;
    location.href = url;
}
*/
