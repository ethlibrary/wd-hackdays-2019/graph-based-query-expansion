/**
* @memberof WIKI
* @ngdoc component
* @name wdExpansionPerson
*
* @description
* WIKI Component:<br>
*
* - person page
* - switch between search variants (precision/recall)
* - informations and links for the person
*
*
* <b>Requirements</b><br>
*
* Service {@link WIKI.wikidataService}<br>
* Service {@link WIKI.daasService}<br>
*
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.prmSearchResultListAfter}<br>
*
*
* <b>In Package</b><br>
*
* WIKI <br>
*
*/
app.component('ethWdExpansionPerson', {
    require: {
        parent: '^prmSearchResultListAfter'
    },
    template: `
    <div class="wikidata-person" ng-if="$ctrl.person && $ctrl.person.item">
        <a name="wikidata-person-searchprecision-anchor"></a>
        <div class="wikidata-person-searchprecision">
            <h2>{{$ctrl.person.label}}: {{$ctrl.label.precision[$ctrl.lang]}}?</h2>
            <p class="wikidata-person-searchprecision__intro">
                {{$ctrl.label.precisionIntro1[$ctrl.lang]}}
                <span class="wikidata-person-searchprecision__gnd1"></span>
                {{$ctrl.label.precisionIntro2[$ctrl.lang]}}
                <b>{{$ctrl.label.precisionIntro3[$ctrl.lang]}}</b><br>
                {{$ctrl.label.precisionIntro4[$ctrl.lang]}}
            </p>
            <div class="wikidata-person-searchprecision__options">
                <div ng-if="$ctrl.person.gnd" class="wikidata-person-searchprecision__option-gnd">
                    <md-button active="true" class="md-primary wikidata-person-searchprecision wikidata-person-searchprecision-gnd" ng-click="$ctrl.searchPersonByGndId($ctrl.person.gndid,$ctrl.person.qid)"><span class="wikidata-person-searchprecision__gnd2"></span> {{$ctrl.label.precisionGndTitle[$ctrl.lang]}}</md-button>
                    <p>
                        {{$ctrl.label.precisionGnd1[$ctrl.lang]}}
                        <b>{{$ctrl.label.precisionGnd2[$ctrl.lang]}}</b>?
                        {{$ctrl.label.precisionGnd3[$ctrl.lang]}}
                    </p>
                </div>
                <div>
                    <md-button class="md-primary wikidata-person-searchprecision wikidata-person-searchprecision-birthyear" ng-click="$ctrl.searchBestPossibleForPerson($ctrl.person);"><span class="wikidata-person-searchprecision__best"></span> {{$ctrl.label.precisionBestTitle[$ctrl.lang]}}</md-button>
                    <p>
                        {{$ctrl.label.precisionBest1[$ctrl.lang]}}
                        <b>{{$ctrl.label.precisionBest2[$ctrl.lang]}}</b>
                        {{$ctrl.label.precisionBest3[$ctrl.lang]}}
                    </p>
                </div>
                <div>
                    <md-button class="md-primary wikidata-person-searchprecision wikidata-person-searchprecision-label" ng-click="$ctrl.searchPersonByLabel($ctrl.person);"><span class="wikidata-person-searchprecision__name"></span> {{$ctrl.label.precisionNameTitle[$ctrl.lang]}}</md-button>
                    <p>
                        {{$ctrl.label.precisionName1[$ctrl.lang]}}
                        <b>{{$ctrl.label.precisionName2[$ctrl.lang]}}</b>
                        {{$ctrl.label.precisionName3[$ctrl.lang]}}
                    </p>
                </div>
                <div>
                    <md-button class="md-primary wikidata-person-searchprecision wikidata-person-searchprecision-variants" ng-click="$ctrl.searchPersonByVariantQuery($ctrl.person);"><span class="wikidata-person-searchprecision__variants"></span> {{$ctrl.label.precisionNameVariantsTitle[$ctrl.lang]}}</md-button>
                    <p>
                        {{$ctrl.label.precisionNameVariants1[$ctrl.lang]}}
                        <b>{{$ctrl.label.precisionNameVariants2[$ctrl.lang]}}</b>
                        {{$ctrl.label.precisionNameVariants3[$ctrl.lang]}}
                    </p>
                </div>
            </div>
        </div>
        <a name="wikidata-person-anchor"></a>
        <md-card md-theme="default" md-theme-watch>
            <md-card-title>
                <md-card-title-text>
                    <!-- span class="wikidata-source-hint">Die folgenden Informationen sind aus Wikidata, Metagrid und der GND</span-->
                    <h2 class="md-headline">{{$ctrl.person.itemLabel.value}}</h2>
                </md-card-title-text>
            </md-card-title>
            <md-card-content>
                <div class="wikidata-person__section wikidata-person__section-first">
                    <div>
                        <h3 class="md-subhead">{{$ctrl.label.infoWD[$ctrl.lang]}}</h3>
                        <span>{{$ctrl.person.itemDescription.value}}</span>
                        <span>{{$ctrl.person.professions.value}}</span>
                        <span ng-if="$ctrl.person.gnd">GND ID: {{$ctrl.person.gnd.value}}</span>
                        <span ng-if="$ctrl.person.birth">{{$ctrl.label.born[$ctrl.lang]}} {{$ctrl.person.birth.value.substring(0,$ctrl.person.birth.value.indexOf('T'))}} in {{$ctrl.person.birthplaceLabel.value}}</span>
                        <span ng-if="$ctrl.person.death">{{$ctrl.label.died[$ctrl.lang]}} {{$ctrl.person.death.value.substring(0,$ctrl.person.death.value.indexOf('T'))}} in {{$ctrl.person.deathplaceLabel.value}}</span>
                    </div>
                    <img ng-if="$ctrl.person.image.value" class="md-card-image" ng-src="{{$ctrl.person.image.value}}?width=200px" />
                </div>
                <div class="wikidata-person__section" ng-if="$ctrl.person.entityfacts.biographicalOrHistoricalInformation">
                    <h3 class="md-subhead">{{$ctrl.label.infoGND[$ctrl.lang]}}</h3>
                    <span ng-if="$ctrl.person.entityfacts.biographicalOrHistoricalInformation">{{$ctrl.person.entityfacts.biographicalOrHistoricalInformation}}</span>
                    <span ng-if="$ctrl.person.entityfacts.professionOrOccupation">{{$ctrl.person.entityfacts.professionOrOccupation}}</span>
                    <span ng-if="$ctrl.person.entityfacts.dateOfBirth">{{$ctrl.label.born[$ctrl.lang]}} {{$ctrl.person.entityfacts.dateOfBirth}} in {{$ctrl.person.entityfacts.placeOfBirth}}</span>
                    <span ng-if="$ctrl.person.entityfacts.dateOfDeath">{{$ctrl.label.died[$ctrl.lang]}} {{$ctrl.person.entityfacts.dateOfDeath}} in {{$ctrl.person.entityfacts.placeOfDeath}}</span>
                </div>
                <div ng-if="$ctrl.person.archived  && $ctrl.person.archived.length>0" class="wikidata-person__section">
                    <h3 class="md-subhead">{{$ctrl.label.archives[$ctrl.lang]}}</h3>
                    <p ng-repeat="object in $ctrl.person.archived">
                        <span ng-if="!object.ref.value"">
                            <span>{{object.archivedLabel.value}}</span>
                            <span ng-if="object.inventoryno"> (Inventarnummer: {{object.inventoryno.value}})</span>
                        </span>
                        <a target="_blank" ng-if="object.ref.value" ng-href="{{object.ref.value}}">
                            <span>{{object.archivedLabel.value}}</span>
                            <span ng-if="object.inventoryno"> (Inventarnummer: {{object.inventoryno.value}})</span>
                        </a>
                    </p>
                </div>
                <div class="wikidata-person__section">
                    <div ng-if="$ctrl.person.researcherProfile  && $ctrl.person.researcherProfile.length>0" class="wikidata-person__section">
                        <h3 class="md-subhead">{{$ctrl.label.researcherProfile[$ctrl.lang]}}</h3>
                        <a ng-repeat="object in $ctrl.person.researcherProfile" target="_blank" ng-href="{{object.url}}">
                            {{object.label}}
                        </a>
                    </div>
                    <h3 class="md-subhead">{{$ctrl.label.linksWD[$ctrl.lang]}}</h3>
                        <a target="_blank" href="{{$ctrl.person.item.value}}">Wikidata</a>
                        <a ng-if="$ctrl.person.wc" target="_blank" href="https://commons.wikimedia.org/wiki/Category:{{$ctrl.person.wc.value}}">Wikimedia Commons</a>
                        <a ng-if="$ctrl.person.hls" target="_blank" href="http://www.hls-dhs-dss.ch/textes/d/D{{$ctrl.person.hls.value}}.php">HLS (Historisches Lexikon der Schweiz)</a>
                        <a ng-if="$ctrl.person.viaf" target="_blank" href="https://viaf.org/viaf/{{$ctrl.person.viaf.value}}/">VIAF (Virtual International Authority File)</a>
                        <a ng-if="$ctrl.person.gnd" target="_blank" href="https://d-nb.info/gnd/{{$ctrl.person.gnd.value}}">GND der DNB (Deutsche Nationalbibliothek)</a>
                        <a ng-if="$ctrl.person.sfa" target="_blank" href="https://www.swiss-archives.ch/archivplansuche.aspx?ID={{$ctrl.person.sfa.value}}">SBA (Schweizerisches Bundesarchiv)</a>
                        <a ng-if="$ctrl.person.dodis" target="_blank" href="https://dodis.ch/{{$ctrl.person.dodis.value}}">DODIS (Diplomatische Dokumente der Schweiz)</a>
                        <a ng-if="$ctrl.person.bdel" target="_blank" href="https://www2.unil.ch/elitessuisses/index.php?page=detailPerso&idIdentite={{$ctrl.person.bdel.value}}">BDEL (Schweizerische Eliten im 20. Jahrhundert)</a>
                        <a ng-if="$ctrl.person.loc" target="_blank" href="http://id.loc.gov/authorities/names/{{$ctrl.person.loc.value}}.html">LOC (Library of Congress)</a>
                        <a ng-if="$ctrl.person.cerl" target="_blank" href="https://data.cerl.org/thesaurus/{{$ctrl.person.cerl.value}}">CERL Thesaurus</a>
                    <div ng-if="$ctrl.person.metagrid && $ctrl.person.metagrid.length>0">
                        <h3 class="md-subhead">{{$ctrl.label.linksMetagrid[$ctrl.lang]}}</h3>
                        <p class="wikidata-person__links-hint">Links powered by <a target="_blank" href="https://www.metagrid.ch/" class="wikidata-person__links-source md-default-theme">Metagrid</a></p>
                        <a target="_blank" ng-repeat="object in $ctrl.person.metagrid" ng-href="{{object.url}}">{{object.label}}</a>
                    </div>
                    <div ng-if="$ctrl.person.findbuchLinks && $ctrl.person.findbuchLinks.length>0">
                        <h3 class="md-subhead">{{$ctrl.label.linksFindbuch[$ctrl.lang]}}</h3>
                        <p class="wikidata-person__links-hint">
                            {{$ctrl.label.linksFindbuchHint1[$ctrl.lang]}}
                            <a target="_blank" href="http://beacon.findbuch.de/seealso/pnd-aks" class="wikidata-person__links-source md-default-theme">pnd-aks</a>,
                            {{$ctrl.label.linksFindbuchHint2[$ctrl.lang]}}
                            <a target="_blank" href="https://de.wikipedia.org/wiki/Wikipedia:BEACON" class="wikidata-person__links-source md-default-theme">BEACON</a>
                            {{$ctrl.label.linksFindbuchHint3[$ctrl.lang]}}
                        </p>
                        <a target="_blank" ng-repeat="object in $ctrl.person.findbuchLinks" ng-href="{{object.url}}">{{object.label}}</a>
                    </div>
                </div>

                <div ng-if="$ctrl.person.students  && $ctrl.person.students.length>0" class="wikidata-person__section">
                    <h3 class="md-subhead">{{$ctrl.label.students[$ctrl.lang]}}</h3>
                    <p class="wikidata-person-small" ng-repeat="object in $ctrl.person.students">
                        <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
                            <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
                            <span class="wikidata-person-small__text">{{object.itemLabel.value}} ({{object.itemDescription.value}}, {{$ctrl.label.birthday[$ctrl.lang]}}: {{object.births.value.substring(0,object.births.value.indexOf('T'))}})</span>
                        </a>
                    </p>
                </div>
                <div ng-if="$ctrl.person.teachers  && $ctrl.person.teachers.length > 0" class="wikidata-person__section">
                    <h3 class="md-subhead">{{$ctrl.label.teachers[$ctrl.lang]}}</h3>
                    <p class="wikidata-person-small" ng-repeat="object in $ctrl.person.teachers">
                        <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
                            <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
                            <span class="wikidata-person-small__text">{{object.itemLabel.value}} ({{object.itemDescription.value}}, {{$ctrl.label.birthday[$ctrl.lang]}}: {{object.births.value.substring(0,object.births.value.indexOf('T'))}})</span>
                        </a>
                    </p>
                </div>
            </md-card-content>
        </md-card>
    </div>
    `,

    controller: class WdExpansionPersonController {
        constructor( wikidataService, daasService, $location, $element, $scope, $compile, $anchorScroll, $timeout, $rootScope) {
            this.wikidataService = wikidataService;
            this.daasService = daasService;
            this.$location = $location;
            this.$element = $element;
            this.$scope = $scope;
            this.$compile = $compile;
            this.$anchorScroll = $anchorScroll;
            this.$timeout = $timeout;
            this.$rootScope = $rootScope;
            this.label = {
                results: {
                    de_DE: 'Ergebnisse',
                    en_US: 'results'
                },
                precision: {
                    de_DE: 'Genauere oder mehr Ergebnisse',
                    en_US: 'More precise or more results'
                },
                moreInformations: {
                    de_DE: 'Mehr Informationen zur Person',
                    en_US: 'More information about the person'
                },
                infoWD: {
                    de_DE: 'Informationen aus Wikidata',
                    en_US: 'Informations from Wikidata'
                },
                infoGND: {
                    de_DE: 'Informationen aus der GND (Deutsche Nationalbibliothek)',
                    en_US: 'Informations from the GND (Deutsche Nationalbibliothek)'
                },
                archives: {
                    de_DE: 'Archive',
                    en_US: 'Archives'
                },
                researcherProfile: {
                    de_DE: 'Forscherprofile',
                    en_US: 'Researcher profiles'
                },
                linksWD: {
                    de_DE: 'Externe Links aus Wikidata',
                    en_US: 'External links from Wikidata'
                },
                linksMetagrid: {
                    de_DE: 'Externe Links von Metagrid',
                    en_US: 'External links from Metagrid'
                },
                linksFindbuch: {
                    de_DE: 'Externe Links von beacon.findbuch',
                    en_US: 'External links from beacon.findbuch'
                },
                linksFindbuchHint1: {
                    de_DE: 'Links von dem SeeAlso-Service',
                    en_US: 'Links from the SeeAlso-Service'
                },
                linksFindbuchHint2: {
                    de_DE: 'der auf',
                    en_US: 'which is based on'
                },
                linksFindbuchHint3: {
                    de_DE: 'basiert',
                    en_US: ''
                },
                students: {
                    de_DE: 'Bedeutende Schüler oder Doktoranden (aus Wikidata)',
                    en_US: 'Important students or doctoral students (from Wikidata)'
                },
                teachers: {
                    de_DE: 'Lehrer (aus Wikidata)',
                    en_US: 'Teachers (from Wikidata)'
                },
                born: {
                    de_DE: 'Geboren am',
                    en_US: 'Born on'
                },
                died: {
                    de_DE: 'Gestorben am',
                    en_US: 'Died on'
                },
                precisionIntro1: {
                    de_DE: 'Sie sehen',
                    en_US: 'You see'
                },
                precisionIntro2: {
                    de_DE: 'Ergebnisse der',
                    en_US: 'results of the'
                },
                precisionIntro3: {
                    de_DE: 'bei Suche nach einer eindeutigen Identifikationsnummer der Person (GND).',
                    en_US: 'in search for a unique identification number of the person (GND).'
                },
                precisionIntro4: {
                    de_DE: 'Sie sehen möglicherweise nicht alle Ergebnisse, da die Identifikationsnummer der Person nicht bei allen Ressourcen zugeordnet ist.',
                    en_US: 'You may not see all results because the identification number of the person is not assigned to all resources.However, you can expand the search to see more results.'
                },
                precisionGndTitle: {
                    de_DE: 'Nur nach eindeutiger Identifikationsnummer (GND) suchen',
                    en_US: 'Search only for unique identification number (GND)'
                },
                precisionGnd1: {
                    de_DE: 'Wollen Sie nach der',
                    en_US: 'Do you want to search for'
                },
                precisionGnd2: {
                    de_DE: 'eindeutigen Identifikationsnummer der Person (GND) suchen',
                    en_US: 'the unique identification number of the person (GND)'
                },
                precisionGnd3: {
                    de_DE: 'Sie sehen möglicherweise nicht alle Ergebnisse, da die Identifikationsnummer nicht bei allen Ressourcen zugeordnet ist.',
                    en_US: 'You may not see all results because the identification number is not assigned to all resources.'
                },
                precisionBestTitle: {
                    de_DE: 'bei Suche nach Name und Geburtsjahr oder eindeutiger Identifikationsnummer (GND)',
                    en_US: 'in search by name and year of birth or unique identification number (GND)'
                },
                precisionBest1: {
                    de_DE: 'Wollen Sie nach dem',
                    en_US: 'Do you want to search for the'
                },
                precisionBest2: {
                    de_DE: 'Namen suchen, eingeschränkt durch Identifikationsnummer der Person (GND) oder das Geburtsjahr',
                    en_US: 'name, restricted by the identification number of the person (GND) or the year of birth'
                },
                precisionBest3: {
                    de_DE: 'und damit mehr Ergebnisse sehen? Sie sehen möglicherweise nicht alle Ergebnisse, da Geburtsjahr oder Identifikationsnummer nicht allen Ressourcen zugeordnet sind.',
                    en_US: 'and see more results? You may not see all results because the birth year or identification number is not assigned to all resources.'
                },
                precisionNameTitle: {
                    de_DE: 'bei Suche nach Name',
                    en_US: 'in search by name'
                },
                precisionName1: {
                    de_DE: 'Wollen Sie',
                    en_US: 'Do you want to search'
                },
                precisionName2: {
                    de_DE: 'nur nach dem Namen',
                    en_US: 'by name only'
                },
                precisionName3: {
                    de_DE: 'suchen? Sie sehen möglicherweise auch falsche Ergebnisse, wenn mehrere Personen denselben Namen tragen.',
                    en_US: 'You may also see wrong results if several people have the same name.'
                },
                precisionNameVariantsTitle: {
                    de_DE: 'bei Suche nach Namensvarianten',
                    en_US: 'in search by name variants'
                },
                precisionNameVariants1: {
                    de_DE: 'Wollen Sie nach',
                    en_US: 'Do you want to search for'
                },
                precisionNameVariants2: {
                    de_DE: 'dem Namen und den Namensvarianten (aus Wikidata)',
                    en_US: 'the name and the name variants (from Wikidata)'
                },
                precisionNameVariants3: {
                    de_DE: 'suchen? Sie sehen möglicherweise auch falsche Ergebnisse, wenn mehrere Personen einen der Namen tragen.',
                    en_US: '? You may also see wrong results if several people have one of the names.'
                },
                birthday: {
                    de_DE: 'Geburtstag',
                    en_US: 'Birthday'
                }
            }
        }

        $onInit() {
            this.parentCtrl = this.parent.parentCtrl;
            // this.search: input in searchField
            this.search = '';
            this.person = {};
            this.lang = this.getLanguage(this.$rootScope);
            let query = this.parentCtrl.searchService.searchUtil.$stateParams.query;
            if(query.indexOf('lsr04') > -1){
                this.search = query.replace('lsr04,contains,','');
            }
            else{
                this.search = query.replace('any,contains,','');
            }

            // check querystring for searchfor
            let searchFor = this.$location.search().searchfor;

            // call by searchpage
            if (searchFor && searchFor.indexOf('person/') > -1) {
                this.getPerson(searchFor.substring(searchFor.indexOf('/')+1));
            }
            // permalink
            else if (this.search.indexOf('[wd/person]') > -1) {
                this.getPerson(this.search.substring(this.search.indexOf(']')+1));
            }
        };

        getPerson(qid){
            this.wikidataService.getPersonGndByQID(qid, this.lang)
                // do a quick search for item and gnd
                .then((data) => {
                    if(!data.results || !data.results.bindings || data.results.bindings.length === 0)
                            return null;
                    if (this.search.indexOf('[wd/person]') > -1) {
                        if(data.results.bindings[0].gnd){
                            let gndid = data.results.bindings[0].gnd.value;
                            this.parentCtrl.searchService.searchFieldsService.mainSearch = gndid;
                            this.$timeout(() => {
                                this.parentCtrl.searchService.search({query: 'lsr04,contains,' + gndid},true);
                            }, 300);
                        }
                        else{
                            let label = data.results.bindings[0].itemLabel.value;
                            this.parentCtrl.searchService.searchFieldsService.mainSearch = label;
                            this.$timeout(() => {
                                this.parentCtrl.searchService.search({query: 'any,contains,' + label},true);
                            }, 300);
                        }
                    }
                    return this.wikidataService.getPersonByQID(qid, this.lang);
                })
                // get additional informations
                .then((data) => {
                    if(!data || !data.results || !data.results.bindings || data.results.bindings.length === 0)
                            return null;
                    // set properties of person object
                    this.person = data.results.bindings[0];

                    // name variants / aliases
                    let aVariants = data.results.bindings[0].aliasList.value.split('|');
                    if(aVariants.indexOf(this.person.itemLabel.value) === -1){
                        aVariants.unshift(this.person.itemLabel.value);
                    }
                    this.person.variantQuery = this.buildPrimoQueryValue(aVariants);
                    if(this.person.gnd){
                        this.person.gndid = this.person.gnd.value;
                    }
                    this.person.label = this.person.itemLabel.value;
                    this.person.qid = qid;
                    this.person.yearOfBirth = this.person.birth ? this.person.birth.value.substring(0,this.person.birth.value.indexOf('-')): null;

                    // render precision and recall navigation
                    try{
                        this.enrichPRNavigation();
                    }
                    catch(e){
                        throw(e)
                        console.error("***ETH*** an error occured: wdExpansionPerson.enrichPRNavigation");
                        console.error(e.message);
                    }
                    return this.wikidataService.getResearcherProfile(qid, this.lang);
                })
                .then((data) => {
                    if(!data || !data.results || !data.results.bindings || data.results.bindings.length === 0)
                            return null;
                    this.person.researcherProfile = [];
                    if(data.results.bindings[0].orcid && data.results.bindings[0].orcid.value){
                        this.person.researcherProfile.push({'orcid': data.results.bindings[0].orcid.value, 'label': 'ORCID ID', 'url': 'https://orcid.org/' + data.results.bindings[0].orcid.value});
                    }
                    if(data.results.bindings[0].publonid && data.results.bindings[0].publonid.value){
                        this.person.researcherProfile.push({'publonid': data.results.bindings[0].publonid.value, 'label': 'Publons author ID', 'url': 'https://publons.com/researcher/' + data.results.bindings[0].publonid.value});
                    }
                    if(data.results.bindings[0].researcherid && data.results.bindings[0].researcherid.value){
                        this.person.researcherProfile.push({'researcherid': data.results.bindings[0].researcherid.value, 'label': 'ResearcherID', 'url': 'https://www.researcherid.com/rid/' + data.results.bindings[0].researcherid.value});
                    }
                    if(data.results.bindings[0].scholar && data.results.bindings[0].scholar.value){
                        this.person.researcherProfile.push({'scholar': data.results.bindings[0].scholar.value, 'label': 'Google Scholar author ID', 'url': 'https://scholar.google.com/citations?user=' + data.results.bindings[0].scholar.value});
                    }
                    if(data.results.bindings[0].scopus && data.results.bindings[0].scopus.value){
                        this.person.researcherProfile.push({'scopus': data.results.bindings[0].scopus.value, 'label': 'Scopus author ID', 'url': 'https://www.scopus.com/authid/detail.uri?authorId=' + data.results.bindings[0].scopus.value});
                    }
                    return this.person.qid;
                })
                .then((qid) => {
                    if(!qid) return null;

                    if(this.person.gnd){
                        this.daasService.searchEntityfacts(this.person.gnd.value)
                            .then((data) => {
                                this.person.entityfacts = data;
                            })
                    }
                    if(this.person.archived && this.person.archived.value && this.person.archived.value != ''){
                        this.wikidataService.getArchivedOfPerson(qid, this.lang)
                            .then((data) => {
                                if(data.results && data.results.bindings){
                                    this.person.archived = data.results.bindings;
                                }
                                else{
                                    this.person.archived = null;
                                }
                            })
                    }
                    this.wikidataService.getStudentsOfPerson(qid, this.lang)
                        .then((data) => {
                            if(data.results && data.results.bindings){
                                this.person.students = data.results.bindings;
                            }
                            else{
                                this.person.students = null;
                            }
                        })

                    this.wikidataService.getTeachersOfPerson(qid, this.lang)
                        .then((data) => {
                            if(data.results && data.results.bindings){
                                this.person.teachers = data.results.bindings;
                            }
                            else{
                                this.person.teachers = null;
                            }
                        })

                    if(this.person.gnd){
                        this.daasService.searchMetagrid(this.person.gnd.value)
                            .then((data) => {
                                if(!data || !data.resources)return;
                                let sourcesWhitelist = ["sikart","elites-suisses-au-xxe-siecle","bsg", "dodis", "helveticarchives", "helveticat", "hls-dhs-dss", "ethz", "histoirerurale","lonsea","ssrq","alfred-escher","geschichtedersozialensicherheit","history-state","rag"];
                                let whitelistedMetagridLinks = [];
                                for(var j=0; j < data.resources.length; j++){
                                    let resource = data.resources[j];
                                    // https://api.metagrid.ch/providers.json
                                    let slug = resource.slug;
                                    let url = resource.uri;
                                    if (sourcesWhitelist.indexOf(slug) === -1) {
                                        continue;
                                    }
                                    let label = slug;
                                    if (slug === 'elites-suisses-au-xxe-siecle') {
                                        label = "Elites suisses au XXe siècle";
                                    }
                                    else if (slug === 'sikart') {
                                        label = "SIKART Lexikon zur Kunst in der Schweiz";
                                    }
                                    else if (slug === 'history-state') {
                                        label = "FRUS (Office of the Historian)";
                                    }
                                    else if (slug === 'rag') {
                                        label = "Repertorium Academicum Germanicum";
                                    }
                                    else if (slug === 'bsg') {
                                        label = "Bibliographie der Schweizergeschichte";
                                    }
                                    else if (slug === 'dodis') {
                                        label = "Diplomatische Dokumente der Schweiz";
                                    }
                                    else if (slug === 'helveticarchives') {
                                        label = "HelveticArchives";
                                    }
                                    else if (slug === 'helveticat') {
                                        label = "Helveticat";
                                    }
                                    else if (slug === 'hls-dhs-dss') {
                                        label = "Historisches Lexikon der Schweiz";
                                    }
                                    else if (slug === 'ethz') {
                                        label = "ETH Zürich Hochschularchiv";
                                    }
                                    else if (slug === 'histoirerurale') {
                                        label = "Archiv für Agrargeschichte";
                                    }
                                    else if (slug === 'lonsea') {
                                        label = "Lonsea";
                                    }
                                    else if (slug === 'ssrq') {
                                        label = "Sammlung Schweizerischer Rechtsquellen";
                                    }
                                    else if (slug === 'alfred-escher') {
                                        label = "Alfred Escher-Briefedition";
                                    }
                                    else if (slug === 'geschichtedersozialensicherheit') {
                                        label = "Geschichte der sozialen Sicherheit";
                                    }
                                    whitelistedMetagridLinks.push({'slug': slug,'url': url, 'label': label});
                                }
                                this.person.metagrid = whitelistedMetagridLinks;
                            })
                        this.daasService.searchFindbuch(this.person.gnd.value)
                            .then((data) => {
                                if(!data.uris)
                                    return;
                                let sourcesWhitelist = ["ba.e-pics.ethz.ch","tools.wmflabs.org/reasonator","e-codices.unifr.ch",
                                    "deutsche-biographie.de", "deutsche-digitale-bibliothek.de",
                                    "kalliope.staatsbibliothek-berlin.de","tools.wmflabs.org/persondata/redirect/gnd/de/",
                                    "tools.wmflabs.org/persondata/redirect/gnd/en/","tools.wmflabs.org/persondata/redirect/gnd/commons/",
                                    "www.hdg.de/lemo/html/biografien/","beacon.findbuch.de/gnd-resolver/pw_tls/", "www.perlentaucher.de/autor/","archinform.net/gnd/",
                                    "www.gutenberg.org/ebooks/author/","beacon.findbuch.de/gnd-resolver/pw_mactut/","beacon.findbuch.de/gnd-resolver/pw_imdb/"
                                    ];
                                let whitelistedFindbuchLinks = [];
                                let alreadyFoundUrls = [];
                                this.person.findbuchLinks = [];
                                for(var j=0; j < data.uris.length; j++){
                                    let isWhitelisted = false;
                                    let url = data.uris[j];
                                    let path = url;

                                    if (url.indexOf("?")>-1)
                                        path = url.substring(0, url.indexOf("?"));
                                    // Für NDB und ADB nur einen Link
                                    if (url.indexOf("#ndbcontent")>-1)
                                            path = url.substring(0, url.indexOf("#ndbcontent"));
                                    else if (url.indexOf("#adbcontent")>-1)
                                            path = url.substring(0, url.indexOf("#adbcontent"));
                                    else if (url.indexOf("#allcontent")>-1)
                                            path = url.substring(0, url.indexOf("#allcontent"));

                                    if (url.indexOf("gnd-resolver/pw_tls/")>-1){
                                        path = url.substring(0, url.indexOf("pw_tls/") + 7);
                                    }
                                    for(var k=0;k<sourcesWhitelist.length;k++){
                                        if(url.indexOf(sourcesWhitelist[k])>-1)
                                                isWhitelisted = true;
                                    }
                                    if (!isWhitelisted || alreadyFoundUrls.indexOf(path)>-1)
                                            continue;
                                    let label = data.labels[j];
                                    this.person.findbuchLinks.push({'url': url, 'label': label});
                                    alreadyFoundUrls.push(path);
                                }
                            })
                    }
                })
                .catch((e) => {
                    throw(e)
                    console.error("***ETH*** an error occured: wdExpansionPerson.getPerson");
                    console.error(e.message);
                })
        }

        buildPrimoQueryValue(items){
            // in primo 7 rows and max 30 boolean operators
            /*
            The system will display a message and provide suggestions when the following limits are exceeded:
            The query contains more than 30 boolean operators.
            The query contains more than 8 question marks.
            The query contains more than 8 asterisks and the word length is greater than 2 (such as abb* or ab*c).
            The query contains more than 4 asterisks and the word length is less than 3 (such as ab*).
            The entire query consists of a single letter and an asterisk (such as a*).
            */
            let query = '';
            let maxItems = 20;
            let maxLength = 1000;
            items.forEach( (e, i) => {
                if (i < maxItems) {
                    if((query.length + e.length) < maxLength)
                    {
                        query += e;
                        if(i < items.length-1 && i < (maxItems-1)){
                            query += ' OR ';
                        }
                    }
                }
            });
            return query;
        }

        // enrich precision and recall navigation
        enrichPRNavigation(){
            let resultList = angular.element(this.$element[0].parentElement.parentElement);
            if(resultList){
                let html = `
                    <a name="wikidata-top-anchor"></a>
                    <div class="wikidata-person-top">
                        <a class="wikidata-anchor-link" ng-click="$ctrl.scrollTo('wikidata-person-searchprecision-anchor')">
                            {{$ctrl.label.precision[$ctrl.lang]}}?
                        </a>
                        <a class="wikidata-anchor-link" ng-click="$ctrl.scrollTo('wikidata-person-anchor')">{{$ctrl.label.moreInformations[$ctrl.lang]}} "${this.person.label}"</a>
                    </div>
                `;
                resultList.prepend(this.$compile(html)(this.$scope));

                // determine number of results of search variants
                // search for label
                this.daasService.searchPrimo('any,contains,' + this.person.label)
                    .then((data) => {
                        if(document.getElementsByClassName('wikidata-person-searchprecision__name').length > 0){
                            document.getElementsByClassName('wikidata-person-searchprecision__name')[0].innerHTML = data.info.total + ' ' + this.label.results[this.lang];
                        }
                    })
                // search for gnd
                if(this.person.gndid){
                    this.daasService.searchPrimo('lsr04,contains,' + this.person.gndid)
                        .then((data) => {
                            if(document.getElementsByClassName('wikidata-person-searchprecision__gnd1').length > 0){
                                document.getElementsByClassName('wikidata-person-searchprecision__gnd1')[0].innerHTML = data.info.total;
                            }
                            if(document.getElementsByClassName('wikidata-person-searchprecision__gnd2').length > 0){
                                document.getElementsByClassName('wikidata-person-searchprecision__gnd2')[0].innerHTML = data.info.total + ' ' + this.label.results[this.lang];
                            }
                        })
                }
                else{
                    if(document.getElementsByClassName('wikidata-person-searchprecision__gnd1').length > 0){
                        document.getElementsByClassName('wikidata-person-searchprecision__gnd1')[0].innerHTML = '0';
                    }
                    if(document.getElementsByClassName('wikidata-person-searchprecision__gnd2').length > 0){
                        document.getElementsByClassName('wikidata-person-searchprecision__gnd2')[0].innerHTML = '0 ' + this.label.results[this.lang];
                    }
                }
                // search for name variants / aliases
                this.daasService.searchPrimo('any,contains,' + this.person.variantQuery)
                    .then((data) => {
                        if(document.getElementsByClassName('wikidata-person-searchprecision__variants').length > 0){
                            document.getElementsByClassName('wikidata-person-searchprecision__variants')[0].innerHTML = data.info.total + ' ' + this.label.results[this.lang];
                        }
                    })
                // search for name AND (GND OR BIRTHYEAR)
                let primoServiceQuery = this.person.label;;
                if(this.person.yearOfBirth &&  this.person.gndid){
                    primoServiceQuery = primoServiceQuery + ' AND (' + this.person.yearOfBirth + ' OR ' + this.person.gndid + ')';
                }
                else if (this.person.yearOfBirth) {
                    primoServiceQuery = primoServiceQuery + ' ' + this.person.yearOfBirth;
                }
                else if (this.person.gndid) {
                    primoServiceQuery = primoServiceQuery + ' ' + this.person.gndid;
                }
                this.daasService.searchPrimo('any,contains,' + primoServiceQuery)
                    .then((data) => {
                        if(document.getElementsByClassName('wikidata-person-searchprecision__best').length > 0){
                            document.getElementsByClassName('wikidata-person-searchprecision__best')[0].innerHTML = data.info.total + ' ' + this.label.results[this.lang];
                        }
                    })
            }
        }

        scrollTo(anchorName){
            this.$location.hash(anchorName);
            this.$anchorScroll();
        }

        searchPersonByLabel(person){
            let query = person.label;
            this.parentCtrl.searchService.search({query: 'any,contains,' + query},true);
            this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
            let buttons = document.getElementsByClassName('wikidata-person-searchprecision');
            for(let i = 0; i < buttons.length; i++)
            {
               buttons[i].removeAttribute('active');
            }
            document.getElementsByClassName('wikidata-person-searchprecision__intro')[0].style.display = 'none';
            document.getElementsByClassName('wikidata-person-searchprecision__option-gnd')[0].style.display = 'block';
            document.getElementsByClassName('wikidata-person-searchprecision-label')[0].setAttribute('active','true');
            this.scrollTo('wikidata-top-anchor');
        }

        searchPersonByGndId(gndid, qid){
            let query = gndid;
            this.parentCtrl.searchService.search({query: 'lsr04,contains,' + query},true);
            this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
            let buttons = document.getElementsByClassName('wikidata-person-searchprecision');
            for(let i = 0; i < buttons.length; i++)
            {
               buttons[i].removeAttribute('active');
            }
            document.getElementsByClassName('wikidata-person-searchprecision__intro')[0].style.display = 'none';
            document.getElementsByClassName('wikidata-person-searchprecision__option-gnd')[0].style.display = 'block';
            document.getElementsByClassName('wikidata-person-searchprecision-gnd')[0].setAttribute('active','true');
            this.scrollTo('wikidata-top-anchor');
        }

        loadPersonPageByGndId(gndid, qid){
            let query = '[wd/person]' + qid;
            let url = `/primo-explore/search?query=any,contains,${query}&tab=default_tab&vid=WIKI&lang=${this.lang}`;
            location.href = url;
        }

        searchPersonByVariantQuery(person){
            let query = person.variantQuery;
            if(!person.variantQuery){
                query = person.label;
            }
            this.parentCtrl.searchService.search({query: 'any,contains,' + query},true);
            this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
            let buttons = document.getElementsByClassName('wikidata-person-searchprecision');
            for(let i = 0; i < buttons.length; i++)
            {
               buttons[i].removeAttribute('active');
            }
            document.getElementsByClassName('wikidata-person-searchprecision__intro')[0].style.display = 'none';
            document.getElementsByClassName('wikidata-person-searchprecision__option-gnd')[0].style.display = 'block';
            document.getElementsByClassName('wikidata-person-searchprecision-variants')[0].setAttribute('active','true');
            this.scrollTo('wikidata-top-anchor');
        }

        searchBestPossibleForPerson(person){
            let query = person.label;
            if(person.yearOfBirth &&  person.gndid){
                query = query + ' AND (' + person.yearOfBirth + ' OR ' + person.gndid + ')';
            }
            else if (person.yearOfBirth) {
                query = query + ' ' + person.yearOfBirth;
            }
            else if (person.gndid) {
                query = query + ' ' + person.gndid;
            }
            this.parentCtrl.searchService.search({query: 'any,contains,' + query},true);
            this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
            let buttons = document.getElementsByClassName('wikidata-person-searchprecision');
            for(let i = 0; i < buttons.length; i++)
            {
               buttons[i].removeAttribute('active');
            }
            document.getElementsByClassName('wikidata-person-searchprecision__intro')[0].style.display = 'none';
            document.getElementsByClassName('wikidata-person-searchprecision__option-gnd')[0].style.display = 'block';
            document.getElementsByClassName('wikidata-person-searchprecision-birthyear')[0].setAttribute('active','true');
            this.scrollTo('wikidata-top-anchor');
        }

        loadPlacePage(qid, label){
            let query = '[wd/place]' + qid;
            let url = `/primo-explore/search?query=any,contains,${query}&tab=default_tab&vid=WIKI&lang=${this.lang}`;
            location.href = url;
        }

        getLanguage(){
            let sms = this.$rootScope.$$childHead.$ctrl.userSessionManagerService;
            if (!sms) {
                console.error("***ETH*** WdExpansionPersonController: userSessionManagerService not available");
                return 'de';
            }
            else{
                return sms.getUserLanguage() || $window.appConfig['primo-view']['attributes-map'].interfaceLanguage;
            }
        }

    }
});
/*
<div ng-if="$ctrl.person.places && $ctrl.person.places.length>0" class="wikidata-person__section">
    <h3 class="md-subhead">Orte (aus Wikidata)</h3>
    <p class="wikidata-place-small" ng-repeat="object in $ctrl.person.places">
        <a target="_blank" ng-click="$ctrl.loadPlacePage(object.place.value.substring(object.place.value.lastIndexOf('/')+1),object.placeLabel.value);">
            <span class="wikidata-place-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
            <span class="wikidata-place-small__text">{{object.placeLabel.value}} ({{object.translatedPred}})</span>
        </a>
    </p>
</div>
*/
/*
this.wikidataService.getPlacesOfPerson(qid)
    .then((data) => {
        this.person.places = data.results.bindings;
        this.person.places.forEach( (e,i) => {
            e.translatedPred = '';
            if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P19'){
                e.translatedPred = 'Geburtsort'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P20'){
                e.translatedPred = 'Sterbeort'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P69'){
                e.translatedPred = 'besuchte Bildungseinrichtung'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P108'){
                e.translatedPred = 'Arbeitgeber'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P119'){
                e.translatedPred = 'Begräbnisort'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P551'){
                e.translatedPred = 'Wohnsitz'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P937'){
                e.translatedPred = 'Wirkungsort'
            }
            else if(e.predLabel.value === 'http://www.wikidata.org/prop/direct/P1321'){
                e.translatedPred = 'Heimatort'
            }
        })
    })
*/
