/**
* @memberof WIKI
* @ngdoc component
* @name wdExpansionPlaces
*
* @description
* WIKI Component:<br>
*
* - Query expansion via wikidata: places for searchterm
*
* <b>Requirements</b><br>
*
* Service {@link WIKI.wikidataService}<br>
*
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.prmSearchResultListAfter}<br>
*
*
* <b>In Package</b><br>
*
* WIKI <br>
 */
app.component('ethWdExpansionPlaces', {
    require: {
        parent: '^prmSearchResultListAfter'
    },
    template: `
    <div class="wikidata-places" ng-if="$ctrl.hasResults">
        <a name="wikidata-places-anchor"></a>
        <div class="wikidata-map-container">
            <div id="card"></div>
        </div>
    </div>
    `,

    controller: class WdExpansionPlacesController {
        constructor( wikidataService, poiService, $timeout, $location, $element, $scope, $compile, $anchorScroll, $rootScope ) {
            this.wikidataService = wikidataService;
            this.poiService = poiService;
            this.$timeout = $timeout;
            this.$location = $location;
            this.$element = $element;
            this.$scope = $scope;
            this.$compile = $compile;
            this.$anchorScroll = $anchorScroll;
            this.$rootScope = $rootScope;
            this.label = {
                places: {
                    de_DE: 'Orte zu meinem Suchbegriff',
                    en_US: 'Places matching my search term'
                },
                searchThis1: {
                    de_DE: 'Klicken Sie bitte auf den Marker, um nach"',
                    en_US: 'Please click on the marker to search for "'
                },
                searchThis2: {
                    de_DE: '" zu suchen!',
                    en_US: '"!'
                }
            }
        }

        $onInit() {
            this.parentCtrl = this.parent.parentCtrl;
            this.search = '';
            this.pois = [];
            this.hasResults = false;
            this.lang = this.getLanguage(this.$rootScope);

            // this.search: input in Primo searchField
            let query = this.parentCtrl.searchService.searchUtil.$stateParams.query;
            if(query.indexOf('lsr04') > -1){
                this.search = query.replace('lsr04,contains,','');
            }
            else{
                this.search = query.replace('any,contains,','');
            }
            // check querystring for searchfor
            let searchFor = this.$location.search().searchfor;
            if(!searchFor && this.search.indexOf('[wd/') === -1){
                this.getPlaces(this.search);
            }
            if(this.search === '[current]')
            {
                if ("geolocation" in navigator) {
                    let that = this;
                    navigator.geolocation.getCurrentPosition(function(position) {
                        that.getCurrentPoi(position.coords.latitude, position.coords.longitude);
                    });
                }
            }
        };

        getPlaces(search){
            this.wikidataService.getPlaces(search, this.lang)
                .then((data) => {
                    if(!data.results || !data.results.bindings || data.results.bindings.length === 0)
                            return null;
                    // filter for instanceOf to reduce number of places
                    let wikiResults = data.results.bindings.filter(e => {
                        if (e.instanceOfLabels.value.indexOf('canton of') > -1) {
                            return false;
                        }
                        else if (e.instanceOfLabels.value.indexOf('greater region') > -1) {
                            return false;
                        }
                        else if (e.instanceOfLabels.value == 'Kanton') {
                            return false;
                        }
                        else if (e.instanceOfLabels.value == 'Wikimedia-Dublette') {
                            return false;
                        }
                        else if (e.instanceOfLabels.value.indexOf('Grossregionen') > -1) {
                            return false;
                        }
                        else if (e.instanceOfLabels.value.indexOf('Bezirk des Kantons') > -1) {
                            return false;
                        }
                        return true;
                    })
                    if(wikiResults.length === 0){
                        return;
                    }
                    this.hasResults = true;
                    this.$timeout( () => {
                        this.renderPlacesMap(wikiResults);
                    }, 1000);
                })
        }

        renderPlacesMap(wikiResults){
            let map = new google.maps.Map(document.getElementById("card"),{
                zoom: 5,
                center: {lat: 47, lng: 8}
            });
            let latlngList = [];
            let controller = this;
            let lastLng = 0;
            let lastLat = 0;
            // if point is located at the moon: <http://www.wikidata.org/entity/Q405> Point(-176.2 49.6)
            wikiResults = wikiResults.filter((wikiResult, i) => {
                if(!wikiResult.coordinate_location)return false;
                if(wikiResult.coordinate_location.value.indexOf('www.wikidata.org') > 0){
                    return false;
                }
                return true;
            })
            let markers = wikiResults.map((wikiResult, i) => {
                let point = wikiResult.coordinate_location.value;
                let lat = parseFloat(point.substring(point.indexOf(' ') + 1, point.length-1));
                let lng = parseFloat(point.substring(6,point.indexOf(' ')));
                if(lng == lastLng && lat == lastLat){
                    lat = lat + 0.00001;
                }
                let position = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                latlngList.push(position);
                lastLng = lng;
                lastLat = lat;
                let marker = new google.maps.Marker({
                    position: position
                });
                google.maps.event.addListener(marker, 'click', function() {
                  controller.loadPlacePage(wikiResult.item.value.substring(wikiResult.item.value.lastIndexOf('/')+1), wikiResult.itemLabel.value);
                });
                let content = '<div class="marker-info">';
                if(wikiResult.image){
                    content += '<img src="' + wikiResult.image.value + '">';
                }
                content += '<div class="marker-info-text"><p class="marker-info-title">' + wikiResult.itemLabel.value + '</p>';
                if(wikiResult.instanceOfLabels){
                    content += '(' + wikiResult.instanceOfLabels.value + ')';
                }
                content += '<p class="marker-info-hint">' + this.label.searchThis1[this.lang] + wikiResult.itemLabel.value + this.label.searchThis2[this.lang] + '</p></div></div>';
                let infoWindow = new google.maps.InfoWindow({
                    content:  content
                });
                google.maps.event.addListener(marker, 'mouseover', function() {
                  infoWindow.open(map,this);
                });
                google.maps.event.addListener(marker, 'mouseout', function() {
                  infoWindow.close(map,this);
                });
                return marker;
            });
            // Add a marker clusterer to manage the markers.
            let markerCluster = new MarkerClusterer(map, markers,{
                imagePath: 'custom/WIKI/img/m',
                maxZoom: 15
            });

            // Add bounds -> zoom appropriate
            let bounds = new google.maps.LatLngBounds();
            latlngList.forEach( n => {
                bounds.extend(n);
            });
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);

            // top link to map container
            let resultList = angular.element(this.$element[0].parentElement.parentElement);
            if(resultList){
                let html = `
                    <div class="wikidata-search-suggestion">
                        <a class="wikidata-anchor-link" ng-click="$ctrl.$location.hash('wikidata-places-anchor');$ctrl.$anchorScroll();">
                            {{$ctrl.label.places[$ctrl.lang]}}
                        </a>
                    </div>
                `;
                resultList.prepend(this.$compile(html)(this.$scope));
            }
        }

        loadPlacePage(qid, label){
            let url = `/primo-explore/search?query=any,contains,${label}&tab=default_tab&vid=WIKI&lang=${this.lang}&searchfor=place/${qid}`;
            location.href = url;
        }

        getCurrentPoi(lat, lng){
            this.poiService.nearTo(lat, lng, 50)
                .then((pois) => {
                    if(!pois || pois.length === 0)return null;
                    this.wikidataService.enrichPOIsWithQID(pois)
                        .then((pois) => {
                            if(!pois[0].qid)return null;
                            console.error(pois);
                            let url = `/primo-explore/search?query=any,contains,${pois[0].title}&tab=default_tab&vid=WIKI&lang=${this.lang}&searchfor=place/${pois[0].qid}`;
                            location.href = url;
                        })

                })
                .catch( (e) => {
                    console.error("***ETH*** an error occured: MainAfterController getPoi ");
                    console.error(e.message);
                    throw(e)
                })
        }

        getLanguage(){
            let sms = this.$rootScope.$$childHead.$ctrl.userSessionManagerService;
            if (!sms) {
                console.error("***ETH*** WdExpansionPersonController: userSessionManagerService not available");
                return 'de';
            }
            else{
                return sms.getUserLanguage() || $window.appConfig['primo-view']['attributes-map'].interfaceLanguage;
            }
        }
    }
});
