/**
* @memberof WIKI
* @ngdoc component
* @name wdExpansionPlace
*
* @description
* WIKI Component:<br>
*
* - place page
* - switch between all results and selected results from ETHorama
* - map and links
*
* <b>Requirements</b><br>
*
* Service {@link WIKI.wikidataService}<br>
* Service {@link WIKI.poiService}<br>
* Service {@link WIKI.daasService}<br>
*
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.prmSearchResultListAfter}<br>
*
*
* <b>In Package</b><br>
*
* WIKI <br>
 */
/*
<div class="wikidata-map-container">
    <div id="card"></div>
</div>
*/
app.component('ethWdExpansionPlace', {
    require: {
        parent: '^prmSearchResultListAfter'
    },
    template: `
    <div class="wikidata-place" ng-if="$ctrl.hasResults">
        <a name="wikidata-place-anchor"></a>
        <div class="wikidata-map-container">
            <div id="mapid"></div>
        </div>
        <md-card md-theme="default" md-theme-watch>
            <md-card-title>
                <md-card-title-text>
                    <h2 class="md-headline">{{$ctrl.itemLabel}}</h2>
                </md-card-title-text>
                <md-card-title-media ng-if="$ctrl.pois[0].image">
                    <img class="md-card-image" ng-src="{{$ctrl.pois[0].image.value}}?width=200px" />
                </md-card-title-media>
            </md-card-title>
            <md-card-content>
                <h3 class="md-subhead">{{$ctrl.label.wdLinks[$ctrl.lang]}}</h3>
                <p ng-if="$ctrl.wikidataURL"><a href="{{$ctrl.wikidataURL}}" target="_blank">Wikidata</a></p>
                <p ng-if="$ctrl.wikipediaURL"><a href="{{$ctrl.wikipediaURL}}" target="_blank">Wikipedia</a></p>
                <p ng-if="$ctrl.geonamesURL"><a href="{{$ctrl.geonamesURL}}" target="_blank">Geonames</a></p>
                <p ng-if="$ctrl.gndURL"><a href="{{$ctrl.gndURL}}" target="_blank">GND (DNB)</a></p>
                <p ng-if="$ctrl.hlsURL"><a href="{{$ctrl.hlsURL}}" target="_blank">Historisches Lexikon der Schweiz</a></p>
                <p ng-if="$ctrl.archinformURL"><a href="{{$ctrl.archinformURL}}" target="_blank">archINFORM</a></p>

                <h3 class="md-subhead" ng-if="$ctrl.geolinks.length > 0">{{$ctrl.label.geolinkerLinks[$ctrl.lang]}}</h3>
                <p ng-repeat="object in $ctrl.geolinks">
                    <a href="{{object}}" target="_blank">{{object}}</a>
                </p>
            <md-card-content>
        </md-card>
    </div>
    `,

    controller: class WdExpansionPlaceController {
        constructor( wikidataService, poiService, daasService, $timeout, $location, $element, $scope, $compile, $rootScope) {
            this.wikidataService = wikidataService;
            this.poiService = poiService;
            this.daasService = daasService;
            this.$timeout = $timeout;
            this.$location = $location;
            this.$element = $element;
            this.$scope = $scope;
            this.$compile = $compile;
            this.$rootScope = $rootScope;
            this.label = {
                allResults: {
                    de_DE: 'Alle Suchergebnisse zu',
                    en_US: 'All search results for'
                },
                insteadAll1: {
                    de_DE: 'Stattdessen alle Suchergebnisse zu',
                    en_US: 'See all search results for'
                },
                insteadAll2: {
                    de_DE: 'sehen',
                    en_US: 'instead'
                },
                dossier: {
                    de_DE: 'Unser Dossier zu',
                    en_US: 'Our dossier on'
                },
                insteadDossier1: {
                    de_DE: 'Stattdessen unser Dossier zum Ort',
                    en_US: 'See our dossier on place'
                },
                insteadDossier2: {
                    de_DE: 'sehen',
                    en_US: 'instead'
                },
                wdLinks: {
                    de_DE: 'Externe Links aus Wikidata',
                    en_US: 'External links from Wikidata'
                },
                geolinkerLinks: {
                    de_DE: 'Externe Links der Geolinker API',
                    en_US: 'External links from Geolinker API'
                }
            }
        }

        $onInit() {
            this.parentCtrl = this.parent.parentCtrl;
            this.search = '';
            this.pois = [];
            this.hasResults = false;
            this.lang = this.getLanguage(this.$rootScope);

            // this.search: input in Primo searchField
            let query = this.parentCtrl.searchService.searchUtil.$stateParams.query;
            if(query.indexOf('lsr04') > -1){
                this.search = query.replace('lsr04,contains,','');
            }
            else{
                this.search = query.replace('any,contains,','');
            }
            // check querystring for searchfor
            let searchFor = this.$location.search().searchfor;

            // call by searchpage
            if (searchFor && searchFor.indexOf('place/') > -1) {
                this.getPlace(searchFor.substring(searchFor.indexOf('/')+1));
            }
            // permalink
            else if (this.search.indexOf('[wd/place]') > -1) {
                this.getPlace(this.search.substring(this.search.indexOf(']')+1));
            }


        };

        getPlace(qid){
            this.poiService.getPlaceByQNumber(qid)
                .then((data) => {
                    if(!data)return qid;
                    let firstKey = Object.keys(data)[0];
                    this.pois = [data[firstKey]];
                    if(this.pois.length === 0)return qid;
                    this.pois[0].qid = qid;
                    return qid;
                })
                .then((qid) => {
                    return this.wikidataService.getPlace(qid, this.lang);
                })
                .then((data) => {
                    if(data.results.bindings && data.results.bindings.length > 0){
                        this.hasResults = true;
                            this.itemLabel = data.results.bindings[0].itemLabel.value;
                        this.parentCtrl.searchService.searchFieldsService.mainSearch = this.itemLabel;
                        this.$timeout(() => {
                            this.searchAllPlaceRessources(this.itemLabel);
                        }, 200);

                        this.$timeout(() => {
                            this.renderLeafletMap(data.results.bindings[0]);
                        }, 500);

                        // render header for result list
                        let resultList = angular.element(this.$element[0].parentElement.parentElement);
                        if(resultList){
                            let html = `
                            <div class="wikidata-place-top" ng-if="$ctrl.pois.length == 0">
                                <h1>Ergebnisse zu "${this.itemLabel}"</h1>
                            </div>
                            <div class="wikidata-place-top" ng-if="$ctrl.pois.length > 0">
                                <h1><span class="wikidata-place-top__text">${this.label.allResults[this.lang]}</span> "${this.itemLabel}" </h1>
                                <md-button style="display:none;" class="md-primary wikidata-place-top__button-all" ng-click="$ctrl.searchAllPlaceRessources('${this.itemLabel}');">${this.label.insteadAll1[this.lang]} "${this.itemLabel}" ${this.label.insteadAll2[this.lang]}</md-button>
                                <md-button class="md-primary wikidata-place-top__button-dossier" ng-click="$ctrl.searchSelectedPlaceRessources($ctrl.pois[0]);">${this.label.insteadDossier1[this.lang]} "${this.itemLabel}" ${this.label.insteadDossier2[this.lang]}</md-button>
                            </div>`;
                            resultList.prepend(this.$compile(html)(this.$scope));
                        }

                        let place = data.results.bindings[0];
                        if(place.item && place.item.value){
                            this.wikidataURL = place.item.value;
                        }
                        if(place.geonames){
                            this.geonamesURL = 'https://www.geonames.org/' + place.geonames.value;
                        }
                        if(place.wikipedia){
                            this.wikipediaURL = place.wikipedia.value;
                        }
                        if(place.gnd){
                            this.gndURL = 'http://d-nb.info/gnd/' + place.gnd.value;
                        }
                        if(place.hls){
                            this.hlsURL = 'http://www.hls-dhs-dss.ch/textes/d/D' + place.hls.value + '.php';
                        }
                        if(place.archinform){
                            this.archinformURL = 'https://www.archinform.net/ort/' + place.archinform.value +'.htm';
                        }
                        this.daasService.searchGeolinkerByQID(qid)
                            .then((data) => {
                                try{
                                    this.geolinks = data.data.resolverneo4j.links;
                                }
                                catch(e){
                                    this.geolinks = null;
                                }
                            })
                    }
                })
                .catch( (e) => {
                    console.error("***ETH*** an error occured: getPlace wikidataService ");
                    console.error(e.message);
                    throw(e)
                })
        }

        renderPlaceMap(wikiResult){
            let point = wikiResult.coordinate_location.value;
            let lng = point.substring(6,point.indexOf(' '));
            let lat = point.substring(point.indexOf(' ') + 1, point.length-1);
            let position = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));

            let map = new google.maps.Map(document.getElementById("card"),{
                zoom: 13,
                center: position
            });
            let marker = new google.maps.Marker({
                position: position,
                map: map
            });
        }

        renderLeafletMap(wikiResult){
            let point = wikiResult.coordinate_location.value;
            let lng = point.substring(6,point.indexOf(' '));
            let lat = point.substring(point.indexOf(' ') + 1, point.length-1);
            if(!document.getElementById('mapid'))return;
            let map = L.map('mapid').setView([lat, lng], 13);
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            	maxZoom: 18,
            	id: 'mapbox.streets',
            	accessToken: 'pk.eyJ1IjoiYmVybmR1IiwiYSI6ImNrMjBpMHRidjE1ZGkzaHFnNDdlMXV4eWkifQ.RACc6dWL675djVZaMmHBww'
            }).addTo(map);
            let marker = L.marker([lat, lng]).addTo(map);

        }
        searchSelectedPlaceRessources(poi){
            let items = [];
            poi.contentitems.forEach(e => {
                items.push(e.sourceid);
            });
            // in primo max 30 boolsche operatoren
            let query = 'any,contains,';
            items.forEach( (e, i) => {
                if (i < 31) {
                    query += e;
                    if (i < (items.length - 1) && i < 30) {
                        query += ' OR ';
                    }
                }
            });
            this.parentCtrl.searchService.search({query:query},true);
            if(document.getElementsByClassName('wikidata-place-top__text')[0]){
                document.getElementsByClassName('wikidata-place-top__text')[0].innerHTML = this.label.dossier[this.lang];
                document.getElementsByClassName('wikidata-place-top__button-all')[0].style.display = 'block';
                document.getElementsByClassName('wikidata-place-top__button-dossier')[0].style.display = 'none';
            }
        }

        searchAllPlaceRessources(search){
            // input field
            this.parentCtrl.searchService.searchFieldsService.mainSearch = search;
            // do search
            this.parentCtrl.searchService.search({query:'any,contains,' + search},true);
            if(document.getElementsByClassName('wikidata-place-top__text')[0]){
                document.getElementsByClassName('wikidata-place-top__text')[0].innerHTML = this.label.allResults[this.lang];
                document.getElementsByClassName('wikidata-place-top__button-all')[0].style.display = 'none';
                document.getElementsByClassName('wikidata-place-top__button-dossier')[0].style.display = 'block';
            }
        }

        getLanguage(){
            let sms = this.$rootScope.$$childHead.$ctrl.userSessionManagerService;
            if (!sms) {
                console.error("***ETH*** WdExpansionPersonController: userSessionManagerService not available");
                return 'de';
            }
            else{
                return sms.getUserLanguage() || $window.appConfig['primo-view']['attributes-map'].interfaceLanguage;
            }
        }

    }
});

/*
Persons:
<h3 class="md-subhead" ng-if="$ctrl.birthplaceAtPlace.length > 0">Hier geborene historische Personen (vor 1940 geboren, aus Wikidata)</h3>
<p class="wikidata-person-small" ng-repeat="object in $ctrl.birthplaceAtPlace">
    <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
        <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
        <span class="wikidata-person-small__text">{{object.itemLabel.value}} ({{object.itemDescription.value}}, Geburtstag:{{object.births.value.substring(0,object.births.value.indexOf('T'))}})</span>
    </a>
</p>

<h3 class="md-subhead" ng-if="$ctrl.educatedAtPlace.length > 0">Hier ausgebildete historische Personen (vor 1940 geboren, aus Wikidata)</h3>
<p class="wikidata-person-small" target="_blank" ng-repeat="object in $ctrl.educatedAtPlace">
    <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
        <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
        <span class="wikidata-person-small__text">{{object.itemLabel.value}} ({{object.itemDescription.value}}, Geburtstag:{{object.births.value.substring(0,object.births.value.indexOf('T'))}})</span>
    </a>
</p>

<h3 class="md-subhead" ng-if="$ctrl.personsAtPlace.length > 0">Hier lebten, arbeiteten oder starben historische Personen(vor 1940 geboren, aus Wikidata)</h3>
<p class="wikidata-person-small" ng-repeat="object in $ctrl.personsAtPlace">
    <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
        <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
        <span class="wikidata-person-small__text">{{object.itemLabel.value}} (<span ng-if="object.itemDescription">{{object.itemDescription.value}},</span> Geburtstag:{{object.births.value.substring(0,object.births.value.indexOf('T'))}})</span>
    </a>
</p>

<h3 class="md-subhead" ng-if="$ctrl.employeesAtPlace.length > 0">Arbeitsstätte von historischen Personen (vor 1940 geboren, aus Wikidata)</h3>
<p class="wikidata-person-small" ng-repeat="object in $ctrl.employeesAtPlace">
    <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
        <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
        <span class="wikidata-person-small__text">{{object.itemLabel.value}} ({{object.itemDescription.value}}, Geburtstag:{{object.births.value.substring(0,object.births.value.indexOf('T'))}})</span>
    </a>
</p>

<h3 class="md-subhead" ng-if="$ctrl.burriedInPlace.length > 0">Hier begrabene historische Personen (vor 1940 geboren, aus Wikidata)</h3>
<p class="wikidata-person-small" ng-repeat="object in $ctrl.burriedInPlace">
    <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
        <span class="wikidata-person-small__imagecontainer"><img ng-if="object.image" src="{{object.image.value}}?width=100px" /></span>
        <span class="wikidata-person-small__text">{{object.itemLabel.value}} ({{object.itemDescription.value}}, Geburtstag:{{object.births.value.substring(0,object.births.value.indexOf('T'))}})</span>
    </a>
</p>
*/
/*
this.wikidataService.getPersonsBornInPlace(qid)
    .then((data) => {
        try{
            this.birthplaceAtPlace = data.results.bindings;
        }
        catch(e){
            this.birthplaceAtPlace = null;
        }
    })
this.wikidataService.getPersonsByPlace(qid)
    .then((data) => {
        try{
            this.personsAtPlace = data.results.bindings;
        }
        catch(e){
            this.personsAtPlace = null;
        }
    })
this.wikidataService.getPersonsEmployedByPlace(qid)
    .then((data) => {
        try{
            this.employeesAtPlace = data.results.bindings;
        }
        catch(e){
            this.employeesAtPlace = null;
        }

    })
this.wikidataService.getPersonsEducatedAtPlace(qid)
    .then((data) => {
        try{
            this.educatedAtPlace = data.results.bindings;
        }
        catch(e){
            this.educatedAtPlace = null;
        }
    })
this.wikidataService.getPersonsBurriedInPlace(qid)
    .then((data) => {
        try{
            this.burriedInPlace = data.results.bindings;
        }
        catch(e){
            this.burriedInPlace = null;
        }
    })

*/
/*
loadPersonPageByGndId(gndid, qid){
    let query = gndid;
    if(!gndid){
        query = qid;
    }
    let url = `/primo-explore/search?query=lsr04,contains,${query}&tab=default_tab&vid=WIKI&lang=de_DE&searchfor=person/${qid}`;
    location.href = url;
}
*/
