/**
* @memberof WIKI
* @ngdoc component
* @name wikidataQueryExpansionon
*
* @description
* WIKI Component:<br>
*
* - Query expansion via wikidata
*
* <b>Requirements</b><br>
*
* Service {@link WIKI.wikidataService}<br>
* Service {@link WIKI.poiService}<br>
*
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.prmSearchResultListAfter}<br>
*
*
* <b>In Package</b><br>
*
* WIKI <br>
 */
app.component('wikidataQueryExpansion', {
    require: {
        parent: '^prmSearchResultListAfter'
    },
    template: `
    <div ng-if="$ctrl.terms.length > 0"  style="background-color:white;margin-top:2em;padding:2em;">
        <h2>Welche Bedeutung suchen Sie</h2>
        <ul>
            <li ng-repeat="term in $ctrl.terms" style="margin-top: 2em;">
                <a style="display:flex;align-items:center;" ng-click="$ctrl.loadSearchResult(term.itemLabel.value + ' ' + term.instanceOfLabel.value);">
                    "{{term.itemLabel.value}}" als Instanz von "{{term.instanceOfLabel.value}}":  {{term.itemDescription.value}}
                </a>
            </li>
        </ul>
    </div>

    <div class="wikidata-persons" ng-if="$ctrl.persons.length > 0">
        <a name="wikidata-persons-anchor"></a>
        <md-card ng-repeat="person in $ctrl.persons" md-theme="default" md-theme-watch>
            <md-card-title>
                <md-card-title-text>
                    <span class="md-headline">{{person.label}}</span>
                    <span class="md-subhead">{{person.description}}</span>
                    <span class="md-subhead">{{person.professions}}</span>
                    <span class="md-subhead">GND ID: {{person.gndid}}</span>
                    <span class="md-subhead">Geburtstag: {{person.dateOfBirth}}</span>
                </md-card-title-text>
                <md-card-title-media ng-if="person.image">
                    <img class="md-card-image" ng-src="{{person.image}}?width=200px" />
                </md-card-title-media>
            </md-card-title>
            <md-card-actions layout-xs="column" layout="row" layout-align="start center" layout-align-xs="start start">
                <md-button ng-if="person.gndid" ng-click="$ctrl.loadPersonPageByGndId(person.gndid, person.qid);">Suche nach GND ID</md-button>
                <md-button ng-click="$ctrl.loadPersonPageByVariantQuery(person.variantQuery, person.qid);">Suche nach Namensvarianten</md-button>
                <md-button ng-click="$ctrl.loadPersonPageByLabel(person.label, person.qid);">Suche nach Name</md-button>
            </md-card-actions>
        </md-card>
    </div>

    <div class="wikidata-person" ng-if="$ctrl.person && $ctrl.person.item">
        <!-- person page -->
        <a name="wikidata-person-anchor"></a>
        <md-card md-theme="default" md-theme-watch>
            <md-card-title>
                <md-card-title-text>
                    <span class="md-headline">{{$ctrl.person.itemLabel.value}}</span>
                    <span class="md-subhead">{{$ctrl.person.itemDescription.value}}</span>
                    <span class="md-subhead">{{$ctrl.person.professions.value}}</span>
                    <span class="md-subhead">GND ID: {{$ctrl.person.gnd.value}}</span>
                    <span class="md-subhead">ORCID: <a href="https://orcid.org/{{$ctrl.person.orcid.value}}" target="_blank">{{$ctrl.person.orcid.value}}</a></span>
                    <span class="md-subhead">Geburtstag: {{$ctrl.person.dateOfBirth.value}}</span>
                </md-card-title-text>
                <md-card-title-media ng-if="$ctrl.person.image.value">
                    <img class="md-card-image" ng-src="{{$ctrl.person.image.value}}?width=200px" />
                </md-card-title-media>
            </md-card-title>
            <md-card-actions layout-xs="column" layout="row" layout-align="start center" layout-align-xs="start start">
                <md-button  ng-if="$ctrl.person.gndIdQuery" ng-click="$ctrl.loadPersonPageByGndId($ctrl.person.gndIdQuery, $ctrl.person.qid);">Suche nach GND ID</md-button>
                <md-button ng-click="$ctrl.loadPersonPageByVariantQuery($ctrl.person.variantQuery, $ctrl.person.qid);">Suche nach Namensvarianten</md-button>
                <md-button ng-click="$ctrl.loadPersonPageByLabel($ctrl.person.itemLabel.value, $ctrl.person.qid);">Suche nach Name</md-button>
            </md-card-actions>
            <md-card-content >
                <span class="md-headline">Externe Links</span>
                <p><a target="_blank" href="{{$ctrl.person.item.value}}">Wikidata</a></p>
                <p ng-if="$ctrl.person.hls"><a target="_blank" href="http://www.hls-dhs-dss.ch/textes/f/F{{$ctrl.person.hls.value}}.php">HLS</a></p>
                <p ng-if="$ctrl.person.viaf"><a target="_blank" href="https://viaf.org/viaf/{{$ctrl.person.viaf.value}}/">VIAF</a></p>
                <p ng-if="$ctrl.person.gnd"><a target="_blank" href="https://d-nb.info/gnd/{{$ctrl.person.gnd.value}}">GND</a></p>
                <p ng-if="$ctrl.person.orcid"><a target="_blank" href="https://orcid.org/{{$ctrl.person.orcid.value}}">ORCID</a></p>
                <div ng-if="$ctrl.person.archived  && $ctrl.person.archived.length>0">
                    <span class="md-headline">Archive</span>
                    <p ng-repeat="object in $ctrl.person.archived">
                        <span ng-if="!object.ref.value"">
                            <span>{{object.archivedLabel.value}}</span>
                            <span ng-if="object.inventoryno"> (Inventarnummer: {{object.inventoryno.value}})</span>
                        </span>
                        <a target="_blank" ng-if="object.ref.value" ng-href="{{object.ref.value}}">
                            <span>{{object.archivedLabel.value}}</span>
                            <span ng-if="object.inventoryno"> (Inventarnummer: {{object.inventoryno.value}})</span>
                        </a>
                    </p>
                </div>
                <div ng-if="$ctrl.person.students  && $ctrl.person.students.length>0">
                    <span class="md-headline">Bedeutende Schüler, Doktorand</span>
                    <p class="wikidata-places-of-person" ng-repeat="object in $ctrl.person.students">
                        <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
                            <img ng-if="object.image" src="{{object.image.value}}?width=200px" />{{object.itemLabel.value}} - {{object.gnd.value}}
                        </a>
                    </p>
                </div>
                <div ng-if="$ctrl.person.teachers  && $ctrl.person.teachers.length>0">
                    <span class="md-headline">Lehrer, Promotionsbetreuer</span>
                    <p class="wikidata-places-of-person" ng-repeat="object in $ctrl.person.teachers">
                        <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
                            <img ng-if="object.image" src="{{object.image.value}}?width=200px" />{{object.itemLabel.value}} - {{object.gnd.value}}
                        </a>
                    </p>
                </div>
                <div ng-if="$ctrl.person.places && $ctrl.person.places.length>0">
                    <span class="md-headline">Orte</span>
                    <p class="wikidata-places-of-person" ng-repeat="object in $ctrl.person.places">
                        <a target="_blank" ng-click="$ctrl.loadPlacePage(object.place.value.substring(object.place.value.lastIndexOf('/')+1),object.placeLabel.value);">
                            <img ng-if="object.image" src="{{object.image.value}}?width=200px" />{{object.placeLabel.value}} - {{object.pred.value}}
                        </a>
                    </p>
                </div>
            </md-card-content>
        </md-card>
    </div>

    <div class="wikidata-pois" ng-if="$ctrl.pois.length > 0">
        <a name="wikidata-pois-anchor"></a>
        <h1 ng-if="!$ctrl.wikidataURL">Orte mit ausgewählten Dokumenten</h1>
        <div class="wikidata-map-container">
            <div id="card"></div>
        </div>
        <div class="wikidata-link-container" ng-if="$ctrl.wikidataURL">
            <h1>{{$ctrl.pois[0].title}}</h1>
            <p>Die Dokumente sind manuell ausgewählte Dokumente zum Ort "{{$ctrl.pois[0].title}}".</p>
            <p> Wollen sie alle Ergebnisse zum Suchbegriff "{{$ctrl.pois[0].title}}" sehen?
            <md-button ng-click="$ctrl.loadSearchResult($ctrl.pois[0].title);">Suche nach "{{$ctrl.pois[0].title}}"</md-button>
            </p>
            <h2>Externe Links</h2>
            <p ng-if="$ctrl.wikidataURL"><a href="{{$ctrl.wikidataURL}}" target="_blank">Wikidata</a></p>
            <p ng-if="$ctrl.wikipediaURL"><a href="{{$ctrl.wikipediaURL}}" target="_blank">Wikipedia</a></p>
            <p ng-if="$ctrl.geonamesURL"><a href="{{$ctrl.geonamesURL}}" target="_blank">Geonames</a></p>
            <p ng-if="$ctrl.gndURL"><a href="{{$ctrl.gndURL}}" target="_blank">GND</a></p>
            <p ng-if="$ctrl.hlsURL"><a href="{{$ctrl.hlsURL}}" target="_blank">HLS</a></p>
            <p ng-if="$ctrl.archinformURL"><a href="{{$ctrl.archinformURL}}" target="_blank">archINFORM</a></p>
        </div>
    </div>
    <div class="wikidata-persons-at-place" ng-if="$ctrl.personsAtPlace.length > 0">
        <h2>Wohnsitz, Arbeitsort, Sterbeort, Heimatort</h2>
        <p ng-repeat="object in $ctrl.personsAtPlace">
            <a target="_blank" ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
                <img ng-if="object.image" src="{{object.image.value}}?width=200px" />{{object.itemLabel.value}} (GND ID: {{object.gnd.value}})
            </a>
        </p>
    </div>
    <div class="wikidata-persons-at-place" ng-if="$ctrl.employeesAtPlace.length > 0">
        <h2>Arbeitgeber von</h2>
        <p target="_blank" ng-repeat="object in $ctrl.employeesAtPlace">
            <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
                <img ng-if="object.image" src="{{object.image.value}}?width=200px" />{{object.itemLabel.value}} - {{object.gnd.value}}
            </a>
        </p>
    </div>
    <div class="wikidata-persons-at-place" ng-if="$ctrl.educatedAtPlace.length > 0">
        <h2>Educated at</h2>
        <p target="_blank" ng-repeat="object in $ctrl.educatedAtPlace">
            <a ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
                <img ng-if="object.image" src="{{object.image.value}}?width=200px" />{{object.itemLabel.value}} - {{object.gnd.value}}
            </a>
        </p>
    </div>
    <div class="wikidata-persons-at-place" ng-if="$ctrl.burriedInPlace.length > 0">
        <h2>Begräbnisort</h2>
        <p ng-repeat="object in $ctrl.burriedInPlace">
            <a target="_blank" ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
                <img ng-if="object.image" src="{{object.image.value}}?width=200px" />{{object.itemLabel.value}} - {{object.gnd.value}}
            </a>
        </p>
    </div>
    <div class="wikidata-persons-at-place" ng-if="$ctrl.birthplaceAtPlace.length > 0">
        <h2>Geburtsort</h2>
        <p ng-repeat="object in $ctrl.birthplaceAtPlace">
            <a target="_blank" ng-click="$ctrl.loadPersonPageByGndId(object.gnd.value,object.item.value.substring(object.item.value.lastIndexOf('/')+1))">
                <img ng-if="object.image" ng-src="{{object.image.value}}?width=200px" />{{object.itemLabel.value}} - {{object.gnd.value}}
            </a>
        </p>
    </div>
    `,

    controller: class WikidataQueryExpansionController {
        constructor( wikidataService, poiService, $timeout, $location, $element, $scope, $compile) {
            this.wikidataService = wikidataService;
            this.poiService = poiService;
            this.$timeout = $timeout;
            this.$location = $location;
            this.$element = $element;
            this.$scope = $scope;
            this.$compile = $compile;
        }

        $onInit() {
            this.parentCtrl = this.parent.parentCtrl;
            this.search = '';
            this.pois = [];
            this.persons = [];
            this.person = {};

            // this.search: input in searchField
            let query = this.parentCtrl.searchService.searchUtil.$stateParams.query;
            if(query.indexOf('lsr04') > -1){
                this.search = query.replace('lsr04,contains,','');
            }
            else{
                this.search = query.replace('any,contains,','');
            }
            // check querystring for searchfor
            let searchFor = this.$location.search().searchfor;
            if (searchFor && searchFor.indexOf('/') > -1) {
                if (searchFor.indexOf('person/') > -1) {
                    this.getPerson(searchFor.substring(searchFor.indexOf('/')+1));
                }
                else if (searchFor.indexOf('place/') > -1) {
                    this.getPlace(searchFor.substring(searchFor.indexOf('/')+1));
                }
            }
            else if (searchFor && searchFor.indexOf('term') > -1) {
                this.getTerms(this.search);
            }
            else{
                this.getPlaces(this.search);
                this.getPersons(this.search);
            }
            return;
        };

        getPersons(search){
            this.wikidataService.getPersons(search)
                .then((data) => {
                    if(!data.results || !data.results.bindings || data.results.bindings.length === 0)
                            return null;
                    let lastItemValue = '';
                    data.results.bindings.forEach(e => {
                        // todo falls mehrere Bilder, dann mehrere Zeilen
                        if(e.item.value != lastItemValue){
                            let aVariants = e.aliasList.value.split('|');
                            let variantQuery = '';
                            if(aVariants.indexOf(e.itemLabel.value) > -1){
                                variantQuery = this.buildPrimoQueryValue(aVariants);
                            }
                            else{
                                variantQuery = this.buildPrimoQueryValue([e.itemLabel.value].concat(aVariants));
                            }
            this.persons.push({'qid':e.item.value.substring(e.item.value.lastIndexOf('/')+1),'label':e.itemLabel.value, 'description':e.itemDescription ? e.itemDescription.value : null, 'alias':e.aliasList ? e.aliasList.value : null, 'image':e.image ? e.image.value : null, 'dateOfBirth':e.dateOfBirth ? e.dateOfBirth.value : null, 'professions':e.professions ? e.professions.value:null, 'gndid':e.gnd ? e.gnd.value : null, 'viafid':e.viaf ? e.viaf.value : null, 'variantQuery': variantQuery});
                            lastItemValue = e.item.value;
                        }
                    });
                    // Link nach unten rendern
                    let resultList = angular.element(this.$element[0].parentElement.parentElement);
                    if(resultList){
                        resultList.prepend('<div class="wikidata-container-link"><a href="#wikidata-persons-anchor">Zeige Personen zu meinem Suchbegriff</a></div>');
                    }
                })
                .catch( (e) => {
                    console.error("***ETH*** an error occured: getPersons");
                    console.error(e.message);
                })
        }

        getPerson(qid){
            this.wikidataService.getPersonByQID(qid)
                .then((data) => {
                    if(data.results && data.results.bindings && data.results.bindings.length > 0){
                        this.person = data.results.bindings[0];
                        let aVariants = data.results.bindings[0].aliasList.value.split('|');
                        if(aVariants.indexOf(this.person.itemLabel.value) > -1){
                            this.person.variantQuery = this.buildPrimoQueryValue(aVariants);
                        }
                        else{
                            this.person.variantQuery = this.buildPrimoQueryValue([this.person.itemLabel.value].concat(aVariants));
                        }
                        if(this.person.gnd){
                            this.person.gndIdQuery = this.person.gnd.value;
                        }
                        this.person.qid = qid;
                        // falls der Suchbegriff=QID, dann wird im Primo falls vorhanden nach GND ID gesucht, sonst nach Namensvarianten
                        if(this.parentCtrl.searchService.searchFieldsService.mainSearch === qid){
                            if(this.person.gnd){
                                this.parentCtrl.searchService.search({query: 'any,contains,' + this.person.gndIdQuery},true);
                                this.parentCtrl.searchService.searchFieldsService.mainSearch = this.person.gnd.value;
                            }
                            else{
                                this.parentCtrl.searchService.search({query: 'any,contains,' + this.person.variantQuery},true);
                                this.parentCtrl.searchService.searchFieldsService.mainSearch = this.person.variantQuery;
                            }
                        }
                        // Link nach unten rendern
                        let resultList = angular.element(this.$element[0].parentElement.parentElement);
                        if(resultList){
                            resultList.prepend('<div class="wikidata-container-link"><a href="#wikidata-person-anchor">Zeige weitere Informationen und Links zur Person</a></div>');
                        }
                    }
                    return;
                })
                .then(() => {
                    let qid = this.person.item.value.substring(this.person.item.value.lastIndexOf('/')+1);
                    if(this.person.archived && this.person.archived.value && this.person.archived.value != ''){
                        this.wikidataService.getArchivedOfPerson(qid)
                            .then((data) => {
                                this.person.archived = data.results.bindings;
                            })
                    }
                    this.wikidataService.getStudentsOfPerson(qid)
                        .then((data) => {
                            this.person.students = data.results.bindings;
                        })
                        this.wikidataService.getTeachersOfPerson(qid)
                            .then((data) => {
                                this.person.teachers = data.results.bindings;
                            })
                    this.wikidataService.getLocationsOfPerson(qid)
                        .then((data) => {
                            this.person.places = data.results.bindings;
                        })

                })
                .catch((e) => {
                    throw(e)
                    console.error("***ETH*** an error occured: getPerson");
                    console.error(e.message);
                })
        }

        getTerms(search){
            this.wikidataService.getTerms(search)
                .then((data) => {
                    if(!data.results || !data.results.bindings || data.results.bindings.length === 0)
                            return;
                    this.terms = data.results.bindings;
                })
                .catch( (e) => {
                    console.error("***ETH*** an error occured: getTerms ");
                    console.error(e.message);
                })
        }

        getPlaces(search){
            this.poiService.search(search)
                .then((data) => {
                    if(!data.pois)
                        return null;
                    let pois = data.pois.filter( e => {
                        if(e.type == 'poi')return true;
                        else return false;
                    });
                    if(pois.length === 0)return;
                    return this.wikidataService.enrichPOIsWithQID(pois)

                })
                .then((data) => {
                    if(!data){
                        return null;
                    }
                    this.pois = data.filter( e => {
                        if(e.qid && e.qid != '')return true;
                        else return false;
                    });
                    this.$timeout( () => {
                        this.renderPlaceContainer();
                    }, 1000);
                })
                .catch( (e) => {
                    throw(e)
                    console.error("***ETH*** an error occured: getPlaces ");
                    console.error(e.message);
                })
        }

        getPlacesFromWiki(search){
            // wikidata: get coordinates
            this.wikidataService.getLocations(search)
                .then((data) => {
                    if(!data.results || !data.results.bindings || data.results.bindings.length === 0)
                            return null;
                    // todo - welche Bedingungen zur Auswahl aus mehreren Resultaten?
                    let wikiResults = data.results.bindings.filter(e => {
                        if (e.instanceOfLabels.value.indexOf('canton of') > -1) {
                            return false;
                        }
                        else if (e.instanceOfLabels.value.indexOf('greater region') > -1) {
                            return false;
                        }
                        else if (e.instanceOfLabels.value == 'Kanton' > -1) {
                            return false;
                        }
                        else if (e.instanceOfLabels.value.indexOf('Grossregionen') > -1) {
                            return false;
                        }
                        return true;
                    })
                    if(wikiResults.length === 0){
                        return;
                    }
                    this.pois = [];
                    //let wikiResult = wikiResults[wikiResults.length-1];
                    wikiResults.forEach(wikiResult => {
                        // todo - welche distance abh. von was?
                        let distance = 500;
                        if(wikiResult.instanceOfLabels && wikiResult.instanceOfLabels.value === 'municipality of Switzerland'){
                            distance = 3000;
                        }
                        let point = wikiResult.coordinate_location.value;
                        let lng = point.substring(6,point.indexOf(' '));
                        let lat = point.substring(point.indexOf(' ') + 1, point.length-1);
                        this.poiService.nearTo(lat, lng, distance)
                            .then((data) => {
                                if(!data || data.length === 0)return;
                                return this.wikidataService.enrichPOIsWithQID(data)
                            })
                            .then((data) => {
                                if(!data)return;
                                this.pois = this.pois.concat(data);
                                if(this.pois.length === 0)return;
                                this.$timeout( () => {
                                    this.renderPlaceContainer();
                                }, 1000);
                            })
                            .catch( (e) => {
                                throw(e)
                                console.error("***ETH*** an error occured: getPlaces ");
                                console.error(e.message);
                            })
                    });
                })
        }

        getPlace(qid){
            this.poiService.getPlaceByQNumber(qid)
                .then((data) => {
                    if(!data)return qid;
                    let firstKey = Object.keys(data)[0];
                    this.pois = [data[firstKey]];
                    if(this.pois.length === 0)return;
                    this.pois[0].qid = qid;
                    if(this.pois[0].contentitems){
                        this.processPOI(this.pois[0]);
                    }
                    this.$timeout(() => {
                        this.renderPlaceContainer();
                    }, 1000);
                    return qid;
                })
                .then((qid) => {
                    if(!qid)return null;

                    this.wikidataService.getLocation(qid)
                        .then((data) => {
                            if(data.results.bindings && data.results.bindings.length>0){
                                let place = data.results.bindings[0];
                                if(place.item && place.item.value){
                                    this.wikidataURL = place.item.value;
                                }
                                if(place.item && place.item.value){
                                    this.geolinkerURL = 'https://api.geolinker.histhub.ch/v1/sameas/' + place.item.value;
                                }
                                if(place.geonames){
                                    this.geonamesURL = 'https://www.geonames.org/' + place.geonames.value;
                                }
                                if(place.wikipedia){
                                    this.wikipediaURL = place.wikipedia.value;
                                }
                                if(place.gnd){
                                    this.gndURL = 'http://d-nb.info/gnd/' + place.gnd.value;
                                }
                                if(place.hls){
                                    this.hlsURL = 'http://www.hls-dhs-dss.ch/textes/f/F' + place.hls.value + '.php';
                                }
                                if(place.archinform){
                                    this.archinformURL = 'https://www.archinform.net/ort/' + place.archinform.value +'.htm';
                                }
                            }
                        })

                    this.wikidataService.getPersonsByPlace(qid)
                        .then((data) => {
                            this.personsAtPlace = data.results.bindings;
                        })
                    this.wikidataService.getPersonsEmployedByPlace(qid)
                        .then((data) => {
                            this.employeesAtPlace = data.results.bindings;
                        })
                    this.wikidataService.getPersonsEducatedAtPlace(qid)
                        .then((data) => {
                            this.educatedAtPlace = data.results.bindings;
                        })
                    this.wikidataService.getPersonsBornInPlace(qid)
                        .then((data) => {
                            this.birthplaceAtPlace = data.results.bindings;
                        })
                    this.wikidataService.getPersonsBurriedInPlace(qid)
                        .then((data) => {
                            this.burriedInPlace = data.results.bindings;
                        })
                })
                .catch( (e) => {
                    console.error("***ETH*** an error occured: getPlace ");
                    console.error(e.message);
                })
        }

        renderPlaceContainer(){
            let pois = this.pois;
            let zoom = 8;
            let isPlacePage = false;

            // Ein POI mit contentitems = Orts-Seite
            if(pois[0].contentitems){
                isPlacePage = true;
                zoom = 16;
            }

            let map = new google.maps.Map(document.getElementById("card"),{
                center: new google.maps.LatLng(pois[0].lat, pois[0].lng),
                zoom: zoom,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            for (let i = 0; i < pois.length; i++) {
                let poi = pois[i];

                let marker = new google.maps.Marker({
                    position: new google.maps.LatLng(parseFloat(poi.lat), parseFloat(poi.lng)),
                    map: map
                });
                if(!isPlacePage){
                    let infoWindow = new google.maps.InfoWindow({
                        content:  '<div class="marker-info"><img src="' + poi.thumbnail + '"><div class="marker-info-text"><p class="marker-info-title">' +  poi.title + '</p><p  class="marker-info-hint">Klicken Sie bitte auf den Marker, um ausgewählte Dokumente anzuzeigen!</p></div></div>'
                    });
                    google.maps.event.addListener(marker, 'mouseover', function() {
                      infoWindow.open(map,this);
                    });
                    google.maps.event.addListener(marker, 'mouseout', function() {
                      infoWindow.close(map,this);
                    });
                    let controller = this;

                    google.maps.event.addListener(marker, 'click', function() {
                      controller.loadPOIPage(poi.qid);
                    });
                }
            }
            // Überschrift bzw. Link nach unten rendern
            let resultList = angular.element(this.$element[0].parentElement.parentElement);
            if(isPlacePage){
                if(resultList){
                    resultList.prepend(this.$compile('<div class="wikidata-poi-heading">Ausgewählte Dokumente zu "' + pois[0].title + '"</div>')(this.$scope));
                }
            }
            else{
                if(resultList){
                    resultList.prepend('<div class="wikidata-container-link"><a href="#wikidata-pois-anchor">Zeige Orte zu meinem Suchbegriff, zu denen es ausgewählte Dokumente gibt</a></div>');
                }
            }
        }

        loadPOIPage(id){
            let url = `/primo-explore/search?query=any,contains,${this.search}&tab=default_tab&vid=WIKI&lang=de_DE&searchfor=place/${id}`;
            location.href = url;
        }

        processPOI(poi){
            let items = [];
            poi.contentitems.forEach(e => {
                items.push(e.sourceid);
            });
            // in primo 7 zeilen und max 30 boolsche operatoren
            let query = '';
            let orCounter = 0;
            items.forEach( (e, i) => {
                if (i < 30) {
                    if (orCounter === 7) {
                        orCounter = 0;
                    }
                    if (orCounter === 0) {
                       query += 'any,contains,';
                    }
                    query += e;
                    if (orCounter === 6 || i === (items.length - 1) || i === 29) {
                     query += ',OR';
                    }
                    else{
                     query += ' OR ';
                    }
                    orCounter += 1;
                }
            });
            this.parentCtrl.searchService.search({query:query},true);
        }

        buildPrimoQueryValue(items){
            // in primo 7 zeilen und max 30 boolsche operatoren
            //let query = 'any,contains,';
            /*
            The system will display a message and provide suggestions when the following limits are exceeded:
            The query contains more than 30 boolean operators.
            The query contains more than 8 question marks.
            The query contains more than 8 asterisks and the word length is greater than 2 (such as abb* or ab*c).
            The query contains more than 4 asterisks and the word length is less than 3 (such as ab*).
            The entire query consists of a single letter and an asterisk (such as a*).
            */

            let query = '';
            items.forEach( (e, i) => {
                if (i < 30) {
                    query += e;
                    if(i < items.length-1 && i < 29){
                        query += ' OR ';
                    }
                }
            });
            return query;
        }

        loadPersonPageByGndId(gndid,qid){
            let search = gndid;
            if(!gndid){
                search = qid;
            }
            let url = `/primo-explore/search?query=any,contains,${search}&tab=default_tab&vid=WIKI&lang=de_DE&searchfor=person/${qid}`;
            location.href = url;
        }
        loadPersonPageByVariantQuery(query,qid){
            let url = `/primo-explore/search?tab=default_tab&search_scope=default_scope&vid=WIKI&lang=de_DE&offset=0&searchfor=person/${qid}&query=any,contains,${query}`;
            location.href = url;
        }
        loadPersonPageByLabel(query,qid){
            let url = `/primo-explore/search?tab=default_tab&search_scope=default_scope&vid=WIKI&lang=de_DE&offset=0&searchfor=person/${qid}&query=any,contains,${query}`;
            location.href = url;
        }
        loadPlacePage(qid, label){
            let url = `/primo-explore/search?query=any,contains,${label}&tab=default_tab&vid=WIKI&lang=de_DE&searchfor=place/${qid}`;
            location.href = url;
        }
        loadSearchResult(search){
            let url = `/primo-explore/search?query=any,contains,${search}&tab=default_tab&vid=WIKI&lang=de_DE`;
            location.href = url;
        }

    }
});
