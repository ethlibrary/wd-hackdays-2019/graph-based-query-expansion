/**
* @memberof WIKI
* @ngdoc directive
* @name prmSearchResultListAfter
*
* @description
* WIKI After Component<br>
*
* Customization for the search result list:<br>
*
* - Query expansion via wikidata
*
*
* <b>Requirements</b><br>
*
* WIKI Component {@link WIKI.ethWdExpansionPersons}<br>
* WIKI Component {@link WIKI.ethWdExpansionPlaces}<br>
* WIKI Component {@link WIKI.ethWdExpansionPerson}<br>
* WIKI Component {@link WIKI.ethWdExpansionPlace}<br>
*
*
* <b>In Package</b><br>
*
* WIKI<br>
 */
app.component('prmSearchResultListAfter', {
    bindings: { parentCtrl: '<' },
    template: `<eth-wd-expansion-persons ng-if="$ctrl.tab===\'default_tab\'"></eth-wd-expansion-persons>
               <eth-wd-expansion-person ng-if="$ctrl.tab===\'default_tab\'"></eth-wd-expansion-person>
               <eth-wd-expansion-places ng-if="$ctrl.tab===\'default_tab\'"></eth-wd-expansion-places>
               <eth-wd-expansion-place ng-if="$ctrl.tab===\'default_tab\'"></eth-wd-expansion-place>
               `,
    controller: class prmSearchResultListAfterController {

        constructor(searchService){
        	this.searchService = searchService;
        	this.tab = '';
        }

        $onInit() {
           if (!this.parentCtrl.primolyticsService || !this.parentCtrl.primolyticsService.searchService) {
                this.tab = "";
                console.error("***ETH*** prmSearchResultListAfter.$onInit: primolyticsService or searchService not available");
            }
        	else{
        		this.tab = this.searchService.getTab(this.parentCtrl.primolyticsService.searchService);
        	}
        }
    }
});
