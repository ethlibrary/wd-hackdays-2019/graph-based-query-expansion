/**
* @memberof WIKI
* @ngdoc service
* @name poiService
*
* @description
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.wdExpansionPlace}<br>
*
*
* <b>In Package</b><br>
*
* WIKI<br>
 */
app.factory('poiService', ['$http', '$sce', function ( $http, $sce ) {
    let baseurl = 'https://daas.library.ethz.ch/rib/v2/pois';

    function nearTo(lat, lng, distance){
        let url = baseurl + "?nearto=" + lat + "," + lng + "," + distance;
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: poiService.nearTo error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    function getPoi(poi){
        let url = baseurl + "/" + poi;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    console.error(httpError)
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: poiService.getPoi error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    return null;
                }
            );
    }

    function getPlaceByQNumber(qid){
        let url = baseurl + '?qid=' + qid;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404 || httpError.statusText === 'Bad Request')return null;
                    let error = "***ETH*** an error occured: poiService getPlaceByQNumber error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function search(q){
        let url = baseurl + '?q=' + q;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    console.error(httpError)
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: poiService.search error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    return null;
                }
            );
    }

    return {
        nearTo: nearTo,
        getPoi: getPoi,
        getPlaceByQNumber: getPlaceByQNumber,
        search: search
    };
}]);
