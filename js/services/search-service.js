/**
* @memberof eth
* @ngdoc service
* @name searchService
*
* @description
* Service to get context informations of the current search (tab, scope etc).
*
* <b>Used by</b><br>
*
* Eth After Component {@link eth.prmBriefResultAfter} (DADS)<br>
* Eth After Component {@link eth.prmSearchResultAvailabilityLineAfter}<br>
* Eth After Component {@link eth.prmSearchResultListAfter} (DADS)<br>
*
* Eth Component {@link eth.ethAltmetric}<br>
* Eth Component {@link eth.ethOadoi}<br>
*
*
* <b>In Package</b><br>
*
* WIKI<br>
 */
app.factory('searchService', function() {
  return {
    getTab: function(searchService) {
        return searchService.searchFieldsService._tab;
    },
    getScope: function(searchService) {
        return searchService.searchFieldsService._scope;
    },
    getMainSearch: function(searchService) {
        return searchService.searchFieldsService._mainSearch;
    }
  };
});
