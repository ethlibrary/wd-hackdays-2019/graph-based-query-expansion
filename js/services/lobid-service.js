/**
* @memberof eth
* @ngdoc service
* @name lobidService
*
* @description
* not used
*
* <b>Used by</b><br>
*
*
* <b>In Package</b><br>
*
* WIKI<br>
*
 */
// http://blog.lobid.org/2018/07/06/lobid-gnd-queries.html
app.factory('lobidService', ['$http', '$sce', function ( $http, $sce ) {

    function getPersonsByID(id){
        const endpointUrl = 'https://lobid.org/gnd/';

        let fullUrl = endpointUrl + id + '.json';
        $sce.trustAsResourceUrl(fullUrl);

        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if(httpError.status === 404){
                        return null;
                    }
                    else{
                        let error = "***ETH*** an error occured: lobidService getPersonsByPreferredName error callback: " + httpError.status;
                        if (httpError.data && httpError.data.errorMessage) {
                            error += ' - ' + httpError.data.errorMessage;
                        }
                        console.error(error);
                        return null;
                    }
                }
            )
    }

    function getPersonsByPreferredName(name){
        const endpointUrl = 'https://lobid.org/gnd/search';

        let fullUrl = endpointUrl + '?size=15&filter=type:Person&format=json:suggest&q=preferredName:' + name;
        //let fullUrl = endpointUrl + '?size=100&filter=type:Person&format=json:suggest&q=sameAs.collection.id:"http://www.wikidata.org/entity/Q2013" AND preferredName:' + name;
        $sce.trustAsResourceUrl(fullUrl);

        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: lobidService getPersonsByPreferredName error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function getPersonsByVariantName(name){
        const endpointUrl = 'https://lobid.org/gnd/search';

        let fullUrl = endpointUrl + '?size=15&filter=type:Person&format=json:suggest&q=variantName:' + name;
        //let fullUrl = endpointUrl + '?size=100&filter=type:Person&format=json:suggest&q=sameAs.collection.id:"http://www.wikidata.org/entity/Q2013" AND preferredName:' + name;
        $sce.trustAsResourceUrl(fullUrl);

        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: lobidService getPersonsByPreferredName error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function getCorporateBodies(name){
        const endpointUrl = 'https://lobid.org/gnd/search';
        let fullUrl = endpointUrl + '?size=10&filter=type:CorporateBody&format=json:preferredName&q="' + name + '"';
        $sce.trustAsResourceUrl(fullUrl);

        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: lobidService getCorporateBodies error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    return {
        getPersonsByID: getPersonsByID,
        getPersonsByVariantName: getPersonsByVariantName,
        getPersonsByPreferredName: getPersonsByPreferredName,
        getCorporateBodies: getCorporateBodies
    };
}]);
