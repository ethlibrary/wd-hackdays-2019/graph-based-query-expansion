/**
* @memberof WIKI
* @ngdoc service
* @name daasService
*
* @description
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.wdExpansionPerson}<br>
* WIKI Component {@link WIKI.wdExpansionPlace}<br>
*
*
* <b>In Package</b><br>
*
* WIKI<br>
 */
app.factory('daasService', ['$http', '$sce', function ( $http, $sce ) {

    function searchPrimo(q){
        let baseurl = 'https://daas.library.ethz.ch/rib/v2/search';
        let url = baseurl + '?lang=de_DE&limit=1&q=' + encodeURIComponent(q);
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    console.error(httpError)
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: daasService.searchPrimo error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    return null;
                }
            );
    }

    function searchMetagrid(gnd){
        let baseurl = 'https://daas.library.ethz.ch/rib/v2/enrichments/metagrid/';
        let url = baseurl + gnd;
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    console.error(httpError)
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: daasService.searchMetagrid error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    return null;
                }
            );
    }

    function searchEntityfacts(gnd){
        let baseurl = 'https://daas.library.ethz.ch/rib/v2/enrichments/entityfacts/';
        let url = baseurl + gnd;
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    console.error(httpError)
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: daasService.searchEntityfacts error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    return null;
                }
            );
    }

    function searchFindbuch(gnd){
        let baseurl = 'https://daas.library.ethz.ch/rib/v2/enrichments/findbuch/gnd-person/';
        let url = baseurl + gnd;
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    console.error(httpError)
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: daasService.searchFindbuch error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    return null;
                }
            );
    }

    function searchGeolinkerByQID(qid){
        let baseurl = 'https://daas.library.ethz.ch/rib/v2/enrichments/geolinker/qid/';
        let url = baseurl + qid;
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: daasService.searchGeolinkerByQID error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    return null;
                }
            );
    }

    return {
        searchPrimo: searchPrimo,
        searchMetagrid: searchMetagrid,
        searchEntityfacts: searchEntityfacts,
        searchFindbuch: searchFindbuch,
        searchGeolinkerByQID: searchGeolinkerByQID
    };
}]);
