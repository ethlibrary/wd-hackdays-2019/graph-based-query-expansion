/**
* @memberof WIKI
* @ngdoc service
* @name wikidataService
*
* @description
* Services for getting informations about persons and places from wikidata
*
* <b>Used by</b><br>
*
* WIKI Component {@link WIKI.wdExpansionPersons}<br>
* WIKI Component {@link WIKI.wdExpansionPerson}<br>
* WIKI Component {@link WIKI.wdExpansionPlaces}<br>
* WIKI Component {@link WIKI.wdExpansionPlace}<br>
*
*
* <b>In Package</b><br>
*
* WIKI<br>
*
 */
app.factory('wikidataService', ['$http', '$sce', function ( $http, $sce ) {

    // only CH: ?item wdt:P17 wd:Q39.
    function getPlaces(search, lang){
        let paramLang = getParamLang(lang);
        const endpointUrl = 'https://query.wikidata.org/sparql';
        let sparqlQuery = `
        SELECT DISTINCT ?item ?itemLabel ?coordinate_location ?image
        (group_concat(DISTINCT(?instanceOfLabel) ;separator = ", ") as ?instanceOfLabels )
        WHERE {
            SERVICE wikibase:mwapi {
                bd:serviceParam wikibase:api "EntitySearch" .
                bd:serviceParam wikibase:endpoint "www.wikidata.org" .
                bd:serviceParam mwapi:search "${search}" .
                bd:serviceParam mwapi:language "de" .
                ?item wikibase:apiOutputItem mwapi:item .
                ?num wikibase:apiOrdinal true .
            }

            ?item wdt:P31 ?instanceOf.
            ?item wdt:P625 ?coordinate_location.

            OPTIONAL {?item wdt:P18 ?image.}
            SERVICE wikibase:label { bd:serviceParam wikibase:language "${paramLang}".
                                    ?instanceOf rdfs:label ?instanceOfLabel .
                                     ?item rdfs:label ?itemLabel .}
        }
        GROUP BY ?item ?itemLabel ?coordinate_location ?image
        ORDER BY ?coordinate_location
        LIMIT 100
        `;
        let fullUrl = endpointUrl + '?query=' + sparqlQuery;
        $sce.trustAsResourceUrl(fullUrl);

        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: wikidataService getPlaces error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function getPlace(qid, lang){
        let paramLang = getParamLang(lang);
        const endpointUrl = 'https://query.wikidata.org/sparql';
        let sparqlQuery = `
        SELECT DISTINCT  ?item ?itemLabel ?wikipedia ?gnd ?geonames ?archinform ?hls ?coordinate_location ?image
        WHERE {
          BIND(wd:${qid} AS ?item).
          ?item wdt:P625 ?coordinate_location.
          OPTIONAL {?item wdt:P18 ?image.}
          OPTIONAL {?item wdt:P227 ?gnd}
          OPTIONAL {?item wdt:P1566 ?geonames}
          OPTIONAL {?item wdt:P5573 ?archinform}
          OPTIONAL {?item wdt:P902 ?hls}
          OPTIONAL {
            ?wikipedia schema:about ?item .
            ?wikipedia schema:inLanguage "de" .
            FILTER (SUBSTR(str(?wikipedia), 1, 25) = "https://de.wikipedia.org/")
          }
          SERVICE wikibase:label { bd:serviceParam wikibase:language "${paramLang}".
                                   ?item rdfs:label ?itemLabel .
                                  }
        }
        `;
        let fullUrl = endpointUrl + '?query=' + encodeURIComponent(sparqlQuery);
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: wikidataService getPlace error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    // default order of mwapi "Search" is relevance (srsort)
    // https://www.mediawiki.org/wiki/API:Search
    function getPersons(search, lang){
        let paramLang = getParamLang(lang);
        const endpointUrl = 'https://query.wikidata.org/sparql';
        let sparqlQuery = `
        SELECT DISTINCT ?item ?itemLabel ?itemDescription ?birth ?death ?image ?gnd ?viaf
            (group_concat(DISTINCT(?professionLabel) ;separator = "| ") as ?professions )
            (group_concat(DISTINCT(?aliasLabel) ;separator = "|") as ?aliasList )
        WHERE {
          SERVICE wikibase:mwapi {
              bd:serviceParam wikibase:api "Search" .
              bd:serviceParam wikibase:endpoint "www.wikidata.org" .
              bd:serviceParam mwapi:srsearch "${search}" .
              bd:serviceParam wikibase:limit 100 .
              ?item wikibase:apiOutputItem mwapi:title .
              ?ordinal wikibase:apiOrdinal true .
         }
           ?item wdt:P227 ?gnd.
           ?item wdt:P214 ?viaf.
           ?item wdt:P31 wd:Q5.
           OPTIONAL {?item wdt:P569 ?birth.}
           OPTIONAL {?item wdt:P570 ?death.}
           OPTIONAL {?item wdt:P106 ?profession.}
           OPTIONAL {?item wdt:P18 ?image.}
           OPTIONAL {?item (rdfs:label|skos:altLabel|wdt:P1449) ?alias.}
            SERVICE wikibase:label { bd:serviceParam wikibase:language "${paramLang}".
                                  ?profession rdfs:label ?professionLabel .
                                   ?alias rdfs:label ?aliasLabel .
                                   ?item schema:description ?itemDescription .
                                   ?item rdfs:label ?itemLabel .}
        }
        GROUP BY ?item ?itemLabel ?itemDescription ?birth ?death ?image ?gnd  ?viaf
        ORDER BY ASC(?ordinal)
        LIMIT 100
        `;
        let fullUrl = endpointUrl + '?query=' + sparqlQuery;
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: wikidataService getPersons error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function getResearcherProfile(qid, lang){
        let paramLang = getParamLang(lang);
        const endpointUrl = 'https://query.wikidata.org/sparql';
        let sparqlQuery = `
        SELECT ?item ?itemLabel ?orcid ?researcherid ?publonid ?scholar ?scopus
        WHERE {
          BIND(wd:${qid} AS ?item).
          ?item wdt:P31 wd:Q5.
          OPTIONAL {?item wdt:P496 ?orcid.}
          OPTIONAL {?item wdt:P1053 ?researcherid.}
          OPTIONAL {?item wdt:P3829 ?publonid.}
          OPTIONAL {?item wdt:P1960 ?scholar.}
          OPTIONAL {?item wdt:P1153 ?scopus.}
          SERVICE wikibase:label { bd:serviceParam wikibase:language "${paramLang}". }
        }
        `;
        let fullUrl = endpointUrl + '?query=' + encodeURIComponent(sparqlQuery);
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: wikidataService getResearcherProfile error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function _getPerson(statement, lang){
        let paramLang = getParamLang(lang);
        const endpointUrl = 'https://query.wikidata.org/sparql';
        let sparqlQuery = `
        SELECT ?item ?itemLabel ?itemDescription ?birth ?death ?birthplaceLabel ?deathplaceLabel ?image ?gnd ?dodis ?sfa ?bdel ?hls ?loc ?viaf ?archived ?wc ?cerl
            (group_concat(DISTINCT(?aliasLabel) ;separator = "|") as ?aliasList )
            (group_concat(DISTINCT(?professionLabel) ;separator = ", ") as ?professions )
        WHERE {
          ${statement}.
          ?item wdt:P31 wd:Q5.
          OPTIONAL {?item wdt:P569 ?birth.}
          OPTIONAL {?item wdt:P19 ?birthplace.}
          OPTIONAL {?item wdt:P570 ?death.}
          OPTIONAL {?item wdt:P20 ?deathplace.}
          OPTIONAL {?item wdt:P106 ?profession.}
          OPTIONAL {?item wdt:P18 ?image.}
          OPTIONAL {?item wdt:P227 ?gnd.}
          OPTIONAL {?item wdt:P701 ?dodis.}
          OPTIONAL {?item wdt:P3889 ?sfa.}
          OPTIONAL {?item wdt:P6231 ?bdel.}
          OPTIONAL {?item wdt:P902 ?hls.}
          OPTIONAL {?item wdt:P244 ?loc.}
          OPTIONAL {?item wdt:P214 ?viaf.}
          OPTIONAL {?item wdt:P485 ?archived.}
          OPTIONAL {?item wdt:P373 ?wc.}
          OPTIONAL {?item wdt:P1871 ?cerl.}
          OPTIONAL {?item (rdfs:label|skos:altLabel|wdt:P1449) ?alias.}
          SERVICE wikibase:label { bd:serviceParam wikibase:language "${paramLang}".
                                ?profession rdfs:label ?professionLabel .
                                 ?alias rdfs:label ?aliasLabel .
                                 ?birthplace rdfs:label ?birthplaceLabel .
                                 ?deathplace rdfs:label ?deathplaceLabel .
                                 ?item schema:description ?itemDescription .
                                 ?item rdfs:label ?itemLabel .}
        }GROUP BY ?item ?itemLabel ?itemDescription ?birth ?death ?birthplaceLabel ?deathplaceLabel ?image ?gnd ?dodis ?sfa ?bdel ?hls ?loc ?viaf ?archived ?wc ?cerl
        `;
        let fullUrl = endpointUrl + '?query=' + encodeURIComponent(sparqlQuery);
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: wikidataService _getPerson error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function getPersonByGND(id, lang){
        let statement = '?item wdt:P227 "' + id + '"';
        return _getPerson(statement, lang);
    }

    function getPersonByQID(qid, lang){
        let statement = 'FILTER ( ?item = <http://www.wikidata.org/entity/' + qid + '> )';
        return _getPerson(statement, lang);
    }

    function getPersonGndByQID(qid, lang){
        let paramLang = getParamLang(lang);
        const endpointUrl = 'https://query.wikidata.org/sparql';
        let sparqlQuery = `
        SELECT ?item ?itemLabel ?gnd
        WHERE {
          BIND(wd:${qid} AS ?item).
          OPTIONAL {?item wdt:P227 ?gnd}.
          SERVICE wikibase:label { bd:serviceParam wikibase:language "${paramLang}". }
        }
        `;
        let fullUrl = endpointUrl + '?query=' + encodeURIComponent(sparqlQuery);
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: wikidataService getPersonGndByQID error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    // archived at: reference
    // 116042621 Peter Debye
    function getArchivedOfPerson(qid, lang){
        let paramLang = getParamLang(lang);
        const endpointUrl = 'https://query.wikidata.org/sparql';
        let sparqlQuery = `
        SELECT ?statement ?archived ?archivedLabel ?refnode  ?ref ?inventoryno
        where{
            wd:${qid} p:P485 ?statement.
            ?statement ps:P485 ?archived.
            OPTIONAL{?statement pq:P217 ?inventoryno.}
            OPTIONAL{ ?statement prov:wasDerivedFrom ?refnode.
              ?refnode pr:P854 ?ref.
            }
            SERVICE wikibase:label { bd:serviceParam wikibase:language "${paramLang}". }
        }        `;
        let fullUrl = endpointUrl + '?query=' + sparqlQuery;
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: wikidataService getArchivedOfPerson error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    // bedeutende Schüler(P802) oder Doktorand(P185)
    function getStudentsOfPerson(qid, lang){
        let paramLang = getParamLang(lang);
        const endpointUrl = 'https://query.wikidata.org/sparql';
        let sparqlQuery = `
        SELECT DISTINCT ?item ?itemLabel ?itemDescription ?gnd ?image
        (group_concat(DISTINCT(?birth) ;separator = " or ") as ?births )
        (group_concat(DISTINCT(?death) ;separator = " or ") as ?deaths )
        WHERE {
          wd:${qid} (wdt:P802|wdt:P185) ?item.
          OPTIONAL {?item wdt:P18 ?image.}
          OPTIONAL {?item wdt:P227 ?gnd.}
          OPTIONAL {?item wdt:P569 ?birth.}
          OPTIONAL {?item wdt:P570 ?death.}
          OPTIONAL {?item wdt:P106 ?profession.}
          SERVICE wikibase:label { bd:serviceParam wikibase:language "${paramLang}". }
        }
        GROUP BY ?item ?itemLabel ?itemDescription ?gnd ?image
        ORDER BY ASC(?births)
        `;
        let fullUrl = endpointUrl + '?query=' + sparqlQuery;
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: wikidataService getStudentsOfPerson error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    // Lehrer(P1066) oder Promotionsbetreuer(P184)
    function getTeachersOfPerson(qid, lang){
        let paramLang = getParamLang(lang);
        const endpointUrl = 'https://query.wikidata.org/sparql';
        let sparqlQuery = `
        SELECT DISTINCT ?item ?itemLabel ?itemDescription ?gnd ?image
        (group_concat(DISTINCT(?birth) ;separator = " or ") as ?births )
        (group_concat(DISTINCT(?death) ;separator = " or ") as ?deaths )
        WHERE {
          wd:${qid} (wdt:P1066|wdt:P184) ?item.
          OPTIONAL {?item wdt:P18 ?image.}
          OPTIONAL {?item wdt:P227 ?gnd.}
          OPTIONAL {?item wdt:P569 ?birth.}
          OPTIONAL {?item wdt:P570 ?death.}
          OPTIONAL {?item wdt:P106 ?profession.}
          SERVICE wikibase:label { bd:serviceParam wikibase:language "${paramLang}". }
        }
        GROUP BY ?item ?itemLabel ?itemDescription ?gnd ?image
        ORDER BY ASC(?births)
        `;
        let fullUrl = endpointUrl + '?query=' + sparqlQuery;
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: wikidataService getTeachersOfPerson error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    // http://fortinet.also.ch/wiki/api.php?action=help&modules=query&submodules=1
    // https://en.wikipedia.org/w/api.php?action=query&prop=pageprops&titles=Kunsthaus_Z%C3%BCrich&format=json

    function enrichPOIsWithQID(pois){
        const endpointUrl = 'https://de.wikipedia.org/w/api.php?action=query&prop=pageprops&format=json&origin=*';
        let titles = ''
        let counter = 1;
        pois.forEach(poi => {
            // todo max 50 titles
            if(counter <= 50){
                let link = '';
                if(poi.link.indexOf('wikipedia')>-1){
                    link = decodeURIComponent(poi.link);
                }
                else if(poi.link2.indexOf('wikipedia')>-1){
                    link = decodeURIComponent(poi.link2);
                }
                if(link !== ''){
                    poi.wikiTitle = link.substring(link.lastIndexOf('/')+1);
                    if(counter > 1){
                        titles += '|';
                    }
                    titles += link.substring(link.lastIndexOf('/')+1);
                    counter += 1;
                }
            }
        });
        if(titles === '')return null;
        let fullUrl = endpointUrl + '&titles=' + titles;
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl)
            .then(
                function(response){
                    let mapTitleQid = [];
                    for (const key in response.data.query.pages) {
                      let page = response.data.query.pages[key];
                      if(page.pageprops){
                          let title = page.title;
                          let qid = page.pageprops['wikibase_item'];
                          // todo optimize
                        if(response.data.query.normalized){
                            let normalized = response.data.query.normalized.filter(e => {
                              return e.to === title;
                            });
                            if(normalized.length > 0){
                              title = normalized[0].from;
                            }
                        }
                        mapTitleQid[title] = qid;
                      }
                    }
                    pois.forEach(poi => {
                        if(poi.wikiTitle && mapTitleQid[poi.wikiTitle]){
                            poi.qid = mapTitleQid[poi.wikiTitle];
                        }
                    });
                    return pois;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: wikidataService enrichPOIsWithQID error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }
    function getParamLang(lang){
        let paramLang = "de, en, fr"
        if(lang && lang === 'en_US'){
            paramLang = "en, de"
        }
        else if(lang && lang === 'fr_FR'){
            paramLang = "fr, en, de"
        }
        return paramLang;
    }

    return {
        getPlaces: getPlaces,
        getPlace: getPlace,
        getPersons: getPersons,
        getPersonByGND: getPersonByGND,
        getPersonByQID: getPersonByQID,
        getPersonGndByQID: getPersonGndByQID,
        getResearcherProfile: getResearcherProfile,
        getStudentsOfPerson: getStudentsOfPerson,
        getTeachersOfPerson: getTeachersOfPerson,
        getArchivedOfPerson: getArchivedOfPerson,
        enrichPOIsWithQID: enrichPOIsWithQID
        //getPlacesOfPerson: getPlacesOfPerson,
        //getPersonsByPlace: getPersonsByPlace,
        //getPersonsEmployedByPlace: getPersonsEmployedByPlace,
        //getPersonsEducatedAtPlace:getPersonsEducatedAtPlace,
        //getPersonsBornInPlace:getPersonsBornInPlace,
        //getPersonsBurriedInPlace: getPersonsBurriedInPlace,
    };
}]);
/* Alles was im HSA archiviert ist
SELECT ?item   ?itemLabel
where{
?item wdt:P485 wd:Q39934978.
SERVICE wikibase:label { bd:serviceParam wikibase:language "de,en". }
}
*/

/*
function getPlacesOfPerson(qid){
    const endpointUrl = 'https://query.wikidata.org/sparql';
    let sparqlQuery = `
    SELECT DISTINCT  ?place ?placeLabel ?image ?pred ?predLabel
    WHERE {
      wd:${qid} (wdt:P551|wdt:P20|wdt:P937|wdt:P1321|wdt:P19|wdt:P119|wdt:P69|wdt:P108) ?place.
      wd:${qid} ?pred ?place.
      ?place wdt:P17 wd:Q39.
      OPTIONAL {?place wdt:P18 ?image.}
      SERVICE wikibase:label { bd:serviceParam wikibase:language "de,en". }
    }
    LIMIT 100
    `;
    let fullUrl = endpointUrl + '?query=' + sparqlQuery;
    $sce.trustAsResourceUrl(fullUrl);

    return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
        .then(
            function(response){
                return response.data;
            },
            function(httpError){
                let error = "***ETH*** an error occured: wikidataService getPlacesOfPerson error callback: " + httpError.status;
                if (httpError.data && httpError.data.errorMessage) {
                    error += ' - ' + httpError.data.errorMessage;
                }
                console.error(error);
                return null;
            }
        )
}
*/
/*
function getPersonsByPlace(qid){
    let props = '(wdt:P551|wdt:P20|wdt:P937|wdt:P1321)';
    return _getPersonsByPlace(qid, props);
}

function getPersonsBornInPlace(qid){
    let props = 'wdt:P19';
    return _getPersonsByPlace(qid, props);
}

function getPersonsBurriedInPlace(qid){
    let props = 'wdt:P119';
    return _getPersonsByPlace(qid, props);
}

function getPersonsEducatedAtPlace(qid){
    let props = 'wdt:P69';
    return _getPersonsByPlace(qid, props);
}

function getPersonsEmployedByPlace(qid){
    let props = 'wdt:P108';
    return _getPersonsByPlace(qid, props);
}
*/
/*
// ?item wdt:P106 wd:Q1622272. Hochschullehrer
function _getPersonsByPlace(qid,props,lang){
    let paramLang = getParamLang(lang);
    const endpointUrl = 'https://query.wikidata.org/sparql';
    let sparqlQuery = `
    SELECT DISTINCT ?item ?itemLabel ?itemDescription ?gnd ?image
    (group_concat(DISTINCT(?birth) ;separator = " or ") as ?births )
    (group_concat(DISTINCT(?death) ;separator = " or ") as ?deaths )
    WHERE {
      ?item ${props} wd:${qid}.
      ?item wdt:P18 ?image.
      OPTIONAL {?item wdt:P227 ?gnd.}
      OPTIONAL {?item wdt:P569 ?birth.}
      OPTIONAL {?item wdt:P570 ?death.}
      OPTIONAL {?item wdt:P106 ?profession.}
      FILTER (!bound(?birth) || !(YEAR(?birth) > 1940))
      SERVICE wikibase:label { bd:serviceParam wikibase:language "${paramLang}". }
    }
    GROUP BY ?item ?itemLabel ?itemDescription ?gnd ?image
    ORDER BY ASC(?births)
    LIMIT 100
    `;
    let fullUrl = endpointUrl + '?query=' + sparqlQuery;
    $sce.trustAsResourceUrl(fullUrl);
    return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
        .then(
            function(response){
                return response.data;
            },
            function(httpError){
                let error = "***ETH*** an error occured: wikidataService _getPersonsByPlace error callback: " + httpError.status;
                if (httpError.data && httpError.data.errorMessage) {
                    error += ' - ' + httpError.data.errorMessage;
                }
                console.error(error);
                return null;
            }
        )
}

*/
